<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appprodUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appprodUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = urldecode($pathinfo);

        // index_ajax
        if ($pathinfo === '/ajax_orders') {
            return array (  '_controller' => 'Movit\\TestBundle\\Controller\\DefaultController::ajaxAction',  '_route' => 'index_ajax',);
        }

        // job_new
        if ($pathinfo === '/job/new') {
            return array (  '_controller' => 'Movit\\TestBundle\\Controller\\JobsController::newjobAction',  '_route' => 'job_new',);
        }

        // job_edit
        if (0 === strpos($pathinfo, '/job/edit') && preg_match('#^/job/edit/(?P<id>[^/]+?)$#x', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Movit\\TestBundle\\Controller\\JobsController::editjobAction',)), array('_route' => 'job_edit'));
        }

        // job_delete
        if (0 === strpos($pathinfo, '/job/delete') && preg_match('#^/job/delete/(?P<id>[^/]+?)$#x', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Movit\\TestBundle\\Controller\\JobsController::deletejobAction',)), array('_route' => 'job_delete'));
        }

        // order_new
        if ($pathinfo === '/order/new') {
            return array (  '_controller' => 'Movit\\TestBundle\\Controller\\OrdersController::neworderAction',  '_route' => 'order_new',);
        }

        // order_edit
        if (0 === strpos($pathinfo, '/order/edit') && preg_match('#^/order/edit/(?P<id>[^/]+?)$#x', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Movit\\TestBundle\\Controller\\OrdersController::editorderAction',)), array('_route' => 'order_edit'));
        }

        // order_delete
        if (0 === strpos($pathinfo, '/order/delete') && preg_match('#^/order/delete/(?P<id>[^/]+?)$#x', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Movit\\TestBundle\\Controller\\OrdersController::deleteorderAction',)), array('_route' => 'order_delete'));
        }

        // sales_ajax
        if ($pathinfo === '/sales/ajax') {
            return array (  '_controller' => 'Movit\\TestBundle\\Controller\\SalesController::ajaxAction',  '_route' => 'sales_ajax',);
        }

        // sales_sku_ajax
        if ($pathinfo === '/sales/sku/ajax') {
            return array (  '_controller' => 'Movit\\TestBundle\\Controller\\SalesController::ajaxskuAction',  '_route' => 'sales_sku_ajax',);
        }

        // sales_city_ajax
        if ($pathinfo === '/sales/city/ajax') {
            return array (  '_controller' => 'Movit\\TestBundle\\Controller\\SalesController::ajaxcityAction',  '_route' => 'sales_city_ajax',);
        }

        // MovitTestBundle_homepage
        if ($pathinfo === '/hello') {
            return array (  '_controller' => 'Movit\\TestBundle\\Controller\\DefaultController::indexAction',  '_route' => 'MovitTestBundle_homepage',);
        }

        // MovitTestBundle_vendorpage
        if ($pathinfo === '/vendor') {
            return array (  '_controller' => 'Movit\\TestBundle\\Controller\\DefaultController::vendorAction',  '_route' => 'MovitTestBundle_vendorpage',);
        }

        // MovitTestBundle_salespage
        if ($pathinfo === '/sales') {
            return array (  '_controller' => 'Movit\\TestBundle\\Controller\\SalesController::salesAction',  '_route' => 'MovitTestBundle_salespage',);
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
