<?php

/* ::base.html.twig */
class __TwigTemplate_839fc9ea0a36e9d5fbee4823f588d911 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = array();
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $context = array_merge($this->env->getGlobals(), $context);

        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
        <title>";
        // line 4
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 5
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 6
        echo "</head>
<body class=\"grid  home\">
  
<div id=\"header\">
  <div class=\"grid\">
     <div class=\"logo-box col two\">
        <h1 class=\"logo\"><a href=\"home.html\">Movit</a></h1>

     </div>


    <div class=\"nav-box col eight\">
      <div class=\"inner\">
        
        <ul class=\"user-navigation\">
          <li class=\"home selected\"><a href=\"home.html\">Home</a></li>
          <li class=\"inbox empty \">
            <a href=\"inbox.html\">Inbox</a>
          </li>
          <li class=\"contacts \"><a href=\"contacts.html\">Contacts</a></li>
          <li class=\"calendar\"><a href=\"calendar.html\">Calendar</a></li>
          <li class=\"tasks \"><a href=\"tasks.html\">Tasks</a></li>
        </ul>

        <div class=\"user\">
          <a href=\"#\" class=\"dropdown\">

            <b>Other Options</b>
          </a>
          <ul class=\"user-options-navigation\">
            <li><a href=\"https://podio.com/users/98507/edit\">Account Settings</a></li>
            <li><a href=\"https://podio.com/-/store/own\">My shared app</a></li>
            <li><a href=\"https://podio.com/-/organization/create\" class=\"js-create-org\">Create another organisation</a></li>
            <li><a href=\"https://podio.com/logout\">Logout</a></li>

          </ul>
        </div>



      </div>
    </div>
  </div>
  
 ";
        // line 50
        $this->displayBlock('body', $context, $blocks);
        // line 51
        echo " ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 52
        echo "</div>



</body>
</html>



";
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome!";
    }

    // line 5
    public function block_stylesheets($context, array $blocks = array())
    {
    }

    // line 50
    public function block_body($context, array $blocks = array())
    {
    }

    // line 51
    public function block_javascripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
