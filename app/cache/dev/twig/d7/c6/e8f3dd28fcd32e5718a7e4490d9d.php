<?php

/* MovitTestBundle:Default:vendor.html.twig */
class __TwigTemplate_d7c6e8f3dd28fcd32e5718a7e4490d9d extends Twig_Template
{
    protected $parent;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = array();
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
        );
    }

    public function getParent(array $context)
    {
        $parent = "::layout.html.twig";
        if ($parent instanceof Twig_Template) {
            $name = $parent->getTemplateName();
            $this->parent[$name] = $parent;
            $parent = $name;
        } elseif (!isset($this->parent[$parent])) {
            $this->parent[$parent] = $this->env->loadTemplate($parent);
        }

        return $this->parent[$parent];
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $context = array_merge($this->env->getGlobals(), $context);

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 3
        echo "<link href=\"/movit/bundles/movittest/css/mainLayout.php\" media=\"all\" rel=\"stylesheet\"/>
";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "    <h1>Hello!</h1>
    <a href='http://www.podio.com'>Podio</a>
";
    }

    public function getTemplateName()
    {
        return "MovitTestBundle:Default:vendor.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
