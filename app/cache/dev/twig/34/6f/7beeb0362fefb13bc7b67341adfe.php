<?php

/* ::layout.html.twig */
class __TwigTemplate_346f7beeb0362fefb13bc7b67341adfe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = array();
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $context = array_merge($this->env->getGlobals(), $context);

        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
        <title>";
        // line 4
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 5
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 8
        echo "</head>
<body class=\"grid  home\">
<div id=\"header\">
  <div class=\"grid\">
     <div class=\"logo-box col two\">
        <h1 class=\"logo\"><a href=\"home.html\">Movit</a></h1>

     </div>


    <div class=\"nav-box col eight\">
      <div class=\"inner\">
        
        <ul id='spaceList' class=\"user-navigation\">
          <li class=\"home selected standardLink\">
            <a href=\"http://conc.ealto.me/movit/app_dev.php/hello\">
            <img class='iconSet1' src='/movit/bundles/movittest/images/cube.png'/>
            Quality Control</a>
          </li>
  

          <li class=\"inbox empty standardLink\">
            <a href=\"inbox.html\">
            <img class='iconSet1' src='/movit/bundles/movittest/images/phone.png'/>
            Sales
            </a>
          </li>
          
          <li class=\"contacts standardLink\">
          <a href=\"contacts.html\">
          <img class='iconSet1' src='/movit/bundles/movittest/images/flag.png'/>
          Maintenance
          </a>
          </li>
        </ul>

        <div class=\"user\">
          <a href=\"#\" class=\"dropdown\">

            <b>Other Options</b>
          </a>
          <ul class=\"user-options-navigation\">
            <li><a href=\"https://podio.com/users/98507/edit\">Account Settings</a></li>
            <li><a href=\"https://podio.com/-/store/own\">My shared app</a></li>
            <li><a href=\"https://podio.com/-/organization/create\" class=\"js-create-org\">Create another organisation</a></li>
            <li><a href=\"https://podio.com/logout\">Logout</a></li>

          </ul>
        </div>



      </div>
    </div>
  </div>
  
 ";
        // line 64
        $this->displayBlock('body', $context, $blocks);
        // line 66
        echo " ";
        $this->displayBlock('content', $context, $blocks);
        // line 68
        echo " ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 70
        echo "</div>



</body>
</html>




";
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome!";
    }

    // line 5
    public function block_stylesheets($context, array $blocks = array())
    {
        echo " 
 \t\t
        ";
    }

    // line 64
    public function block_body($context, array $blocks = array())
    {
        // line 65
        echo " ";
    }

    // line 66
    public function block_content($context, array $blocks = array())
    {
        // line 67
        echo " ";
    }

    // line 68
    public function block_javascripts($context, array $blocks = array())
    {
        // line 69
        echo " ";
    }

    public function getTemplateName()
    {
        return "::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
