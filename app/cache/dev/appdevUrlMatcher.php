<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appdevUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appdevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = urldecode($pathinfo);

        // _welcome
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', '_welcome');
            }
            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\WelcomeController::indexAction',  '_route' => '_welcome',);
        }

        // _demo_login
        if ($pathinfo === '/demo/secured/login') {
            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::loginAction',  '_route' => '_demo_login',);
        }

        // _security_check
        if ($pathinfo === '/demo/secured/login_check') {
            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::securityCheckAction',  '_route' => '_security_check',);
        }

        // _demo_logout
        if ($pathinfo === '/demo/secured/logout') {
            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::logoutAction',  '_route' => '_demo_logout',);
        }

        // acme_demo_secured_hello
        if ($pathinfo === '/demo/secured/hello') {
            return array (  'name' => 'World',  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloAction',  '_route' => 'acme_demo_secured_hello',);
        }

        // _demo_secured_hello
        if (0 === strpos($pathinfo, '/demo/secured/hello') && preg_match('#^/demo/secured/hello/(?P<name>[^/]+?)$#x', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloAction',)), array('_route' => '_demo_secured_hello'));
        }

        // _demo_secured_hello_admin
        if (0 === strpos($pathinfo, '/demo/secured/hello/admin') && preg_match('#^/demo/secured/hello/admin/(?P<name>[^/]+?)$#x', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloadminAction',)), array('_route' => '_demo_secured_hello_admin'));
        }

        if (0 === strpos($pathinfo, '/demo')) {
            // _demo
            if (rtrim($pathinfo, '/') === '/demo') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', '_demo');
                }
                return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::indexAction',  '_route' => '_demo',);
            }

            // _demo_hello
            if (0 === strpos($pathinfo, '/demo/hello') && preg_match('#^/demo/hello/(?P<name>[^/]+?)$#x', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::helloAction',)), array('_route' => '_demo_hello'));
            }

            // _demo_contact
            if ($pathinfo === '/demo/contact') {
                return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::contactAction',  '_route' => '_demo_contact',);
            }

        }

        // _wdt
        if (preg_match('#^/_wdt/(?P<token>[^/]+?)$#x', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::toolbarAction',)), array('_route' => '_wdt'));
        }

        if (0 === strpos($pathinfo, '/_profiler')) {
            // _profiler_search
            if ($pathinfo === '/_profiler/search') {
                return array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::searchAction',  '_route' => '_profiler_search',);
            }

            // _profiler_purge
            if ($pathinfo === '/_profiler/purge') {
                return array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::purgeAction',  '_route' => '_profiler_purge',);
            }

            // _profiler_import
            if ($pathinfo === '/_profiler/import') {
                return array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::importAction',  '_route' => '_profiler_import',);
            }

            // _profiler_export
            if (0 === strpos($pathinfo, '/_profiler/export') && preg_match('#^/_profiler/export/(?P<token>[^/\\.]+?)\\.txt$#x', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::exportAction',)), array('_route' => '_profiler_export'));
            }

            // _profiler_search_results
            if (preg_match('#^/_profiler/(?P<token>[^/]+?)/search/results$#x', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::searchResultsAction',)), array('_route' => '_profiler_search_results'));
            }

            // _profiler
            if (preg_match('#^/_profiler/(?P<token>[^/]+?)$#x', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::panelAction',)), array('_route' => '_profiler'));
            }

        }

        if (0 === strpos($pathinfo, '/_configurator')) {
            // _configurator_home
            if (rtrim($pathinfo, '/') === '/_configurator') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', '_configurator_home');
                }
                return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
            }

            // _configurator_step
            if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?P<index>[^/]+?)$#x', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',)), array('_route' => '_configurator_step'));
            }

            // _configurator_final
            if ($pathinfo === '/_configurator/final') {
                return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
            }

        }

        // index_ajax
        if ($pathinfo === '/ajax_orders') {
            return array (  '_controller' => 'Movit\\TestBundle\\Controller\\DefaultController::ajaxAction',  '_route' => 'index_ajax',);
        }

        // job_new
        if (0 === strpos($pathinfo, '/job/new') && preg_match('#^/job/new(?:/(?P<id>\\d+))?$#x', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  'id' => '0',  '_controller' => 'Movit\\TestBundle\\Controller\\JobsController::newjobAction',)), array('_route' => 'job_new'));
        }

        // job_edit
        if (0 === strpos($pathinfo, '/job/edit') && preg_match('#^/job/edit/(?P<id>[^/]+?)$#x', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Movit\\TestBundle\\Controller\\JobsController::editjobAction',)), array('_route' => 'job_edit'));
        }

        // job_delete
        if (0 === strpos($pathinfo, '/job/delete') && preg_match('#^/job/delete/(?P<id>[^/]+?)$#x', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Movit\\TestBundle\\Controller\\JobsController::deletejobAction',)), array('_route' => 'job_delete'));
        }

        // order_new
        if ($pathinfo === '/order/new') {
            return array (  '_controller' => 'Movit\\TestBundle\\Controller\\OrdersController::neworderAction',  '_route' => 'order_new',);
        }

        // order_edit
        if (0 === strpos($pathinfo, '/order/edit') && preg_match('#^/order/edit/(?P<id>[^/]+?)$#x', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Movit\\TestBundle\\Controller\\OrdersController::editorderAction',)), array('_route' => 'order_edit'));
        }

        // order_delete
        if (0 === strpos($pathinfo, '/order/delete') && preg_match('#^/order/delete/(?P<id>[^/]+?)$#x', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Movit\\TestBundle\\Controller\\OrdersController::deleteorderAction',)), array('_route' => 'order_delete'));
        }

        // sales_ajax
        if ($pathinfo === '/sales/ajax') {
            return array (  '_controller' => 'Movit\\TestBundle\\Controller\\SalesController::ajaxAction',  '_route' => 'sales_ajax',);
        }

        // sales_sku_ajax
        if ($pathinfo === '/sales/sku/ajax') {
            return array (  '_controller' => 'Movit\\TestBundle\\Controller\\SalesController::ajaxskuAction',  '_route' => 'sales_sku_ajax',);
        }

        // sales_city_ajax
        if ($pathinfo === '/sales/city/ajax') {
            return array (  '_controller' => 'Movit\\TestBundle\\Controller\\SalesController::ajaxcityAction',  '_route' => 'sales_city_ajax',);
        }

        // unassigned_legs
        if ($pathinfo === '/unassigned-legs') {
            return array (  '_controller' => 'Movit\\TestBundle\\Controller\\UnassignedLegsController::indexAction',  '_route' => 'unassigned_legs',);
        }

        // reset_database
        if ($pathinfo === '/reset-database') {
            return array (  '_controller' => 'Movit\\TestBundle\\Controller\\UnassignedLegsController::resetDatabaseAction',  '_route' => 'reset_database',);
        }

        // reset_leg_jobs
        if ($pathinfo === '/reset-database-leg-jobs') {
            return array (  '_controller' => 'Movit\\TestBundle\\Controller\\UnassignedLegsController::resetDatabaseLegsAction',  '_route' => 'reset_leg_jobs',);
        }

        // split_undo
        if ($pathinfo === '/split-undo') {
            return array (  '_controller' => 'Movit\\TestBundle\\Controller\\UnassignedLegsController::splitUndoAction',  '_route' => 'split_undo',);
        }

        // meld
        if ($pathinfo === '/meld') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_meld;
            }
            return array (  '_controller' => 'Movit\\TestBundle\\Controller\\UnassignedLegsController::meldAjaxAction',  '_route' => 'meld',);
        }
        not_meld:

        // unassigned_legs_new
        if ($pathinfo === '/unassigned-legs-new') {
            return array (  '_controller' => 'Movit\\TestBundle\\Controller\\UnassignedLegsController::unassignedlegsAction',  '_route' => 'unassigned_legs_new',);
        }

        // ajax_job_legs
        if ($pathinfo === '/ajax-job-legs') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_ajax_job_legs;
            }
            return array (  '_controller' => 'Movit\\TestBundle\\Controller\\UnassignedLegsController::ajaxJobLegsAction',  '_route' => 'ajax_job_legs',);
        }
        not_ajax_job_legs:

        // ajax_assign_job_legs
        if ($pathinfo === '/ajax-assign-job-legs') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_ajax_assign_job_legs;
            }
            return array (  '_controller' => 'Movit\\TestBundle\\Controller\\UnassignedLegsController::ajaxAssignJobLegsAction',  '_route' => 'ajax_assign_job_legs',);
        }
        not_ajax_assign_job_legs:

        // ajax_error_meld_assign_job_legs
        if ($pathinfo === '/ajax-error-meld-assign-job-legs') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_ajax_error_meld_assign_job_legs;
            }
            return array (  '_controller' => 'Movit\\TestBundle\\Controller\\UnassignedLegsController::ajaxErrorAssignJobLegsAction',  '_route' => 'ajax_error_meld_assign_job_legs',);
        }
        not_ajax_error_meld_assign_job_legs:

        // ajax_session
        if ($pathinfo === '/ajax-session') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_ajax_session;
            }
            return array (  '_controller' => 'Movit\\TestBundle\\Controller\\UnassignedLegsController::ajaxSessionAction',  '_route' => 'ajax_session',);
        }
        not_ajax_session:

        // create_job
        if ($pathinfo === '/create-job') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_create_job;
            }
            return array (  '_controller' => 'Movit\\TestBundle\\Controller\\UnassignedLegsController::createJobAction',  '_route' => 'create_job',);
        }
        not_create_job:

        // MovitTestBundle_homepage
        if ($pathinfo === '/hello') {
            return array (  '_controller' => 'Movit\\TestBundle\\Controller\\DefaultController::indexAction',  '_route' => 'MovitTestBundle_homepage',);
        }

        // MovitTestBundle_vendorpage
        if ($pathinfo === '/vendor') {
            return array (  '_controller' => 'Movit\\TestBundle\\Controller\\DefaultController::vendorAction',  '_route' => 'MovitTestBundle_vendorpage',);
        }

        // MovitTestBundle_salespage
        if ($pathinfo === '/sales') {
            return array (  '_controller' => 'Movit\\TestBundle\\Controller\\SalesController::salesAction',  '_route' => 'MovitTestBundle_salespage',);
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
