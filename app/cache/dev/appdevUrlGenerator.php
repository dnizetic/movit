<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;


/**
 * appdevUrlGenerator
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appdevUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    static private $declaredRouteNames = array(
       '_welcome' => true,
       '_demo_login' => true,
       '_security_check' => true,
       '_demo_logout' => true,
       'acme_demo_secured_hello' => true,
       '_demo_secured_hello' => true,
       '_demo_secured_hello_admin' => true,
       '_demo' => true,
       '_demo_hello' => true,
       '_demo_contact' => true,
       '_wdt' => true,
       '_profiler_search' => true,
       '_profiler_purge' => true,
       '_profiler_import' => true,
       '_profiler_export' => true,
       '_profiler_search_results' => true,
       '_profiler' => true,
       '_configurator_home' => true,
       '_configurator_step' => true,
       '_configurator_final' => true,
       'index_ajax' => true,
       'job_new' => true,
       'job_edit' => true,
       'job_delete' => true,
       'order_new' => true,
       'order_edit' => true,
       'order_delete' => true,
       'sales_ajax' => true,
       'sales_sku_ajax' => true,
       'sales_city_ajax' => true,
       'unassigned_legs' => true,
       'reset_database' => true,
       'reset_leg_jobs' => true,
       'split_undo' => true,
       'meld' => true,
       'unassigned_legs_new' => true,
       'ajax_job_legs' => true,
       'ajax_assign_job_legs' => true,
       'ajax_error_meld_assign_job_legs' => true,
       'ajax_session' => true,
       'create_job' => true,
       'MovitTestBundle_homepage' => true,
       'MovitTestBundle_vendorpage' => true,
       'MovitTestBundle_salespage' => true,
    );

    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function generate($name, $parameters = array(), $absolute = false)
    {
        if (!isset(self::$declaredRouteNames[$name])) {
            throw new RouteNotFoundException(sprintf('Route "%s" does not exist.', $name));
        }

        $escapedName = str_replace('.', '__', $name);

        list($variables, $defaults, $requirements, $tokens) = $this->{'get'.$escapedName.'RouteInfo'}();

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $absolute);
    }

    private function get_welcomeRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\WelcomeController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/',  ),));
    }

    private function get_demo_loginRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::loginAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/demo/secured/login',  ),));
    }

    private function get_security_checkRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::securityCheckAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/demo/secured/login_check',  ),));
    }

    private function get_demo_logoutRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::logoutAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/demo/secured/logout',  ),));
    }

    private function getacme_demo_secured_helloRouteInfo()
    {
        return array(array (), array (  'name' => 'World',  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/demo/secured/hello',  ),));
    }

    private function get_demo_secured_helloRouteInfo()
    {
        return array(array (  0 => 'name',), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'name',  ),  1 =>   array (    0 => 'text',    1 => '/demo/secured/hello',  ),));
    }

    private function get_demo_secured_hello_adminRouteInfo()
    {
        return array(array (  0 => 'name',), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloadminAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'name',  ),  1 =>   array (    0 => 'text',    1 => '/demo/secured/hello/admin',  ),));
    }

    private function get_demoRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/demo/',  ),));
    }

    private function get_demo_helloRouteInfo()
    {
        return array(array (  0 => 'name',), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::helloAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'name',  ),  1 =>   array (    0 => 'text',    1 => '/demo/hello',  ),));
    }

    private function get_demo_contactRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::contactAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/demo/contact',  ),));
    }

    private function get_wdtRouteInfo()
    {
        return array(array (  0 => 'token',), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::toolbarAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'token',  ),  1 =>   array (    0 => 'text',    1 => '/_wdt',  ),));
    }

    private function get_profiler_searchRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::searchAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/_profiler/search',  ),));
    }

    private function get_profiler_purgeRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::purgeAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/_profiler/purge',  ),));
    }

    private function get_profiler_importRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::importAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/_profiler/import',  ),));
    }

    private function get_profiler_exportRouteInfo()
    {
        return array(array (  0 => 'token',), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::exportAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '.txt',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/\\.]+?',    3 => 'token',  ),  2 =>   array (    0 => 'text',    1 => '/_profiler/export',  ),));
    }

    private function get_profiler_search_resultsRouteInfo()
    {
        return array(array (  0 => 'token',), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::searchResultsAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/search/results',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'token',  ),  2 =>   array (    0 => 'text',    1 => '/_profiler',  ),));
    }

    private function get_profilerRouteInfo()
    {
        return array(array (  0 => 'token',), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::panelAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'token',  ),  1 =>   array (    0 => 'text',    1 => '/_profiler',  ),));
    }

    private function get_configurator_homeRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/_configurator/',  ),));
    }

    private function get_configurator_stepRouteInfo()
    {
        return array(array (  0 => 'index',), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'index',  ),  1 =>   array (    0 => 'text',    1 => '/_configurator/step',  ),));
    }

    private function get_configurator_finalRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/_configurator/final',  ),));
    }

    private function getindex_ajaxRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Movit\\TestBundle\\Controller\\DefaultController::ajaxAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/ajax_orders',  ),));
    }

    private function getjob_newRouteInfo()
    {
        return array(array (  0 => 'id',), array (  'id' => '0',  '_controller' => 'Movit\\TestBundle\\Controller\\JobsController::newjobAction',), array (  'id' => '\\d+',), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '\\d+',    3 => 'id',  ),  1 =>   array (    0 => 'text',    1 => '/job/new',  ),));
    }

    private function getjob_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Movit\\TestBundle\\Controller\\JobsController::editjobAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  1 =>   array (    0 => 'text',    1 => '/job/edit',  ),));
    }

    private function getjob_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Movit\\TestBundle\\Controller\\JobsController::deletejobAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  1 =>   array (    0 => 'text',    1 => '/job/delete',  ),));
    }

    private function getorder_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Movit\\TestBundle\\Controller\\OrdersController::neworderAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/order/new',  ),));
    }

    private function getorder_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Movit\\TestBundle\\Controller\\OrdersController::editorderAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  1 =>   array (    0 => 'text',    1 => '/order/edit',  ),));
    }

    private function getorder_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Movit\\TestBundle\\Controller\\OrdersController::deleteorderAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  1 =>   array (    0 => 'text',    1 => '/order/delete',  ),));
    }

    private function getsales_ajaxRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Movit\\TestBundle\\Controller\\SalesController::ajaxAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/sales/ajax',  ),));
    }

    private function getsales_sku_ajaxRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Movit\\TestBundle\\Controller\\SalesController::ajaxskuAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/sales/sku/ajax',  ),));
    }

    private function getsales_city_ajaxRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Movit\\TestBundle\\Controller\\SalesController::ajaxcityAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/sales/city/ajax',  ),));
    }

    private function getunassigned_legsRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Movit\\TestBundle\\Controller\\UnassignedLegsController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/unassigned-legs',  ),));
    }

    private function getreset_databaseRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Movit\\TestBundle\\Controller\\UnassignedLegsController::resetDatabaseAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/reset-database',  ),));
    }

    private function getreset_leg_jobsRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Movit\\TestBundle\\Controller\\UnassignedLegsController::resetDatabaseLegsAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/reset-database-leg-jobs',  ),));
    }

    private function getsplit_undoRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Movit\\TestBundle\\Controller\\UnassignedLegsController::splitUndoAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/split-undo',  ),));
    }

    private function getmeldRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Movit\\TestBundle\\Controller\\UnassignedLegsController::meldAjaxAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/meld',  ),));
    }

    private function getunassigned_legs_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Movit\\TestBundle\\Controller\\UnassignedLegsController::unassignedlegsAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/unassigned-legs-new',  ),));
    }

    private function getajax_job_legsRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Movit\\TestBundle\\Controller\\UnassignedLegsController::ajaxJobLegsAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/ajax-job-legs',  ),));
    }

    private function getajax_assign_job_legsRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Movit\\TestBundle\\Controller\\UnassignedLegsController::ajaxAssignJobLegsAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/ajax-assign-job-legs',  ),));
    }

    private function getajax_error_meld_assign_job_legsRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Movit\\TestBundle\\Controller\\UnassignedLegsController::ajaxErrorAssignJobLegsAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/ajax-error-meld-assign-job-legs',  ),));
    }

    private function getajax_sessionRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Movit\\TestBundle\\Controller\\UnassignedLegsController::ajaxSessionAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/ajax-session',  ),));
    }

    private function getcreate_jobRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Movit\\TestBundle\\Controller\\UnassignedLegsController::createJobAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/create-job',  ),));
    }

    private function getMovitTestBundle_homepageRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Movit\\TestBundle\\Controller\\DefaultController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/hello',  ),));
    }

    private function getMovitTestBundle_vendorpageRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Movit\\TestBundle\\Controller\\DefaultController::vendorAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/vendor',  ),));
    }

    private function getMovitTestBundle_salespageRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Movit\\TestBundle\\Controller\\SalesController::salesAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/sales',  ),));
    }
}
