<!DOCTYPE html>
<html>
<head>
	<link href="<?php echo $view['assets']->getUrl('bundles/movittest/css/style2.css') ?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo $view['assets']->getUrl('bundles/movittest/css/mainLayout.php') ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $view['assets']->getUrl('bundles/movittest/css/styles.css') ?>" rel="stylesheet" type="text/css" />
        
	<script src="<?php echo $view['assets']->getUrl('bundles/movittest/js/ui/lib/prototype.js') ?>"></script>
  	<script src="<?php echo $view['assets']->getUrl('bundles/movittest/js/fabtabulous.js') ?>"></script>
  	<script src="<?php echo $view['assets']->getUrl('bundles/movittest/js/tablekit.js') ?>"></script>
        
        <script src="<?php echo $view['assets']->getUrl('bundles/movittest/js/window.js') ?>"></script>
        
        <link href="<?php echo $view['assets']->getUrl('bundles/movittest/css/themes/default.css') ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $view['assets']->getUrl('bundles/movittest/css/themes/mac_os_x.css') ?>" rel="stylesheet" type="text/css"/>
        
</head>

<body class="grid  home">
  
<div id="header">
  <div class="grid">
     <div class="logo-box col two">
        <h1 class="logo"><a id='movitTitle' href="http://conc.ealto.me/movit/app_dev.php/hello">Movit</a></h1>
     </div>


    <div class="nav-box col eight">
      <div class="inner">
        
        <ul id='spaceLink' class="user-navigation">
          <li class="home selected standardLink">
            <a href="http://conc.ealto.me/movit/app_dev.php/hello">
            <!--<img class='iconSet1' src='/movit/bundles/movittest/images/cube.png'/>-->
            Quality Control</a>
          </li>
          <li class="inbox empty standardLink">
            <a href="http://conc.ealto.me/movit/app_dev.php/sales">
            <!--<img class='iconSet1' src='/movit/bundles/movittest/images/phone.png'/>-->
            Sales
            </a>
          </li>  
          <li class="contacts standardLink">
          <a href="contacts.html">
          <!--<img class='iconSet1' src='/movit/bundles/movittest/images/flag.png'/>-->
          Contacts
          </a>
          </li>

        </ul>

        <div class="user">
          <a href="#" class="dropdown">

            <b>Other Options</b>
          </a>
            <!--
          <ul class="user-options-navigation">
            <li><a href="https://podio.com/users/98507/edit">Account Settings</a></li>
            <li><a href="https://podio.com/-/store/own">My shared app</a></li>
            <li><a href="https://podio.com/-/organization/create" class="js-create-org">Create another organisation</a></li>
            <li><a href="https://podio.com/logout">Logout</a></li>

          </ul>
            -->
        </div>



      </div>
    </div>
  </div>
</div>

   
<div id='formsQC'>
        <?php $view['slots']->output('_content') ?>
</div>
</body>


</html>