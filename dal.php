<?php

define('DB_HOST', 'AMAZONA-R6BUP07\EC2SQLEXPRESS');
define('DB_USER', 'CWLogistics');
define('DB_PASSWORD', '(WL0g1st1(5');
define('DB_DB', 'ContainerWest');

class DALQueryResult {
	private $_results = array();

	public function set($key, $val){
		$this->_results[$key] = $val;
	}

	public function get($key) {
		if(isset($this->_results[$key])) {
			return $this->_results[$key];
		} else {
			return null;
		}
	}
}

class DAL {
	private $conn = null;
	private static $inst = null;
	
	private function __construct() {
	}
	
	public static function getInstance() {
		if(!self::$inst) {
			self::$inst = new DAL();		
		}
		return self::$inst;
	}
	
	private function dbconnect() {
		$connection_info = array("UID"=>DB_USER,
		                         "PWD"=>DB_PASSWORD,
		                         "Database"=>DB_DB);
		$conn = sqlsrv_connect(DB_HOST, $connection_info);
		if($conn === false) {
			echo "Unable to connect to sqlsrv.</br>";
			die(print_r(sqlsrv_errors(), true));
		}
		
		$this->conn = $conn;
		return $conn;
	}

	//parameters: $sql, [param1, param2, ...]
	public function query() {
		$this->dbconnect();
		
		$params = func_get_args();
		$sql = array_shift($params);
		
		$res = sqlsrv_query($this->conn, $sql, $params);

		if(!$res) {
			return false;
		}
		if(strpos($sql, 'SELECT') === false) {
			return true;
		}

		$results = array();

		while($row = sqlsrv_fetch_array($res)) {

			$result = new DALQueryResult();

			foreach($row as $k=>$v) {
				$result->set($k, $v);
			}

			$results[] = $result;
		}
		return $results;
	}
	
	function __destruct() {
		if($this->conn) {
			@sqlsrv_close($this->conn);
		}
	}
}

/*
 function modifyAll($user, $pass, $host, $name)
 {
 $this->database_user = $user;
 $this->database_pass = $pass;
 $this->database_host = $host;
 $this->database_name = $name;
 }
 function modifyUser($user)
 {
 $this->database_user = $user;
 }
 function modifyPass($pass)
 {
 $this->database_pass = $pass;
 }
 function modifyHost($host)
 {
 $this->database_host = $host;
 }
 function modifyName($name)
 {
 $this->databse_name = $name;
 }

 function connect()
 {
 $this->database_info = array($this->database_user, $this->database_pass,$this->databse_name);
 $this->database_link = sqlsrv_connect($this->database_host, $this->database_info);
 if ($this->database_link === false){die(print_r( sqlsrv_errors(), true));}
 return $this->database_link;
 }

 */?>
