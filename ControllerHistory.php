<?php
include_once("credentials.php");
include_once("../dal.php");
include_once("./MMM_DB_functions.php");
include_once("./ModelUtil.php");

class ControllerHistory {
	private $webOrders = array();
	private $customerOrder = 'asc';
	
	public function getWebOrders() {
		return $this->webOrders;
	}
	
	public function index() {
		if(!isset($_REQUEST['action'])) {
			$_REQUEST['action'] = '';
		}
		
		switch($_REQUEST['action']) {
			case 'hist_init':
				$this->init();
				break;
		}
	}

	public function init() {
		GLOBAL $conn;
		
		$WebUserID = isset($_POST['WebUserID']) ? $_POST['WebUserID'] : null;
		$SlsManID = isset($_POST['SlsManID']) ? $_POST['SlsManID'] : null;
		$LotID = isset($_POST['LotID']) ? $_POST['LotID'] : null;
		$this->customerOrder = isset($_POST['customerOrder']) ? $_POST['customerOrder'] : 'asc';
		
		$tsql = '
				SELECT
				   [WebOrderID]
			      ,[OrderID]
			      ,[MMM_DEV].[dbo].[WebOrders].[WebOrderStatusID]
			      ,[Status]
			      ,[WebUserID]
			      ,[SlsManID]
			      ,[LotID]
			      ,[CustomPlan]
			      ,CAST([DateProposedDelivery] AS varchar) AS "DateProposedDelivery"
			      ,CAST([DateCreated] AS varchar) AS "DateCreated"
			      ,CAST([DateLastModified] AS varchar) AS "DateLastModified"
			      ,[Customer]
			      ,[City]
			      ,[ProvinceID]
			      ,[MMM_DEV].[dbo].[WebOrder4x12].[Description]
			      ,[SnowLoad]
			      ,[Year]
			      ,[ModelID]
			      ,[BasePrice]
			      ,[DecorID]
			      ,[NotesUser]
			      ,[NotesAdmin]
		  		FROM [MMM_DEV].[dbo].[WebOrders]
				LEFT OUTER JOIN [MMM_DEV].[dbo].[WebOrderStatus]
					ON [MMM_DEV].[dbo].[WebOrders].[WebOrderStatusID] = [MMM_DEV].[dbo].[WebOrderStatus].[WebOrderStatusID]
				LEFT OUTER JOIN [MMM_DEV].[dbo].[WebOrder4x12]
					ON [MMM_DEV].[dbo].[WebOrders].[WebOrder4x12ID] = [MMM_DEV].[dbo].[WebOrder4x12].[WebOrder4x12ID]
				WHERE [MMM_DEV].[dbo].[WebOrders].[WebUserID] = ?
					AND LTRIM(RTRIM([MMM_DEV].[dbo].[WebOrders].[SlsManID])) = ?
					AND LTRIM(RTRIM([MMM_DEV].[dbo].[WebOrders].[LotID])) = ?
				ORDER BY [Customer] '.$this->customerOrder.', [WebOrderID] DESC';
				
		$rtn = DAL::getInstance()->query($tsql, $WebUserID, $SlsManID, $LotID);
		
		if($rtn === false) {
			$errMsg = "SQL could not be executed.<br />";
			$errMsg .= print_r(sqlsrv_errors(), true);
			echo "
				<b>Error loading history. Please refersh page and try again.</b>
				<br />
				<div style='font-size:9px'>
					$errMsg
				</div>
			";
			return;
		}
		
		foreach($rtn as $result) {		
			$this->webOrders[] = array(
									"Customer"=>$result->get('Customer'),
									"WebOrderID"=>$result->get('WebOrderID'),
									"Status"=>$result->get('Status'),
									"DateLastModified"=>$result->get('DateLastModified'),
									"DateCreated"=>$result->get('DateCreated'),
									"ModelID"=>$result->get('ModelID'),
									"CustomPlan"=>$result->get('CustomPlan'),
									"OrderPrice"=>$this->getOrderPrice($result->get('WebOrderID'), $result->get('BasePrice'), $result->get('CustomPlan')),
								);
		}
		
		include ("ViewHistory.php");
	}
	
	private function getOrderPrice($webOrderID, $basePrice, $customPlan) {
		if($customPlan) {
			return "-";
		}
		$stmt = spExecute("sp_Get_WebOrderOptions", array("@webOrderID" => $webOrderID));
  		$aWebOrderOptions = spToAssoc($stmt);
		
		$runningTotal = $basePrice;
		
		 for($i=0; $i<count($aWebOrderOptions); $i++) {
			if((IsInteger(trim($aWebOrderOptions[$i]['Quantity']))) &&
			   (is_numeric(trim($aWebOrderOptions[$i]['Price'])))) {
					$iQuantity = (int) trim($aWebOrderOptions[$i]['Quantity']);
					$iPrice = (float) trim($aWebOrderOptions[$i]['Price']);
					$iTotal = $iQuantity * $iPrice;
					$runningTotal += $iTotal;
			}
		}
		return Util::formatCurrency($runningTotal);
	}
	
	public static function sendOrderModifiedEmail($webOrderID) {
		@session_start();
		if(!isset($_SESSION['rsu'])) {
			return false;
		}
		
		$_POST['0'] = $webOrderID;
		ob_start();
		include ("Receipt_CurrentWebOrder.php");
		$json = ob_get_contents();
		ob_end_clean();
		
		$json = json_decode($json, 1);
		$json[6] = $_SESSION['rsu'];
		$json[7] = true;
		$json = json_encode($json);
		
		$_POST['name'] = $json;
		ob_start();
		include ("GenerateConfirmationEmail.php");
		$json = ob_get_contents();
		ob_end_clean();

		$json = json_decode($json,1);
		
		return $json[0];
	}
}

$c = new ControllerHistory();
$c->index();