<?php

// this check prevents access to debug front controllers that are deployed by accident to production servers.
// feel free to remove this, extend it, or make something more sophisticated.
if (!in_array(@$_SERVER['REMOTE_ADDR'], array(
    '127.0.0.1', '93.139.162.31','50.17.254.171', '93.142.244.108', '93.142.232.84', '93.139.177.240', '93.139.147.180', '93.139.183.4',
    '::1','98.143.92.130', '93.139.186.57', '24.83.192.180','96.55.199.89','64.114.6.146','50.64.3.8','93.139.130.255', '93.139.160.137', '24.108.197.6', '74.198.150.193','68.179.94.89'
))) {
    header('HTTP/1.0 403 Forbidden');
    exit('You are not allowed to access this file. Check '.basename(__FILE__).' for more information.');
}

require_once __DIR__.'/../app/bootstrap.php.cache';
require_once __DIR__.'/../app/AppKernel.php';

use Symfony\Component\HttpFoundation\Request;

$kernel = new AppKernel('dev', true);
$kernel->loadClassCache();
$kernel->handle(Request::createFromGlobals())->send();
