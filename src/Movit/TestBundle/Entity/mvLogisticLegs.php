<?php


/**
* <b>mvLogisticOrders</b> class with integrated CRUD methods.
* @author Php Object Generator
* @version POG 3.0f / PHP5
* @copyright Free for personal & commercial use. (Offered under the BSD license)
* @link http://www.phpobjectgenerator.com/?language=php5&wrapper=pog&objectName=mvLogisticOrders&attributeList=array+%28%0A++0+%3D%3E+%27Date%27%2C%0A++1+%3D%3E+%27DateCommited%27%2C%0A++2+%3D%3E+%27Salesperson_id%27%2C%0A++3+%3D%3E+%27TruckAllocatedTotal%27%2C%0A++4+%3D%3E+%27TruckingCostTotal%27%2C%0A++5+%3D%3E+%27TruckingCostPercent%27%2C%0A%29&typeList=array+%28%0A++0+%3D%3E+%27DATE%27%2C%0A++1+%3D%3E+%27DATE%27%2C%0A++2+%3D%3E+%27INT%27%2C%0A++3+%3D%3E+%27INT%27%2C%0A++4+%3D%3E+%27INT%27%2C%0A++5+%3D%3E+%27FLOAT%27%2C%0A%29
*/

namespace Movit\TestBundle\Entity;

use Movit\TestBundle\Services\DAL as DAL;


class mvLogisticLegs extends BaseObject
{
    
        //change fields, add public fields, change getCurrentData function.
        public $table_name = 'mvLogisticLegs';
        public $fields = "mvLogisticJob_id, WO, OrderLineNum, ReleaseId, PuCityYard, PuProv, PuDateSched, DelCityYard, DelProv, DelDateSched, Status, SubStatus, BookingStatus, ServiceType, SKU, OffloadRequired, CarrierRequested, Customer, SalesRep, Deleted, InitialLegID, PathID, Quantity, IsQuantityParent, PuAddress, DelAddress, OriginalQuantity, FinalDestination";
        public $pk = "mvLogisticLeg_id";
    
        
        //fields
        public $mvLogisticLeg_id = '';
	public $mvLogisticJob_id = '';
	public $WO;
	public $OrderLineNum;
	public $ReleaseId;
	public $PuCityYard;
	public $PuProv;
	public $PuDateSched; 
        public $DelCityYard; 
        public $DelProv;
        public $DelDateSched;
        public $Status;
        public $SubStatus;
	public $BookingStatus; 
        public $ServiceType; 
        public $SKU;
        public $OffloadRequired;
        public $CarrierRequested;
        public $Customer;
        public $SalesRep;
        public $Deleted;
        public $InitialLegID;
	public $PathID;
        public $Quantity;
        public $IsQuantityParent;
        public $PuAddress;
        public $DelAddress;
        public $OriginalQuantity;
        public $FinalDestination;
       
        public $dal_class;
        public $persisted;
	
        public function __construct($data = null)
        {

            //set fields
            $this->dal_class = parent::returnDalClass();
            $this->persisted = 0;
           
            if($data)
                parent::setData($data, $this);
        }
        
        public function getCurrentData($with_primary = false) //data from object
        {
            return array('mvLogisticJob_id' => $this->mvLogisticJob_id, 'WO' => $this->WO, 
                'OrderLineNum' => $this->OrderLineNum, 'ReleaseID' => $this->ReleaseId, 
            /* PUCityYard?    */ 'PuCityYard' => $this->PuCityYard, 'PuProv' => $this->PuProv, 
                'PuDateSched' => $this->PuDateSched, 'DelCityYard' => $this->DelCityYard, 
                'DelProv' => $this->DelProv, 'DelDateSched' => $this->DelDateSched, 
                'Status' => $this->Status, 'SubStatus' => $this->SubStatus, 
                'BookingStatus' => $this->BookingStatus, 'ServiceType' => $this->ServiceType, 
                'SKU' => $this->SKU, 'OffloadRequired' => $this->OffloadRequired, 
                'CarrierRequested' => $this->CarrierRequested, 'Customer' => $this->Customer,
                'SalesRep' => $this->SalesRep, 'Deleted' => $this->Deleted,
                'InitialLegID' => $this->InitialLegID, 'PathID' => $this->PathID,
                'Quantity' => $this->Quantity, 'IsQuantityParent' => $this->IsQuantityParent,
                'PuAddress' => $this->PuAddress, 'DelAddress' => $this->DelAddress,
                'OriginalQuantity' => $this->OriginalQuantity, 'FinalDestination' => $this->FinalDestination);
        }
        
        
        public function save() //creates a new object - has to be customized to support editing aswell
        {
            return parent::baseSave($this->table_name, $this->fields, $this->getCurrentData(), 
                    $this->persisted ? array("mvLogisticLeg_id", $this->mvLogisticLeg_id) : false);
        }
        function delete()
        {
            return parent::baseDelete($this->mvLogisticOrder_id, 'mvLogisticOrders', 'mvLogisticOrder_id');
        }
        
        
        function setDeleted($value)
        {
            $dal_class = $this->dal_class->getInstance();
            $conn = $dal_class->dbconnect();
            $tsql = "UPDATE $this->table_name SET 
                      Deleted = $value
                    WHERE mvLogisticLeg_id = ?";
   
            if(sqlsrv_query($conn, $tsql, array(&$this->mvLogisticLeg_id)) === false )
                return sqlsrv_errors();

            return true;
        }
        function setIsQuantityParent($value)
        {
            $dal_class = $this->dal_class->getInstance();
            $conn = $dal_class->dbconnect();
            $tsql = "UPDATE $this->table_name SET 
                      IsQuantityParent = $value
                    WHERE mvLogisticLeg_id = ?";
   
            if(sqlsrv_query($conn, $tsql, array(&$this->mvLogisticLeg_id)) === false )
                return sqlsrv_errors();

            return true;
        }
        
        function setPathID($value)
        {
            $dal_class = $this->dal_class->getInstance();
            $conn = $dal_class->dbconnect();
            $tsql = "UPDATE $this->table_name SET 
                      PathID = $value
                    WHERE mvLogisticLeg_id = ?";
   
            if(sqlsrv_query($conn, $tsql, array(&$this->mvLogisticLeg_id)) === false )
                return sqlsrv_errors();

            return true;
        }
        
        
        function setInitialLegID($value)
        {
            $dal_class = $this->dal_class->getInstance();
            $conn = $dal_class->dbconnect();
            $tsql = "UPDATE $this->table_name SET 
                      InitialLegID = $value
                    WHERE mvLogisticLeg_id = ?";
   
            if(sqlsrv_query($conn, $tsql, array(&$this->mvLogisticLeg_id)) === false )
                return sqlsrv_errors();

            return true;
        }
        
        
        function mySave()
        {
               $dal_class = $this->dal_class->getInstance();
            
               
               $conn = $dal_class->dbconnect();
                        
               $tsql = "INSERT INTO mvLogisticOrders (Date, DateCommitted, Salesperson_id, Truckallocatedtotal, Truckingcosttotal,
                            Truckingcostpercent) VALUES (?,?,?,?,?,?)";
                

                /* Prepare and execute the statement. */
                $insertReview = sqlsrv_prepare($conn, $tsql, 
                        array(
                            &$this->Date, 
                            &$this->DateCommitted,
                            &$this->Salesperson_id, 
                            &$this->TruckAllocatedTotal, 
                            &$this->TruckingCostTotal, 
                            &$this->TruckingCostPercent));

                
                if( $insertReview === false )
                    { return sqlsrv_errors(); }
                    

                /* By default, all stream data is sent at the time of query execution. */
                if( sqlsrv_execute($insertReview) === false )
                    { return sqlsrv_errors();  }
                
                return true;
                    
        }
        
        function myUpdate($id)
        {
                $dal_class = $this->dal_class->getInstance();
            
               
                $conn = $dal_class->dbconnect();
                        
                
                  $tsql = "UPDATE mvLogisticOrders SET 
                          Date = ?,
                          DateCommitted = ?,
                          Salesperson_id = ?,
                          Truckallocatedtotal = ?,
                          Truckingcosttotal = ?,
                          Truckingcostpercent = ?
                        WHERE mvLogisticOrder_id = ?";
                        
                  $my_id = $id;
                  if( sqlsrv_query($conn, $tsql, array(
                                &$this->Date, 
                                &$this->DateCommitted,
                                &$this->Salesperson_id, 
                                &$this->TruckAllocatedTotal, 
                                &$this->TruckingCostTotal, 
                                &$this->TruckingCostPercent,
                                &$my_id
                          )) === false )
                    return sqlsrv_errors();
                         
                  return true;
                
                   
        }
        
        

        
        function myDelete()
        {
            
            /*
                $dal_class = $this->dal_class->getInstance();
            
               
                $conn = $dal_class->dbconnect();
                        
                
                  $tsql = "DELETE FROM mvLogisticOrders
                           WHERE mvLogisticOrder_id = ?";
                        
                  $my_id = $id;
                  if( sqlsrv_query($conn, $tsql, array(
                                &$my_id
                          )) === false )
                    return false; 
                         
                  
                  return true;
            
             */
        }
        
        
        
        
        /*
	public $pog_attribute_type = array(
		"mvlogisticordersId" => array('db_attributes' => array("NUMERIC", "INT")),
		"Date" => array('db_attributes' => array("NUMERIC", "DATE")),
		"DateCommited" => array('db_attributes' => array("NUMERIC", "DATE")),
		"Salesperson_id" => array('db_attributes' => array("NUMERIC", "INT")),
		"TruckAllocatedTotal" => array('db_attributes' => array("NUMERIC", "INT")),
		"TruckingCostTotal" => array('db_attributes' => array("NUMERIC", "INT")),
		"TruckingCostPercent" => array('db_attributes' => array("NUMERIC", "FLOAT")),
		); */
        
        
        
	public $pog_query;
        
	/**
	* Getter for some private attributes
	* @return mixed $attribute
	*/
	public function __get($attribute)
	{
		if (isset($this->{"_".$attribute}))
		{
			return $this->{"_".$attribute};
		}
		else
		{
			return false;
		}
	}
	
	function mvLogisticOrders($Date='', $DateCommitted='', $Salesperson_id='', $TruckAllocatedTotal='', $TruckingCostTotal='', $TruckingCostPercent='')
	{
		$this->Date = $Date;
		$this->DateCommitted = $DateCommitted;
		$this->Salesperson_id = $Salesperson_id;
		$this->TruckAllocatedTotal = $TruckAllocatedTotal;
		$this->TruckingCostTotal = $TruckingCostTotal;
		$this->TruckingCostPercent = $TruckingCostPercent;
	}
	
	
	/**
	* Gets object from database
	* @param integer $mvlogisticordersId 
	* @return object $mvLogisticOrders
	*/
	function Get($mvlogisticordersId)
	{
		$connection = Database::Connect();
		$this->pog_query = "select * from `mvlogisticorders` where `mvLogisticOrder_id`='".intval($mvlogisticordersId)."' LIMIT 1";
		$cursor = Database::Reader($this->pog_query, $connection);
		while ($row = Database::Read($cursor))
		{
			$this->mvlogisticordersId = $row['mvlogisticordersid'];
			$this->Date = $row['date'];
			$this->DateCommited = $row['datecommited'];
			$this->Salesperson_id = $this->Unescape($row['salesperson_id']);
			$this->TruckAllocatedTotal = $this->Unescape($row['truckallocatedtotal']);
			$this->TruckingCostTotal = $this->Unescape($row['truckingcosttotal']);
			$this->TruckingCostPercent = $this->Unescape($row['truckingcostpercent']);
		}
		return $this;
	}
	
	
	/**
	* Returns a sorted array of objects that match given conditions
	* @param multidimensional array {("field", "comparator", "value"), ("field", "comparator", "value"), ...} 
	* @param string $sortBy 
	* @param boolean $ascending 
	* @param int limit 
	* @return array $mvlogisticordersList
	*/
	function GetList($fcv_array = array(), $sortBy='', $ascending=true, $limit='')
	{
		$connection = Database::Connect();
		$sqlLimit = ($limit != '' ? "LIMIT $limit" : '');
		$this->pog_query = "select * from `mvlogisticorders` ";
		$mvlogisticordersList = Array();
		if (sizeof($fcv_array) > 0)
		{
			$this->pog_query .= " where ";
			for ($i=0, $c=sizeof($fcv_array); $i<$c; $i++)
			{
				if (sizeof($fcv_array[$i]) == 1)
				{
					$this->pog_query .= " ".$fcv_array[$i][0]." ";
					continue;
				}
				else
				{
					if ($i > 0 && sizeof($fcv_array[$i-1]) != 1)
					{
						$this->pog_query .= " AND ";
					}
					if (isset($this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes']) && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'SET')
					{
						if ($GLOBALS['configuration']['db_encoding'] == 1)
						{
							$value = POG_Base::IsColumn($fcv_array[$i][2]) ? "BASE64_DECODE(".$fcv_array[$i][2].")" : "'".$fcv_array[$i][2]."'";
							$this->pog_query .= "BASE64_DECODE(`".$fcv_array[$i][0]."`) ".$fcv_array[$i][1]." ".$value;
						}
						else
						{
							$value =  POG_Base::IsColumn($fcv_array[$i][2]) ? $fcv_array[$i][2] : "'".$this->Escape($fcv_array[$i][2])."'";
							$this->pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." ".$value;
						}
					}
					else
					{
						$value = POG_Base::IsColumn($fcv_array[$i][2]) ? $fcv_array[$i][2] : "'".$fcv_array[$i][2]."'";
						$this->pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." ".$value;
					}
				}
			}
		}
		if ($sortBy != '')
		{
			if (isset($this->pog_attribute_type[$sortBy]['db_attributes']) && $this->pog_attribute_type[$sortBy]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$sortBy]['db_attributes'][0] != 'SET')
			{
				if ($GLOBALS['configuration']['db_encoding'] == 1)
				{
					$sortBy = "BASE64_DECODE($sortBy) ";
				}
				else
				{
					$sortBy = "$sortBy ";
				}
			}
			else
			{
				$sortBy = "$sortBy ";
			}
		}
		else
		{
			$sortBy = "mvlogisticordersid";
		}
		$this->pog_query .= " order by ".$sortBy." ".($ascending ? "asc" : "desc")." $sqlLimit";
		$thisObjectName = get_class($this);
		$cursor = Database::Reader($this->pog_query, $connection);
		while ($row = Database::Read($cursor))
		{
			$mvlogisticorders = new $thisObjectName();
			$mvlogisticorders->mvlogisticordersId = $row['mvlogisticordersid'];
			$mvlogisticorders->Date = $row['date'];
			$mvlogisticorders->DateCommited = $row['datecommited'];
			$mvlogisticorders->Salesperson_id = $this->Unescape($row['salesperson_id']);
			$mvlogisticorders->TruckAllocatedTotal = $this->Unescape($row['truckallocatedtotal']);
			$mvlogisticorders->TruckingCostTotal = $this->Unescape($row['truckingcosttotal']);
			$mvlogisticorders->TruckingCostPercent = $this->Unescape($row['truckingcostpercent']);
			$mvlogisticordersList[] = $mvlogisticorders;
		}
		return $mvlogisticordersList;
	}
	
        
 
        
	
	/**
	* Saves the object to the database
	* @return integer $mvlogisticordersId
	*/
        /*
	function Save()
	{
		//$connection = Database::Connect();
                $dal_class = $this->container->get('DAL')->getInstance();
                
                $connection = $dal_class->dbconnect();
                
		$this->pog_query = "select `mvlogisticordersid` from `mvlogisticorders` where `mvlogisticordersid`='".$this->mvlogisticordersId."' LIMIT 1";
		$rows = Database::Query($this->pog_query, $connection);
		if ($rows > 0)
		{
			$this->pog_query = "update `mvlogisticorders` set 
			`date`='".$this->Date."', 
			`datecommited`='".$this->DateCommited."', 
			`salesperson_id`='".$this->Escape($this->Salesperson_id)."', 
			`truckallocatedtotal`='".$this->Escape($this->TruckAllocatedTotal)."', 
			`truckingcosttotal`='".$this->Escape($this->TruckingCostTotal)."', 
			`truckingcostpercent`='".$this->Escape($this->TruckingCostPercent)."' where `mvlogisticordersid`='".$this->mvlogisticordersId."'";
		}
		else
		{
			$this->pog_query = "insert into `mvlogisticorders` (`date`, `datecommited`, `salesperson_id`, `truckallocatedtotal`, `truckingcosttotal`, `truckingcostpercent` ) values (
			'".$this->Date."', 
			'".$this->DateCommited."', 
			'".$this->Escape($this->Salesperson_id)."', 
			'".$this->Escape($this->TruckAllocatedTotal)."', 
			'".$this->Escape($this->TruckingCostTotal)."', 
			'".$this->Escape($this->TruckingCostPercent)."' )";
		}
		$insertId = Database::InsertOrUpdate($this->pog_query, $connection);
		if ($this->mvlogisticordersId == "")
		{
			$this->mvlogisticordersId = $insertId;
		}
		return $this->mvlogisticordersId;
	}
        
         */
	
	
	/**
	* Clones the object and saves it to the database
	* @return integer $mvlogisticordersId
	*/
	function SaveNew()
	{
		$this->mvlogisticordersId = '';
		return $this->Save();
	}
	
	
	/**
	* Deletes the object from the database
	* @return boolean
	*/
        /*
	function Delete()
	{
		$connection = Database::Connect();
		$this->pog_query = "delete from `mvlogisticorders` where `mvlogisticordersid`='".$this->mvlogisticordersId."'";
		return Database::NonQuery($this->pog_query, $connection);
	}
         * 
         */
	
	
	/**
	* Deletes a list of objects that match given conditions
	* @param multidimensional array {("field", "comparator", "value"), ("field", "comparator", "value"), ...} 
	* @param bool $deep 
	* @return 
	*/
	function DeleteList($fcv_array)
	{
		if (sizeof($fcv_array) > 0)
		{
			$connection = Database::Connect();
			$pog_query = "delete from `mvlogisticorders` where ";
			for ($i=0, $c=sizeof($fcv_array); $i<$c; $i++)
			{
				if (sizeof($fcv_array[$i]) == 1)
				{
					$pog_query .= " ".$fcv_array[$i][0]." ";
					continue;
				}
				else
				{
					if ($i > 0 && sizeof($fcv_array[$i-1]) !== 1)
					{
						$pog_query .= " AND ";
					}
					if (isset($this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes']) && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'SET')
					{
						$pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." '".$this->Escape($fcv_array[$i][2])."'";
					}
					else
					{
						$pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." '".$fcv_array[$i][2]."'";
					}
				}
			}
			return Database::NonQuery($pog_query, $connection);
		}
	}
        
}
?>