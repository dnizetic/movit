<?php
namespace Movit\TestBundle\Entity;

use Movit\TestBundle\Services\DAL as DAL;


class BaseObject
{
    
        public function returnDalClass()
        {
            $dc = new DAL();
            return $dc->getInstance();
        }

        public function setData($data, $child, $from_db = false)
        {
            $fields = $child->fields;
            $fields = explode(',', $fields);
            $trim = array();
            foreach($fields as $field)
                $trim[] = trim($field);
            
            //trim left
            foreach($trim as $field) {
                if($from_db) { //data is from DB, different format of data
                    eval("\$child->\$field = \$data->get('$field');");
                } else { //data is from form
                    if(is_object($data["$field"])) 
                        eval("\$child->\$field = \$data['$field']->format('Y-m-d');");
                    else 
                        eval("\$child->\$field = \$data['$field'];");
                }
            }
            
            /* this actually happens:
                    $order->Date = ($data['Date'] != NULL) ? $data['Date']->format('Y-m-d') : NULL;
                    $order->DateCommitted = ($data['DateCommitted'] != NULL) ? $data['DateCommitted']->format('Y-m-d') : NULL;
                    $order->Salesperson_id = $data['Salesperson_id'];
                    $order->TruckAllocatedTotal = $data['TruckAllocatedTotal'];
                    $order->TruckingCostTotal = $data['TruckingCostTotal'];
                    $order->TruckingCostPercent = $data['TruckingCostPercent'];
            */
            
        }
    
        public function baseDelete($id, $table, $field)
        {
                $dal_class = $this->returnDalClass();
            
                
                $conn = $dal_class->dbconnect();
                        
                
                $tsql = "DELETE FROM $table
                           WHERE $field = ?";
                        
                $my_id = $id;
                if( sqlsrv_query($conn, $tsql, array(&$my_id)) === false )
                    throw new \Exception('Unable to delete Entity');
                
                return true;
        }
        
        public function lastInsertId($queryID) {
            sqlsrv_next_result($queryID);
            sqlsrv_fetch($queryID);
            return sqlsrv_get_field($queryID, 0);
        } 
        
        public function baseSave($table, $fields, $data, $pk = false)
        {
               $dal_class = $this->returnDalClass();
               $conn = $dal_class->dbconnect();
               
               $qst_marks = "";
               $temp_data = $data;
               $ref_data = array();
               
               $i = 0;
               foreach($data as $key => $value) {
                   $qst_marks .= '?,';
                   $ref_data[$i++] = &$temp_data[$key];
               }
               $qsts = substr($qst_marks, 0, -1); //get rid of last ','
               
               if($pk)
                   return $this->update($table, $fields, $ref_data, $pk, $conn);
               else
                   $tsql = "INSERT INTO $table ($fields) VALUES ($qsts)";
                

                //$tsql .= ";";
                /* Prepare and execute the statement. */
                $insertReview = sqlsrv_prepare($conn, $tsql, $ref_data); //data has to be array

                
                if( $insertReview === false )
                    throw new \Exception('Unable to save Entity 1 : ' . sqlsrv_errors());
                    

                /* By default, all stream data is sent at the time of query execution. */
                if( sqlsrv_execute($insertReview) === false )
                    die(var_dump(sqlsrv_errors()));//throw new \Exception('Unable to execute query (Save Method): ' . sqlsrv_errors() . $tsql);
                
                
               
                return true;//lastInsertId(  $insertReview  );
                //return sqlsrv_fetch_array(  $stmt  );
                //return true;
        }
        
        private function update($table, $fields, $ref_data, $pk, $conn)
        {
                   $tsql = "UPDATE $table SET ";
                   
                   $fields = explode(',', trim($fields));
                  
                   foreach($fields as $field)
                       $tsql .= ($field . ' = ?,');
                   $tsql = substr($tsql, 0, -1);
                   
                   $primary_key_name = array_shift($pk);
                   $pk_value = array_shift($pk);
                   $tsql .= " WHERE $primary_key_name = ?";
                   array_push($ref_data, $pk_value);
             
                   
                   if(sqlsrv_query($conn, $tsql, $ref_data) === false )
                      throw new \Exception('Unable to update Entity: ' . sqlsrv_errors()); 
                         
                   return true;
            
        }
        
        public function find($id, $table)
        {
            eval("namespace Movit\\TestBundle\\Entity;  \$object = new $table($id);");
  
                
            $data = $this->findData($id, $table, $object->pk);
     
            if($data) {
                //$object->mvLogisticOrder_id = $id;
                $pk = $object->pk;
                eval("\$object->$pk = \$id;");
                $this->setData($data, $object, true);
                $object->persisted = 1;
            } else
                throw new \Exception("Object of type $table with an id of $id does not exist.");//throw new Exception('Object does not exist');
          
            return $object;
        }
        
        private function findData($id, $table, $field)
        {
            $dal_class = $this->returnDalClass();
            
            $conn = $dal_class->dbconnect();
            
            
            $tsql = "SELECT * FROM $table  
                     WHERE $field = $id";

            $result = $dal_class->query($tsql);

            if(!$result || !$result[0]) {
                return false;
            }
            return $result[0];
        }
        
        public function filterData($data, $child)
        {

            
            $new_data = array();
            
            /*foreach($data as $key => $value) {
                if(!is_numeric($key))
                   $new_data[$key] = $value;
            }*/
            
            for($i = 0; $i < count($data) / 2; ++$i)
                $new_data = $data[$i];
            
            
            /* GOAL:
                    $this->Date = $data->get('Date');
                    $this->DateCommitted = $data->get('DateCommitted');
                    $this->Salesperson_id = $data->get('Salesperson_id');
                    $this->TruckAllocatedTotal = $data->get('TruckAllocatedTotal');
                    $this->TruckingCostTotal = $data->get('TruckingCostTotal');
                    $this->TruckingCostPercent = $data->get('TruckingCostPercent');
             */
            
            return $new_data;
        }
        
        

}


/*
 	function SetFieldAttribute($fieldName, $attributeName, $attributeValue)
	{
        if (isset($this->pog_attribute_type[$fieldName]) && isset($this->pog_attribute_type[$fieldName][$attributeName]))
        {
             $this->pog_attribute_type[$fieldName][$attributeName] = $attributeValue;
        }
	}

	function GetFieldAttribute($fieldName, $attributeName)
	{
        if (isset($this->pog_attribute_type[$fieldName]) && isset($this->pog_attribute_type[$fieldName][$attributeName]))
        {
        	return $this->pog_attribute_type[$fieldName][$attributeName];
        }
        return null;
	}


	///////////////////////////
	// Data manipulation
	///////////////////////////

	/**
	* This function will try to encode $text to base64, except when $text is a number. This allows us to Escape all data before they're inserted in the database, regardless of attribute type.
	* @param string $text
	* @return string encoded to base64
	*/
/*
	public function Escape($text)
	{
		if ($GLOBALS['configuration']['db_encoding'] && !is_numeric($text))
		{
			return base64_encode($text);
		}
		return mysql_real_escape_string($text);
	}

	/**
	 * Enter description here...
	 *
	 * @param unknown_type $text
	 * @return unknown
	 */
/*
	public function Unescape($text)
	{
		if ($GLOBALS['configuration']['db_encoding'] && !is_numeric($text))
		{
			return base64_decode($text);
		}
		return stripcslashes($text);
	}


	////////////////////////////////
	// Table -> Object Mapping
	////////////////////////////////

	/**
	 * Executes $query against database and returns the result set as an array of POG objects
	 *
	 * @param string $query. SQL query to execute against database
	 * @param string $objectClass. POG Object type to return
	 * @param bool $lazy. If true, will also load all children/sibling
	 */
/*
	protected function FetchObjects($query, $objectClass, $lazy = true)
	{
		$databaseConnection = Database::Connect();
		$result = Database::Reader($query, $databaseConnection);
		$objectList = $this->CreateObjects($result, $objectClass, $lazy);
		return $objectList;
	}

	private function CreateObjects($mysql_result, $objectClass, $lazyLoad = true)
	{
		$objectList = array();
		if ($mysql_result != null){
			while ($row =  Database::Read($mysql_result))
			{
				$pog_object = new $objectClass();
				$this->PopulateObjectAttributes($row, $pog_object);
				$objectList[] = $pog_object;
			}
		}
		return $objectList;
	}

	private function PopulateObjectAttributes($fetched_row, $pog_object)
	{
		$att = $this->GetAttributes($pog_object);
 		foreach ($att as $column)
		{
			$pog_object->{$column} = $this->Unescape($fetched_row[strtolower($column)]);
		}
		return $pog_object;
	}

	public function GetAttributes($object, $type='')
	{
		$columns = array();
		foreach ($object->pog_attribute_type as $att => $properties)
		{
			if ($properties['db_attributes'][0] != 'OBJECT')
			{
				if (($type != '' && strtolower($type) == strtolower($properties['db_attributes'][0])) || $type == ''){
					$columns[] = $att;
				}
			}
		}
		return $columns;
	}

	//misc
	public static function IsColumn($value)
	{
		if (strlen($value) > 2)
		{
			if (substr($value, 0, 1) == '`' && substr($value, strlen($value) - 1, 1) == '`')
			{
				return true;
			}
			return false;
		}
		return false;
	}
        */
 
?>


