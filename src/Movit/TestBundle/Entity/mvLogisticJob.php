<?php
namespace Movit\TestBundle\Entity;



/*
	This SQL query will create the table to store your object.

	CREATE TABLE `mvlogisticjob` (
	`mvlogisticjobid` int(11) NOT NULL auto_increment,
	`mvlogisticorder_id` BIGINT NOT NULL,
	`mvlogisticstatus_id` INT NOT NULL,
	`datecreditapproved` DATETIME NOT NULL,
	`freightallocated` FLOAT NOT NULL,
	`delinfo` TINYINT NOT NULL,
	`offloadrequired` TINYINT NOT NULL,
	`mvlogisticdelby_id` INT NOT NULL,
	`datedelsched` DATETIME NOT NULL,
	`datedelsched2` DATETIME NOT NULL,
	`mvlogisticsku_id` INT NOT NULL,
	`sku` VARCHAR(255) NOT NULL,
	`container` VARCHAR(255) NOT NULL,
	`wo` VARCHAR(255) NOT NULL,
	`dateshopin` DATETIME NOT NULL,
	`dateshopoutsched` DATETIME NOT NULL,
	`dateshopoutactual` DATETIME NOT NULL,
	`weight` INT NOT NULL,
	`lxv` VARCHAR(255) NOT NULL,
	`alertweightdimension` TINYINT NOT NULL,
	`pucityyard` VARCHAR(255) NOT NULL,
	`puprov` VARCHAR(255) NOT NULL,
	`pudatesched` DATETIME NOT NULL,
	`pudateactual` DATETIME NOT NULL,
	`delcityyard` VARCHAR(255) NOT NULL,
	`delprov` VARCHAR(255) NOT NULL,
	`deldatesched` DATETIME NOT NULL,
	`deldateactual` DATETIME NOT NULL,
	`mvlogisticservicetype_id` INT NOT NULL,
	`carriername` VARCHAR(255) NOT NULL,
	`carrierquote` FLOAT NOT NULL,
	`carriernotes` VARCHAR(255) NOT NULL, PRIMARY KEY  (`mvlogisticjobid`)) ENGINE=MyISAM;
*/

/**
* <b>mvLogisticJob</b> class with integrated CRUD methods.
* @author Php Object Generator
* @version POG 3.0f / PHP5
* @copyright Free for personal & commercial use. (Offered under the BSD license)
* @link http://www.phpobjectgenerator.com/?language=php5&wrapper=pog&objectName=mvLogisticJob&attributeList=array+%28%0A++0+%3D%3E+%27mvLogisticOrder_id%27%2C%0A++1+%3D%3E+%27mvLogisticStatus_id%27%2C%0A++2+%3D%3E+%27DateCreditApproved%27%2C%0A++3+%3D%3E+%27FreightAllocated%27%2C%0A++4+%3D%3E+%27DelInfo%27%2C%0A++5+%3D%3E+%27OffloadRequired%27%2C%0A++6+%3D%3E+%27mvLogisticDelBy_id%27%2C%0A++7+%3D%3E+%27DateDelSched%27%2C%0A++8+%3D%3E+%27DateDelSched2%27%2C%0A++9+%3D%3E+%27mvLogisticSKU_id%27%2C%0A++10+%3D%3E+%27SKU%27%2C%0A++11+%3D%3E+%27Container%27%2C%0A++12+%3D%3E+%27WO%27%2C%0A++13+%3D%3E+%27DateShopIn%27%2C%0A++14+%3D%3E+%27DateShopOutSched%27%2C%0A++15+%3D%3E+%27DateShopOutActual%27%2C%0A++16+%3D%3E+%27Weight%27%2C%0A++17+%3D%3E+%27L+x+W+x+H%27%2C%0A++18+%3D%3E+%27AlertWeightDimension%27%2C%0A++19+%3D%3E+%27PUCityYard%27%2C%0A++20+%3D%3E+%27PUProv%27%2C%0A++21+%3D%3E+%27PUDateSched%27%2C%0A++22+%3D%3E+%27PUDateActual%27%2C%0A++23+%3D%3E+%27DelCityYard%27%2C%0A++24+%3D%3E+%27DelProv%27%2C%0A++25+%3D%3E+%27DelDateSched%27%2C%0A++26+%3D%3E+%27DelDateActual%27%2C%0A++27+%3D%3E+%27mvLogisticServiceType_id%27%2C%0A++28+%3D%3E+%27CarrierName%27%2C%0A++29+%3D%3E+%27CarrierQuote%27%2C%0A++30+%3D%3E+%27CarrierNotes%27%2C%0A%29&typeList=array+%28%0A++0+%3D%3E+%27BIGINT%27%2C%0A++1+%3D%3E+%27INT%27%2C%0A++2+%3D%3E+%27DATETIME%27%2C%0A++3+%3D%3E+%27FLOAT%27%2C%0A++4+%3D%3E+%27TINYINT%27%2C%0A++5+%3D%3E+%27TINYINT%27%2C%0A++6+%3D%3E+%27INT%27%2C%0A++7+%3D%3E+%27DATETIME%27%2C%0A++8+%3D%3E+%27DATETIME%27%2C%0A++9+%3D%3E+%27INT%27%2C%0A++10+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++11+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++12+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++13+%3D%3E+%27DATETIME%27%2C%0A++14+%3D%3E+%27DATETIME%27%2C%0A++15+%3D%3E+%27DATETIME%27%2C%0A++16+%3D%3E+%27INT%27%2C%0A++17+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++18+%3D%3E+%27TINYINT%27%2C%0A++19+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++20+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++21+%3D%3E+%27DATETIME%27%2C%0A++22+%3D%3E+%27DATETIME%27%2C%0A++23+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++24+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++25+%3D%3E+%27DATETIME%27%2C%0A++26+%3D%3E+%27DATETIME%27%2C%0A++27+%3D%3E+%27INT%27%2C%0A++28+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++29+%3D%3E+%27FLOAT%27%2C%0A++30+%3D%3E+%27VARCHAR%28255%29%27%2C%0A%29
*/


class mvLogisticJob extends BaseObject
{
	public $mvLogisticJob_id = '';

	/**
	 * @var BIGINT
	 */
	public $mvLogisticOrder_id;
	
	/**
	 * @var INT
	 */
	public $mvLogisticStatus_id;
	
	/**
	 * @var DATETIME
	 */
	public $DateCreditApproved;
	
	/**
	 * @var FLOAT
	 */
	public $FreightAllocated;
	
	/**
	 * @var TINYINT
	 */
	public $DelInfo;
	
	/**
	 * @var TINYINT
	 */
	public $OffloadRequired;
	
	/**
	 * @var INT
	 */
	public $mvLogisticDelBy_id;
	
	/**
	 * @var DATETIME
	 */
	public $DateDelSched;
	
	/**
	 * @var DATETIME
	 */
	public $DateDelSched2;
	
	/**
	 * @var INT
	 */
	public $mvLogisticSKU_id;
	
	/**
	 * @var VARCHAR(255)
	 */
	public $SKU;
	
	/**
	 * @var VARCHAR(255)
	 */
	public $Container;
	
	/**
	 * @var VARCHAR(255)
	 */
	public $WO;
	
	/**
	 * @var DATETIME
	 */
	public $DateShopIn;
	
	/**
	 * @var DATETIME
	 */
	public $DateShopOutSched;
	
	/**
	 * @var DATETIME
	 */
	public $DateShopOutActual;
	
	/**
	 * @var INT
	 */
	public $Weight;
	
	/**
	 * @var VARCHAR(255)
	 */
	public $lxw;
	
	/**
	 * @var TINYINT
	 */
	public $AlertWeightDimension;
	
	/**
	 * @var VARCHAR(255)
	 */
	public $PUCityYard;
	
	/**
	 * @var VARCHAR(255)
	 */
	public $PUProv;
	
	/**
	 * @var DATETIME
	 */
	public $PUDateSched;
	
	/**
	 * @var DATETIME
	 */
	public $PUDateActual;
	
	/**
	 * @var VARCHAR(255)
	 */
	public $DelCityYard;
	
	/**
	 * @var VARCHAR(255)
	 */
	public $DelProv;
	
	/**
	 * @var DATETIME
	 */
	public $DelDateSched;
	
	/**
	 * @var DATETIME
	 */
	public $DelDateActual;
	
	/**
	 * @var INT
	 */
	public $mvLogisticServiceType_id;
	
	/**
	 * @var VARCHAR(255)
	 */
	public $CarrierName;
	
	/**
	 * @var FLOAT
	 */
	public $CarrierQuote;
	
	/**
	 * @var VARCHAR(255)
	 */
	public $CarrierNotes;
        
        

        
	
	public $pog_attribute_type = array(
		"mvlogisticjobId" => array('db_attributes' => array("NUMERIC", "INT")),
		"mvLogisticOrder_id" => array('db_attributes' => array("NUMERIC", "BIGINT")),
		"mvLogisticStatus_id" => array('db_attributes' => array("NUMERIC", "INT")),
		"DateCreditApproved" => array('db_attributes' => array("TEXT", "DATETIME")),
		"FreightAllocated" => array('db_attributes' => array("NUMERIC", "FLOAT")),
		"DelInfo" => array('db_attributes' => array("NUMERIC", "TINYINT")),
		"OffloadRequired" => array('db_attributes' => array("NUMERIC", "TINYINT")),
		"mvLogisticDelBy_id" => array('db_attributes' => array("NUMERIC", "INT")),
		"DateDelSched" => array('db_attributes' => array("TEXT", "DATETIME")),
		"DateDelSched2" => array('db_attributes' => array("TEXT", "DATETIME")),
		"mvLogisticSKU_id" => array('db_attributes' => array("NUMERIC", "INT")),
		"SKU" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"Container" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"WO" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"DateShopIn" => array('db_attributes' => array("TEXT", "DATETIME")),
		"DateShopOutSched" => array('db_attributes' => array("TEXT", "DATETIME")),
		"DateShopOutActual" => array('db_attributes' => array("TEXT", "DATETIME")),
		"Weight" => array('db_attributes' => array("NUMERIC", "INT")),
		"lxv" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"AlertWeightDimension" => array('db_attributes' => array("NUMERIC", "TINYINT")),
		"PUCityYard" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"PUProv" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"PUDateSched" => array('db_attributes' => array("TEXT", "DATETIME")),
		"PUDateActual" => array('db_attributes' => array("TEXT", "DATETIME")),
		"DelCityYard" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"DelProv" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"DelDateSched" => array('db_attributes' => array("TEXT", "DATETIME")),
		"DelDateActual" => array('db_attributes' => array("TEXT", "DATETIME")),
		"mvLogisticServiceType_id" => array('db_attributes' => array("NUMERIC", "INT")),
		"CarrierName" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"CarrierQuote" => array('db_attributes' => array("NUMERIC", "FLOAT")),
		"CarrierNotes" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		);
        
	public $pog_query;
	
	
        
        public $dal_class;
	
        public function __construct()
        {
            $this->dal_class = parent::returnDalClass();
        }
        
        
	/**
	* Getter for some private attributes
	* @return mixed $attribute
	*/
	public function __get($attribute)
	{
		if (isset($this->{"_".$attribute}))
		{
			return $this->{"_".$attribute};
		}
		else
		{
			return false;
		}
	}
	
	function mvLogisticJob($mvLogisticOrder_id='', $mvLogisticStatus_id='', $DateCreditApproved='', $FreightAllocated='', $DelInfo='', $OffloadRequired='', $mvLogisticDelBy_id='', $DateDelSched='', $DateDelSched2='', $mvLogisticSKU_id='', $SKU='', $Container='', $WO='', $DateShopIn='', $DateShopOutSched='', $DateShopOutActual='', $Weight='', $lxv='', $AlertWeightDimension='', $PUCityYard='', $PUProv='', $PUDateSched='', $PUDateActual='', $DelCityYard='', $DelProv='', $DelDateSched='', $DelDateActual='', $mvLogisticServiceType_id='', $CarrierName='', $CarrierQuote='', $CarrierNotes='')
	{
		$this->mvLogisticOrder_id = $mvLogisticOrder_id;
		$this->mvLogisticStatus_id = $mvLogisticStatus_id;
		$this->DateCreditApproved = $DateCreditApproved;
		$this->FreightAllocated = $FreightAllocated;
		$this->DelInfo = $DelInfo;
		$this->OffloadRequired = $OffloadRequired;
		$this->mvLogisticDelBy_id = $mvLogisticDelBy_id;
		$this->DateDelSched = $DateDelSched;
		$this->DateDelSched2 = $DateDelSched2;
		$this->mvLogisticSKU_id = $mvLogisticSKU_id;
		$this->SKU = $SKU;
		$this->Container = $Container;
		$this->WO = $WO;
		$this->DateShopIn = $DateShopIn;
		$this->DateShopOutSched = $DateShopOutSched;
		$this->DateShopOutActual = $DateShopOutActual;
		$this->Weight = $Weight;
		$this->lxv = $lxv;
		$this->AlertWeightDimension = $AlertWeightDimension;
		$this->PUCityYard = $PUCityYard;
		$this->PUProv = $PUProv;
		$this->PUDateSched = $PUDateSched;
		$this->PUDateActual = $PUDateActual;
		$this->DelCityYard = $DelCityYard;
		$this->DelProv = $DelProv;
		$this->DelDateSched = $DelDateSched;
		$this->DelDateActual = $DelDateActual;
		$this->mvLogisticServiceType_id = $mvLogisticServiceType_id;
		$this->CarrierName = $CarrierName;
		$this->CarrierQuote = $CarrierQuote;
		$this->CarrierNotes = $CarrierNotes;
	}
	
	
	/**
	* Gets object from database
	* @param integer $mvLogisticJob_id 
	* @return object $mvLogisticJob
	*/
	function Get($mvLogisticJob_id)
	{
		$connection = Database::Connect();
		$this->pog_query = "select * from `mvlogisticjob` where `mvlogisticjobid`='".intval($mvLogisticJob_id)."' LIMIT 1";
		$cursor = Database::Reader($this->pog_query, $connection);
		while ($row = Database::Read($cursor))
		{
			$this->mvlogisticjobId = $row['mvlogisticjobid'];
			$this->mvLogisticOrder_id = $this->Unescape($row['mvlogisticorder_id']);
			$this->mvLogisticStatus_id = $this->Unescape($row['mvlogisticstatus_id']);
			$this->DateCreditApproved = $row['datecreditapproved'];
			$this->FreightAllocated = $this->Unescape($row['freightallocated']);
			$this->DelInfo = $this->Unescape($row['delinfo']);
			$this->OffloadRequired = $this->Unescape($row['offloadrequired']);
			$this->mvLogisticDelBy_id = $this->Unescape($row['mvlogisticdelby_id']);
			$this->DateDelSched = $row['datedelsched'];
			$this->DateDelSched2 = $row['datedelsched2'];
			$this->mvLogisticSKU_id = $this->Unescape($row['mvlogisticsku_id']);
			$this->SKU = $this->Unescape($row['sku']);
			$this->Container = $this->Unescape($row['container']);
			$this->WO = $this->Unescape($row['wo']);
			$this->DateShopIn = $row['dateshopin'];
			$this->DateShopOutSched = $row['dateshopoutsched'];
			$this->DateShopOutActual = $row['dateshopoutactual'];
			$this->Weight = $this->Unescape($row['weight']);
			$this->lxv = $this->Unescape($row['lxv']);
			$this->AlertWeightDimension = $this->Unescape($row['alertweightdimension']);
			$this->PUCityYard = $this->Unescape($row['pucityyard']);
			$this->PUProv = $this->Unescape($row['puprov']);
			$this->PUDateSched = $row['pudatesched'];
			$this->PUDateActual = $row['pudateactual'];
			$this->DelCityYard = $this->Unescape($row['delcityyard']);
			$this->DelProv = $this->Unescape($row['delprov']);
			$this->DelDateSched = $row['deldatesched'];
			$this->DelDateActual = $row['deldateactual'];
			$this->mvLogisticServiceType_id = $this->Unescape($row['mvlogisticservicetype_id']);
			$this->CarrierName = $this->Unescape($row['carriername']);
			$this->CarrierQuote = $this->Unescape($row['carrierquote']);
			$this->CarrierNotes = $this->Unescape($row['carriernotes']);
		}
		return $this;
	}
	
	
	/**
	* Returns a sorted array of objects that match given conditions
	* @param multidimensional array {("field", "comparator", "value"), ("field", "comparator", "value"), ...} 
	* @param string $sortBy 
	* @param boolean $ascending 
	* @param int limit 
	* @return array $mvlogisticjobList
	*/
        /*
	function GetList($fcv_array = array(), $sortBy='', $ascending=true, $limit='')
	{
		$connection = Database::Connect();
		$sqlLimit = ($limit != '' ? "LIMIT $limit" : '');
		$this->pog_query = "select * from `mvlogisticjob` ";
		$mvlogisticjobList = Array();
		if (sizeof($fcv_array) > 0)
		{
			$this->pog_query .= " where ";
			for ($i=0, $c=sizeof($fcv_array); $i<$c; $i++)
			{
				if (sizeof($fcv_array[$i]) == 1)
				{
					$this->pog_query .= " ".$fcv_array[$i][0]." ";
					continue;
				}
				else
				{
					if ($i > 0 && sizeof($fcv_array[$i-1]) != 1)
					{
						$this->pog_query .= " AND ";
					}
					if (isset($this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes']) && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'SET')
					{
						if ($GLOBALS['configuration']['db_encoding'] == 1)
						{
							$value = POG_Base::IsColumn($fcv_array[$i][2]) ? "BASE64_DECODE(".$fcv_array[$i][2].")" : "'".$fcv_array[$i][2]."'";
							$this->pog_query .= "BASE64_DECODE(`".$fcv_array[$i][0]."`) ".$fcv_array[$i][1]." ".$value;
						}
						else
						{
							$value =  POG_Base::IsColumn($fcv_array[$i][2]) ? $fcv_array[$i][2] : "'".$this->Escape($fcv_array[$i][2])."'";
							$this->pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." ".$value;
						}
					}
					else
					{
						$value = POG_Base::IsColumn($fcv_array[$i][2]) ? $fcv_array[$i][2] : "'".$fcv_array[$i][2]."'";
						$this->pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." ".$value;
					}
				}
			}
		}
		if ($sortBy != '')
		{
			if (isset($this->pog_attribute_type[$sortBy]['db_attributes']) && $this->pog_attribute_type[$sortBy]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$sortBy]['db_attributes'][0] != 'SET')
			{
				if ($GLOBALS['configuration']['db_encoding'] == 1)
				{
					$sortBy = "BASE64_DECODE($sortBy) ";
				}
				else
				{
					$sortBy = "$sortBy ";
				}
			}
			else
			{
				$sortBy = "$sortBy ";
			}
		}
		else
		{
			$sortBy = "mvlogisticjobid";
		}
		$this->pog_query .= " order by ".$sortBy." ".($ascending ? "asc" : "desc")." $sqlLimit";
		$thisObjectName = get_class($this);
		$cursor = Database::Reader($this->pog_query, $connection);
		while ($row = Database::Read($cursor))
		{
			$mvlogisticjob = new $thisObjectName();
			$mvlogisticjob->mvlogisticjobId = $row['mvlogisticjobid'];
			$mvlogisticjob->mvLogisticOrder_id = $this->Unescape($row['mvlogisticorder_id']);
			$mvlogisticjob->mvLogisticStatus_id = $this->Unescape($row['mvlogisticstatus_id']);
			$mvlogisticjob->DateCreditApproved = $row['datecreditapproved'];
			$mvlogisticjob->FreightAllocated = $this->Unescape($row['freightallocated']);
			$mvlogisticjob->DelInfo = $this->Unescape($row['delinfo']);
			$mvlogisticjob->OffloadRequired = $this->Unescape($row['offloadrequired']);
			$mvlogisticjob->mvLogisticDelBy_id = $this->Unescape($row['mvlogisticdelby_id']);
			$mvlogisticjob->DateDelSched = $row['datedelsched'];
			$mvlogisticjob->DateDelSched2 = $row['datedelsched2'];
			$mvlogisticjob->mvLogisticSKU_id = $this->Unescape($row['mvlogisticsku_id']);
			$mvlogisticjob->SKU = $this->Unescape($row['sku']);
			$mvlogisticjob->Container = $this->Unescape($row['container']);
			$mvlogisticjob->WO = $this->Unescape($row['wo']);
			$mvlogisticjob->DateShopIn = $row['dateshopin'];
			$mvlogisticjob->DateShopOutSched = $row['dateshopoutsched'];
			$mvlogisticjob->DateShopOutActual = $row['dateshopoutactual'];
			$mvlogisticjob->Weight = $this->Unescape($row['weight']);
			$mvlogisticjob->lxv = $this->Unescape($row['lxv']);
			$mvlogisticjob->AlertWeightDimension = $this->Unescape($row['alertweightdimension']);
			$mvlogisticjob->PUCityYard = $this->Unescape($row['pucityyard']);
			$mvlogisticjob->PUProv = $this->Unescape($row['puprov']);
			$mvlogisticjob->PUDateSched = $row['pudatesched'];
			$mvlogisticjob->PUDateActual = $row['pudateactual'];
			$mvlogisticjob->DelCityYard = $this->Unescape($row['delcityyard']);
			$mvlogisticjob->DelProv = $this->Unescape($row['delprov']);
			$mvlogisticjob->DelDateSched = $row['deldatesched'];
			$mvlogisticjob->DelDateActual = $row['deldateactual'];
			$mvlogisticjob->mvLogisticServiceType_id = $this->Unescape($row['mvlogisticservicetype_id']);
			$mvlogisticjob->CarrierName = $this->Unescape($row['carriername']);
			$mvlogisticjob->CarrierQuote = $this->Unescape($row['carrierquote']);
			$mvlogisticjob->CarrierNotes = $this->Unescape($row['carriernotes']);
			$mvlogisticjobList[] = $mvlogisticjob;
		}
		return $mvlogisticjobList;
	}
	*/
	
	/**
	* Saves the object to the database
	* @return integer $mvLogisticJob_id
	*/
        /*
	function Save()
	{
		$connection = Database::Connect();
		$this->pog_query = "select `mvlogisticjobid` from `mvlogisticjob` where `mvlogisticjobid`='".$this->mvlogisticjobId."' LIMIT 1";
		$rows = Database::Query($this->pog_query, $connection);
		if ($rows > 0)
		{
			$this->pog_query = "update `mvlogisticjob` set 
			`mvlogisticorder_id`='".$this->Escape($this->mvLogisticOrder_id)."', 
			`mvlogisticstatus_id`='".$this->Escape($this->mvLogisticStatus_id)."', 
			`datecreditapproved`='".$this->DateCreditApproved."', 
			`freightallocated`='".$this->Escape($this->FreightAllocated)."', 
			`delinfo`='".$this->Escape($this->DelInfo)."', 
			`offloadrequired`='".$this->Escape($this->OffloadRequired)."', 
			`mvlogisticdelby_id`='".$this->Escape($this->mvLogisticDelBy_id)."', 
			`datedelsched`='".$this->DateDelSched."', 
			`datedelsched2`='".$this->DateDelSched2."', 
			`mvlogisticsku_id`='".$this->Escape($this->mvLogisticSKU_id)."', 
			`sku`='".$this->Escape($this->SKU)."', 
			`container`='".$this->Escape($this->Container)."', 
			`wo`='".$this->Escape($this->WO)."', 
			`dateshopin`='".$this->DateShopIn."', 
			`dateshopoutsched`='".$this->DateShopOutSched."', 
			`dateshopoutactual`='".$this->DateShopOutActual."', 
			`weight`='".$this->Escape($this->Weight)."', 
			`lxv`='".$this->Escape($this->lxv)."', 
			`alertweightdimension`='".$this->Escape($this->AlertWeightDimension)."', 
			`pucityyard`='".$this->Escape($this->PUCityYard)."', 
			`puprov`='".$this->Escape($this->PUProv)."', 
			`pudatesched`='".$this->PUDateSched."', 
			`pudateactual`='".$this->PUDateActual."', 
			`delcityyard`='".$this->Escape($this->DelCityYard)."', 
			`delprov`='".$this->Escape($this->DelProv)."', 
			`deldatesched`='".$this->DelDateSched."', 
			`deldateactual`='".$this->DelDateActual."', 
			`mvlogisticservicetype_id`='".$this->Escape($this->mvLogisticServiceType_id)."', 
			`carriername`='".$this->Escape($this->CarrierName)."', 
			`carrierquote`='".$this->Escape($this->CarrierQuote)."', 
			`carriernotes`='".$this->Escape($this->CarrierNotes)."' where `mvlogisticjobid`='".$this->mvlogisticjobId."'";
		}
		else
		{
			$this->pog_query = "insert into `mvlogisticjob` (`mvlogisticorder_id`, `mvlogisticstatus_id`, `datecreditapproved`, `freightallocated`, `delinfo`, `offloadrequired`, `mvlogisticdelby_id`, `datedelsched`, `datedelsched2`, `mvlogisticsku_id`, `sku`, `container`, `wo`, `dateshopin`, `dateshopoutsched`, `dateshopoutactual`, `weight`, `lxv`, `alertweightdimension`, `pucityyard`, `puprov`, `pudatesched`, `pudateactual`, `delcityyard`, `delprov`, `deldatesched`, `deldateactual`, `mvlogisticservicetype_id`, `carriername`, `carrierquote`, `carriernotes` ) values (
			'".$this->Escape($this->mvLogisticOrder_id)."', 
			'".$this->Escape($this->mvLogisticStatus_id)."', 
			'".$this->DateCreditApproved."', 
			'".$this->Escape($this->FreightAllocated)."', 
			'".$this->Escape($this->DelInfo)."', 
			'".$this->Escape($this->OffloadRequired)."', 
			'".$this->Escape($this->mvLogisticDelBy_id)."', 
			'".$this->DateDelSched."', 
			'".$this->DateDelSched2."', 
			'".$this->Escape($this->mvLogisticSKU_id)."', 
			'".$this->Escape($this->SKU)."', 
			'".$this->Escape($this->Container)."', 
			'".$this->Escape($this->WO)."', 
			'".$this->DateShopIn."', 
			'".$this->DateShopOutSched."', 
			'".$this->DateShopOutActual."', 
			'".$this->Escape($this->Weight)."', 
			'".$this->Escape($this->lxv)."', 
			'".$this->Escape($this->AlertWeightDimension)."', 
			'".$this->Escape($this->PUCityYard)."', 
			'".$this->Escape($this->PUProv)."', 
			'".$this->PUDateSched."', 
			'".$this->PUDateActual."', 
			'".$this->Escape($this->DelCityYard)."', 
			'".$this->Escape($this->DelProv)."', 
			'".$this->DelDateSched."', 
			'".$this->DelDateActual."', 
			'".$this->Escape($this->mvLogisticServiceType_id)."', 
			'".$this->Escape($this->CarrierName)."', 
			'".$this->Escape($this->CarrierQuote)."', 
			'".$this->Escape($this->CarrierNotes)."' )";
		}
		$insertId = Database::InsertOrUpdate($this->pog_query, $connection);
		if ($this->mvlogisticjobId == "")
		{
			$this->mvlogisticjobId = $insertId;
		}
		return $this->mvlogisticjobId;
	}
	*/
	
	/**
	* Clones the object and saves it to the database
	* @return integer $mvLogisticJob_id
	*/
	function SaveNew()
	{
		$this->mvlogisticjobId = '';
		return $this->Save();
	}
	
	
	/**
	* Deletes the object from the database
	* @return boolean
	*/
	function Delete()
	{
		$connection = Database::Connect();
		$this->pog_query = "delete from `mvlogisticjob` where `mvlogisticjobid`='".$this->mvlogisticjobId."'";
		return Database::NonQuery($this->pog_query, $connection);
	}
	
	
	/**
	* Deletes a list of objects that match given conditions
	* @param multidimensional array {("field", "comparator", "value"), ("field", "comparator", "value"), ...} 
	* @param bool $deep 
	* @return 
	*/
	function DeleteList($fcv_array)
	{
		if (sizeof($fcv_array) > 0)
		{
			$connection = Database::Connect();
			$pog_query = "delete from `mvlogisticjob` where ";
			for ($i=0, $c=sizeof($fcv_array); $i<$c; $i++)
			{
				if (sizeof($fcv_array[$i]) == 1)
				{
					$pog_query .= " ".$fcv_array[$i][0]." ";
					continue;
				}
				else
				{
					if ($i > 0 && sizeof($fcv_array[$i-1]) !== 1)
					{
						$pog_query .= " AND ";
					}
					if (isset($this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes']) && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'SET')
					{
						$pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." '".$this->Escape($fcv_array[$i][2])."'";
					}
					else
					{
						$pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." '".$fcv_array[$i][2]."'";
					}
				}
			}
			return Database::NonQuery($pog_query, $connection);
		}
	}
        
        
        function mySave()
        {
               $dal_class = $this->dal_class->getInstance();
            
               
               $conn = $dal_class->dbconnect();
                        
	       $tsql = "insert into mvLogisticJobs (mvLogisticOrder_id, mvLogisticStatus_id, DateCreditApproved, 
                                        FreightAllocated, DelInfo, OffloadRequired, mvLogisticDelBy_id, DateDelSched, 
                                        DateDelSched2, mvLogisticSKU_id, SKU, Container, WO, DateShopIn, DateShopOutSched, DateShopOutActual, 
                                        Weight, [L x W x H], AlertWeightDimension, PUCityYard, PUProv, PUDateSched, PUDateActual, DelCityYard, 
                                        DelProv, DelDateSched, DelDateActual, mvLogisticServiceType_id, CarrierName, CarrierQuote, CarrierNotes ) values (
			?,
			?,
			?, 
			?, 
			?, 
			?, 
			?, 
			?, 
			?, 
			?, 
			?, 
			?,
			?,
			?,
			?, 
			?, 
			?, 
			?, 
			?, 
			?, 
			?, 
			?, 
			?, 
			?, 
			?, 
			?, 
			?, 
			?, 
			?, 
			?,
			?)";
               

                

                /* Prepare and execute the statement. */
                $insertReview = sqlsrv_prepare($conn, $tsql, 
                        array(
                            &$this->mvLogisticOrder_id, 
                            &$this->mvLogisticStatus_id, 
                            &$this->DateCreditApproved, 
                            &$this->FreightAllocated, 
                            &$this->DelInfo, 
                            &$this->OffloadRequired, 
                            &$this->mvLogisticDelBy_id, 
                            &$this->DateDelSched, 
                            &$this->DateDelSched2, 
                            &$this->mvLogisticSKU_id, 
                            &$this->SKU, 
                            &$this->Container, 
                            &$this->WO, 
                            &$this->DateShopIn, 
                            &$this->DateShopOutSched, 
                            &$this->DateShopOutActual, 
                            &$this->Weight, 
                            null, 
                            &$this->AlertWeightDimension, 
                            &$this->PUCityYard, 
                            &$this->PUProv, 
                            &$this->PUDateSched, 
                            &$this->PUDateActual, 
                            &$this->DelCityYard, 
                            &$this->DelProv, 
                            &$this->DelDateSched, 
                            &$this->DelDateActual, 
                            &$this->mvLogisticServiceType_id, 
                            &$this->CarrierName, 
                            &$this->CarrierQuote, 
                            &$this->CarrierNotes
                        
                        
                ));

                
                if( $insertReview === false )
                    { return sqlsrv_errors(); }
                    

                /* By default, all stream data is sent at the time of query execution. */
                if( sqlsrv_execute($insertReview) === false )
                    { return sqlsrv_errors();  }
                
                return true;
                    
        }
        
        
        function myUpdate($id)
        {
                $dal_class = $this->dal_class->getInstance();
            
               
                $conn = $dal_class->dbconnect();
                        
                //target: field = ?,
                  $tsql = "UPDATE mvLogisticJobs SET 
                             mvLogisticOrder_id = ?,
                             mvLogisticStatus_id = ?,
                             DateCreditApproved = ?,
                             FreightAllocated = ?,
                             DelInfo = ?,
                             OffloadRequired = ?,
                             mvLogisticDelBy_id = ?,
                             DateDelSched = ?,
                             
                             DateDelSched2 = ?,
                             mvLogisticSKU_id = ?,
                             SKU = ?,
                             Container = ?,
                             WO = ?,
                             DateShopIn = ?,
                             DateShopOutSched = ?,
                             DateShopOutActual = ?,
    
                             Weight = ?,
                             [L x W x H] = ?, 
                             AlertWeightDimension = ?,
                             PUCityYard = ?,
                             PUProv = ?,
                             PUDateSched = ?,
                             PUDateActual = ?,
                             DelCityYard = ?,

                             DelProv = ?,
                             DelDateSched = ?,
                             DelDateActual = ?,
                             mvLogisticServiceType_id = ?,
                             CarrierName = ?,
                             CarrierQuote = ?,
                             CarrierNotes = ?
                        WHERE mvLogisticJob_id = ?";
                        
                  $my_id = $id;
                  if(sqlsrv_query($conn, $tsql, array(
                                //&$this->Date, 
                                &$this->mvLogisticOrder_id, 
                                &$this->mvLogisticStatus_id, 
                                &$this->DateCreditApproved, 
                                &$this->FreightAllocated, 
                                &$this->DelInfo, 
                                &$this->OffloadRequired, 
                                &$this->mvLogisticDelBy_id, 
                                &$this->DateDelSched, 
                                &$this->DateDelSched2, 
                                &$this->mvLogisticSKU_id, 
                                &$this->SKU, 
                                &$this->Container, 
                                &$this->WO, 
                                &$this->DateShopIn, 
                                &$this->DateShopOutSched, 
                                &$this->DateShopOutActual, 
                                &$this->Weight, 
                                &$this->lxv, 
                                &$this->AlertWeightDimension, 
                                &$this->PUCityYard, 
                                &$this->PUProv, 
                                &$this->PUDateSched, 
                                &$this->PUDateActual, 
                                &$this->DelCityYard, 
                                &$this->DelProv, 
                                &$this->DelDateSched, 
                                &$this->DelDateActual, 
                                &$this->mvLogisticServiceType_id, 
                                &$this->CarrierName, 
                                &$this->CarrierQuote, 
                                &$this->CarrierNotes,
                      
                                &$my_id
                          )) === false )
                    return sqlsrv_errors();
                         
                  return true;
                
                   
        }
        
        function myDelete($id)
        {
                $dal_class = $this->dal_class->getInstance();
            
               
                $conn = $dal_class->dbconnect();
                        
                
                  $tsql = "DELETE FROM mvLogisticJobs
                           WHERE mvLogisticJob_id = ?";
                        
                  $my_id = $id;
                  if( sqlsrv_query($conn, $tsql, array(
                                &$my_id
                          )) === false )
                    return false; 
                         
                  
                  return true;
        }
        
        
        public function findById($id)
        {
            $dal_class = $this->dal_class->getInstance();
            
               
            $conn = $dal_class->dbconnect();
            
            
            $tsql = "SELECT * FROM mvLogisticJobs
                     WHERE mvLogisticJob_id = $id";

            $result = $dal_class->query($tsql);

            if(!$result || !$result[0]) {
                return false;
                //die('No such object.');
                
            }
            return $result[0];
        }
}
?>