<?php 
namespace Movit\TestBundle\Services;

class Filter 
{
   
    private function add_where_clause(&$qry)
    {
            if(strpos($qry, 'WHERE') === false) {
                $qry .= ' WHERE ';
                return 1;
            }
            return 0;
    }
    
    /*
     * v 1.0
     * Author: Denis Nizetic
     * Purpose: modify query according to the applied filters
     * Usage/dependency:
     *              - the function requires the filter form to have fileds in this format:
     *                  - date fields -> have to be 2, one ending with _from, the other with _to, one after the other
     *                  - numeric fields -> have to be 2, one ending with _from, the other with _to, one after the other
     * Arguments:
     *              $value_from/$value_to => the two form fields that represent a start/end value (only NUMERIC/DATE fileds atm)
     *              $fld_name => name of the field in DATABASE
     *              $fld_name_frontend => name displayed in VIEW (on filter applied messages)
     *              $qry => query being modified, reference
     *              $is_boolean => check this true when passing a boolean form field. Specify $value_from and $value_to as NULL
     *                             in this case.
     *              
     */
    private function modify_query($value_from, $value_to, $fld_name, $fld_name_frontend, &$qry, $is_boolean = false, $bool_value = NULL, $is_text = false, $text = NULL)
    {
            
            $flag = $this->add_where_clause($qry);
            
            if($is_text) {
                if(!$flag) $qry .= ' AND ';
                $qry .= "($fld_name LIKE '%$text%')";
                return "$fld_name_frontend contains '$text'";
            }
            
            //boolean fields
            if($is_boolean) {
                if(!$flag) $qry .= ' AND ';
                $qry .= "$fld_name = '$bool_value'";
                $msg_text = ($bool_value === "1") ? 'Yes' : 'No';
                return "$fld_name_frontend = $msg_text";
            } 
            
           
            //if DateTime type, convert to proper format
            if(is_object($value_from))
                $value_from = $value_from->format('Y-m-d');
            if(is_object($value_to))
                $value_to = $value_to->format('Y-m-d');
            
            //numeric and date fields
            if($value_from != NULL && $value_to == NULL) {
                    if(!$flag) $qry .= ' AND ';
                    $qry .= "$fld_name >= '$value_from'";
                    $msg = "$fld_name_frontend >= $value_from";
            } else if($value_from == NULL && $value_to != NULL) {
                    if(!$flag) $qry .= ' AND ';
                    $qry .= "$fld_name <= '$value_to'";
                    $msg = "$fld_name_frontend <= $value_to";
            } else {
                if($value_from <= $value_to) {
                    if(!$flag) 
                        $qry .= ' AND ';
                    $qry .= "$fld_name >= '$value_from' AND $fld_name <= '$value_to'";
                    $msg = "$value_from <= $fld_name_frontend <= $value_to";
                } else { //in case we added WHERE, and the if clause above fails (that would leave the query ending in WHERE; )
                    $qry .= ' 1=1 ';
                    $msg = '';
                }
            }

            
            return $msg;
     }
    
     //returns all Applied Filters message
     /*
      * Author: Denis N.
      * Version: 1.0
      * Parameter: form_data
      * Dependency:
      *     The filter form has to satisfy following conditions:
      *         - each numeric filter has to have 2 fields, one ending with _from, other with _to
      *         - they need to be one after the other when passed from a post request 
      * 
      */
     public function applyFilters($form_data, &$orders_query)
     {
         
           $value_from = $value_to = NULL;
           $applied_filters_messages = array();
           
           foreach($form_data as $key => $dat) {
                    
               
                    if($dat === "1__choice__" || $dat === "0__choice__") { //triple === IMPORTANT! this tells the loop it's a boolean type, not integer.
                        $applied_filters_messages[] = $this->modify_query(NULL, NULL, $key, $key, $orders_query, true, $dat);
                        continue;
                    }
                    
                    if(substr($key, -5) == '_from') {
                        $value_from = $dat;
                        continue;
                    } else if(substr($key, -3) == '_to') {
                        $value_to = $dat;
                    } else if($dat) { //it's a text field
                        $applied_filters_messages[] = $this->modify_query(NULL, NULL, $key, $key, $orders_query, false, NULL, true, $dat);
                        continue;
                    }
                    
                    $fld_name = substr($key, 0, -3); //remove the _to from the key to get table column name
                    if($value_from || $value_to)
                        $applied_filters_messages[] = $this->modify_query($value_from, $value_to, $fld_name, 
                                $fld_name == 'mvLogisticOrder_id' ? 'Order' : $fld_name, $orders_query);
           }
           
           return $applied_filters_messages;
     }
}
?>