<?php

namespace Movit\TestBundle\Services;


use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\SecurityContext;

define('DB_HOST', 'AMAZONA-R6BUP07\EC2SQLEXPRESS');
define('DB_USER', 'CWLogistics');
define('DB_PASSWORD', '(WL0g1st1(5');
define('DB_DB', 'ContainerWest');


class DAL {
    
	private $conn = null;
	private static $inst = null;
	
	public function __construct() {
	}
	
	public static function getInstance() {
		if(!self::$inst) {
			self::$inst = new DAL();		
		}
		return self::$inst;
	}
	
	public function dbconnect() {
		$connection_info = array("UID"=>DB_USER,
		                         "PWD"=>DB_PASSWORD,
		                         "Database"=>DB_DB);
		$conn = sqlsrv_connect(DB_HOST, $connection_info);
		if($conn === false) {
			echo "Unable to connect to sqlsrv.</br>";
			die(print_r(sqlsrv_errors(), true));
		}
		
		$this->conn = $conn;
		return $conn;
	}

	//parameters: $sql, [param1, param2, ...]
	public function query() {
		$this->dbconnect();
		
		$params = func_get_args();
		$sql = array_shift($params);
		
		$res = sqlsrv_query($this->conn, $sql, $params);

		if(!$res) {
			return false;
		}
		if(strpos($sql, 'SELECT') === false) {
			return true;
		}

		$results = array();

		while($row = sqlsrv_fetch_array($res)) {

			$result = new DALQueryResult();

			foreach($row as $k=>$v) {
				$result->set($k, $v);
			}

			$results[] = $result;
		}
		return $results;
	}
        
        private function selectFirstRow($table, $field)
        {
            $dal_class = $this->getInstance();
            
            $slct_1_row = "SELECT TOP 1 $field 
                    FROM 
                    ( 
                        SELECT TOP 1 $field 
                        FROM $table
                    ) sub ";
            
            $data = $dal_class->query($slct_1_row);
            
            return $data[0]->get($field);
            
        }
        
        public function loadFixtures()
        {
            $conn = $this->dbconnect();
            
            
            //get foreign keys first
            //status, del_by, sku_id, jobserv_type
            $status = $this->selectFirstRow('mvLogisticStatuses', 'mvLogisticStatus_id');
            
            
            var_dump($status);
            
            
            $del_by = $this->selectFirstRow('mvLogisticDelBy', 'mvLogisticDelBy_id');
            var_dump($del_by);
            //$sku_id = $this->selectFirstRow('mvLogisticSKUs', 'Flag'); //add later, no data yet
            //job_serv_type not available yet
            
            
            
            
            $tsql = "TRUNCATE TABLE mvLogisticOrders;
                
                INSERT INTO mvLogisticOrders (
                    Date,
                    DateCommitted,
                    Salesperson_id,
                    TruckAllocatedTotal, 
                    TruckingCostTotal,
                    TruckingCostPercent
                ) VALUES (
                    '20110221',
                    '20110222',
                    NULL,
                    25,
                    50,
                    2.5
                ), (
                    '20110222',
                    '20110224',
                    NULL,
                    212,
                    5123,
                    2.53
                )
                ;";
            
            
            
            //nulls => Foreign keys
            $tsql .= "TRUNCATE TABLE mvLogisticJobs;
                INSERT INTO mvLogisticJobs (
                    mvLogisticOrder_id,
                    mvLogisticStatus_id,
                    DateCreditApproved,
                    FreightAllocated,
                    DelInfo,
                    OffloadRequired,
                    mvLogisticDelBy_id,
                    DateDelSched,
                    DateDelSched2,
                    mvLogisticSKU_id,
                    SKU,
                    Container,
                    WO,
                    DateShopIn,
                    DateShopOutSched,
                    DateShopOutActual,
                    Weight,
                    [L x W x H],
                    AlertWeightDimension,
                    PUCityYard,
                    PUProv,
                    PUDateSched,
                    PUDateActual,
                    DelCityYard,
                    DelProv,
                    DelDateSched,
                    DelDateActual,
                    mvLogisticServiceType_id,
                    CarrierName,
                    CarrierQuote,
                    CarrierNotes
                ) VALUES (
                    NULL,
                    $status,
                    '20110221',
                    1850,
                    0,
                    1,
                    $del_by,
                    '20110221',
                    '20110221',
                    NULL,
                    'Test SKU',
                    'Test Container',
                    'Test WO',
                    '20120822',
                    '20120823',
                    '20120824',
                    78,
                    'Test lxwxh',
                    1,
                    'Pucity test',
                    'd',
                    '20120818',
                    '20120821',
                    'DelCityYard test',
                    'd',
                    '20120818',
                    '20120821',
                    NULL,
                    'Carrier Name',
                    35,
                    'Carrier notes'
                ), (
                    NULL,
                    $status,
                    '20110223',
                    1555,
                    1,
                    0,
                    $del_by,
                    '20110219',
                    '20110225',
                    NULL,
                    'Test again SKU',
                    'Test again Container',
                    'Test again WO',
                    '20120820',
                    '20120828',
                    '20120823',
                    52,
                    'Test again lxwxh',
                    0,
                    'Pucity again test',
                    'd',
                    '20120815',
                    '20120814',
                    'DelCityYard again test',
                    'd',
                    '20120813',
                    '20120828',
                    NULL,
                    'Carrier again Name',
                    302,
                    'Carrier again notes'
            
                )
                ;";
            
            
            return sqlsrv_query($conn, $tsql);
        }
        
	
	function __destruct() {
		if($this->conn) {
			@sqlsrv_close($this->conn);
		}
	}
}


class DALQueryResult {
	private $_results = array();

	public function set($key, $val){
		$this->_results[$key] = $val;
	}

	public function get($key) {
		if(isset($this->_results[$key])) {
			return $this->_results[$key];
		} else {
			return null;
		}
	}
}


/*
 function modifyAll($user, $pass, $host, $name)
 {
 $this->database_user = $user;
 $this->database_pass = $pass;
 $this->database_host = $host;
 $this->database_name = $name;
 }
 function modifyUser($user)
 {
 $this->database_user = $user;
 }
 function modifyPass($pass)
 {
 $this->database_pass = $pass;
 }
 function modifyHost($host)
 {
 $this->database_host = $host;
 }
 function modifyName($name)
 {
 $this->databse_name = $name;
 }

 function connect()
 {
 $this->database_info = array($this->database_user, $this->database_pass,$this->databse_name);
 $this->database_link = sqlsrv_connect($this->database_host, $this->database_info);
 if ($this->database_link === false){die(print_r( sqlsrv_errors(), true));}
 return $this->database_link;
 }

 */?>

