<?php $view->extend('::salesview.html.php') ?>
<p>Orders table</p>


<div id="mvLogisticOrders"></div>


<a href="<?php echo $view['router']->generate('order_new') ?>">New Order</a>




<p>Jobs table</p>
<div id="mvLogisticJobs"></div>
<a href="<?php echo $view['router']->generate('job_new') ?>">New Job</a>
<?php /* ?>
<table class="sortable resizable editable" id="tablekit-table-2">
    <thead>
	<tr>
            <th id="job" class="sortfirstdesc sortcol sortdesc">Job</th> <?php //mvLogisticJob_id in table, primary key ?>
            <th id="order_jobs" class="sortcol">Order</th> <?php //mvLogisticOrder_id, FK ?>
            <th id="status" class="sortcol">Status</th> <?php //FK to status, possible values: B23 - B26 in Logistic worksheet ?>
            
            
            <th id="credit_approved" class="sortcol">CreditApproved</th>
            <th id="fr_alloc" class="sortcol">FreightAllocated</th>
            <th id="del_info" class="sortcol">DelInfo</th>
            <th id="offload_req" class="sortcol">OffloadRequired</th>
            <th id="delby" class="sortcol">DelBy</th> <?php //possible values in J7 ?>
            
            <th id="deldate_sched" class="sortcol">DelDateSched</th>
            <th id="deldate_sched2" class="sortcol">DelDateSched2</th> <?php //null unless value in DelBy is set to a value between range (delby is end) ?>
            <th id="sku_id" class="sortcol">SKU_id</th> <?php //dropdown to model "SKU", SKU sheet, column C ?>
            <th id="sku_" class="sortcol">SKU</th> <?php //dropdown to model "SKU", SKU sheet, column C ?>
            <th id="container_id" class="sortcol">Container</th>
            <th id="wo" class="sortcol">WO</th>
            
            <th id="shop_in" class="sortcol">ShopIn</th>
            <th id="shipout_sched" class="sortcol">ShipOutSched</th>
            <th id="shipout_act" class="sortcol">ShopOutAct</th>
            
           
            
            <th id="weight" class="sortcol">Weight</th>
            <!--<th id="lwh" class="sortcol">L x W x H</th>
            <th id="alert" class="sortcol">Alert per weight or dimensions</th>
            
            
            
            <?php //PickUp ?>
            
            <th id="pucity_yard" class="sortcol">Origin Point</th> <?php //reference values from E2 dropdown, Yards worksheet ?>
            <th id="pu_pr" class="sortcol">PuProv</th>
            
            <th id="pu_sched" class="sortcol">PuSched</th>
            <th id="pu_actual" class="sortcol">PuActual</th>
            
            
            <?php //Delivery ?>
            <th id="delcity_yard" class="sortcol">Delivery Point</th> <?php //reference values from E2 dropdown, Yards worksheet ?>
            <th id="del_pr" class="sortcol">DelPr</th>
            <th id="del_sched" class="sortcol">Delsched</th>
            <th id="del_actual" class="sortcol">DelActual</th>
            -->
            
            <?php //Job Carrier ?>
            <th id="job_serv_type" class="sortcol">JobServType</th> <?php //mvJobServType_id, Sales worksheet, J51 - K51, reference values from mvJobServTypes ?>
       
            
            <th id="carrier_name" class="sortcol">carrierName</th> <?php  // mvCarriers table (reference) ?>
            <th id="carrier_quote" class="sortcol">carrierQuote</th>
            <th id="notes" class="sortcol">carrierNotes</th>
        </tr>
    </thead>
    
    <tbody>
        
        <?php foreach($jobs as $job): ?>
          <tr>
              <td><?php echo $job->get('mvLogisticJob_id') ?></td>
              <td><?php echo $job->get('mvLogisticOrder_id') ?></td>
              <td><?php echo $job->get('mvLogisticStatus_id') ?></td>
              
              
              
              <td><?php echo $job->get('DateCreditApproved')->format("Y-m-d") ?></td>
              <td><?php echo $job->get('FreightAllocated') ?></td>
              <td style="<?php echo !$job->get('DelInfo') ? 'background-color: yellow' : '' ?>"><?php echo $job->get('DelInfo') ? 'Yes' : 'No' ?></td>
              <td><?php echo $job->get('OffloadRequired') ? 'Yes' : 'No' ?></td>
              <td><?php echo $job->get('mvLogisticDelBy_id') ?></td>
              
              
              
              <td><?php echo $job->get('DateDelSched')->format("Y-m-d") ?></td>
              <td><?php echo $job->get('DateDelSched2')->format("Y-m-d") ?></td>
              
              <td><?php echo $job->get('mvLogisticSKU_id') ?></td>
              <td><?php echo $job->get('SKU') ?></td>
              <td><?php echo $job->get('Container') ?></td>
              <td><?php echo $job->get('WO') ?></td>
              <td><?php echo $job->get('DateShopIn')->format("Y-m-d") ?></td>
              <td><?php echo $job->get('DateShopOutSched')->format("Y-m-d") ?></td>
              
              <td><?php echo $job->get('DateShopOutActual')->format("Y-m-d") ?></td>
              <td><?php echo $job->get('Weight') ?></td>
              <!--<td><?php echo $job->get('L x W x H') ?></td>
              <td style="<?php echo $job->get('AlertWeightDimension') ? 'background-color: yellow' : '' ?>"><?php echo $job->get('AlertWeightDimension') ? 'Yes' : 'No' ?></td>
              <td><?php echo $job->get('PUCityYard') ?></td>
              <td><?php echo $job->get('PUProv') ?></td>       
              
              
              <td><?php echo $job->get('PUDateSched')->format("Y-m-d") ?></td>
              <td><?php echo $job->get('PUDateActual')->format("Y-m-d") ?></td>
              <td><?php echo $job->get('DelCityYard') ?></td>
              <td><?php echo $job->get('DelProv') ?></td>
              <td><?php echo $job->get('DelDateSched')->format("Y-m-d") ?></td>
              <td><?php echo $job->get('DelDateActual')->format("Y-m-d") ?></td>  -->
              
              
              <td><?php echo $job->get('mvLogisticServiceType_id') ?></td>
              <td><?php echo $job->get('CarrierName') ?></td>
              <td><?php echo $job->get('CarrierQuote') ?></td>
              <td><?php echo $job->get('CarrierNotes') ?></td>  
              
              <!-- <td id="customer" class="sortcol">Customer</td> -->
          </tr>
        <?php endforeach; ?>
        
        <?php //echo genTestdata(33, 10); ?>
    </tbody>
</table>
*/ ?> 

<p>Jobs Filter Form</p>
<form id="jobs_filter_form" onsubmit="return false" action="<?php echo $view['router']->generate('index_ajax') ?>" method="post" <?php echo $view['form']->enctype($job_f_form) ?> >
    <?php echo $view['form']->widget($job_f_form) ?>

    
    
    <input type="button" id="jobs_button" value="Apply Filters"/>
    <input type="reset" value="Clear input"/>
</form>

  <script src="<?php echo $view['assets']->getUrl('bundles/movittest/js/orders_ajax.js') ?>"></script>
