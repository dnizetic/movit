<?php 
    /*
     * Name: Generate Test Data
     * Purpose: Generate random hardcoded test data
     * Note: Remove after db
     */
   function genTestdata($num_columns, $num_rows)
   {
       $str = "";
       for($i = 0; $i < $num_rows; ++$i)
       {
           $str .= "<tr>";
           for($j = 0; $j < $num_columns; ++$j)
           {
               $str .= "<td>" . "random_" . mt_rand(0, 1000) . "</td>";
           }
           $str .= "</tr>";
       }
       return $str;
   }
?>