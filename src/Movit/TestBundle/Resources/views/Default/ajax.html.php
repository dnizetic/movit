<?php if(!empty($msgs)): ?>
    <p>Applied filters: 
      <ul>
        <?php foreach($msgs as $msg): ?>
          <li><?php echo $msg ?></li>
        <?php endforeach; ?>
      </ul>
    </p>
<?php endif; ?>
<form id="orders_filter_form" onsubmit="return false" action="<?php echo $view['router']->generate('index_ajax') ?>" method="post" <?php echo $view['form']->enctype($form) ?> >
<table class="sortable" id="my_table<?php echo $timestamp ?>">
    <thead>
        
        <?php echo $view->render('MovitTestBundle:Default:tableHeader.html.php', array('table' => $table, 'form' => $form)) ?>

    </thead>


    <tfoot>
    </tfoot>
    
    <tbody>
        
        
            <?php foreach($results as $result): ?>
              <tr>
                  <?php echo $view->render('MovitTestBundle:Default:tableBody.html.php', array('table' => $table, 
                                'job' => $result, 'order' => $result, 'statuses' => $statuses)) ?>
              </tr>
        <?php endforeach; ?>
          
    </tbody>
    
    
</table>      
  </form>