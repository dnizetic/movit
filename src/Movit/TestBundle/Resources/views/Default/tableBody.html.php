        <?php if($table == 'mvLogisticOrders'): ?>
              <td><?php echo $order->get('mvLogisticOrder_id') ?></td>
              <td><?php echo ($order->get('Date')) ? $order->get('Date')->format('Y-m-d') : NULL ?></td>
              <td><?php echo ($order->get('DateCommitted')) ? $order->get('DateCommitted')->format('Y-m-d') : NULL ?></td>
              <td><?php echo $order->get('Salesperson_id') ? $order->get('Salesperson_id') : '<span style="color: red;">n/a</span>' ?></td>
              <td><?php echo $order->get('TruckAllocatedTotal') ? $order->get('TruckAllocatedTotal') : '<span style="color: red;">n/a</span>' ?></td>
              <td><?php echo $order->get('TruckAllocatedTotal') ? $order->get('TruckingCostTotal') : '<span style="color: red;">n/a</span>' ?></td>
              <td><?php echo $order->get('TruckingCostPercent') ? $order->get('TruckingCostPercent') : '<span style="color: red;">n/a</span>' ?></td>
              <!--<td><?php //echo $order->get('Customer_id') ?></td>-->
              <td>
                  <a href="<?php echo $view['router']->generate('order_edit', array('id' =>  $order->get('mvLogisticOrder_id'))) ?>">Edit</a>/
                  <a href="<?php echo $view['router']->generate('order_delete', array('id' => $order->get('mvLogisticOrder_id'))) ?>">Delete</a>/
                  <a href="<?php echo $view['router']->generate('job_new', array('id' =>  $order->get('mvLogisticOrder_id'))) ?>">Add Job</a>
              </td>
              
        <?php elseif($table == 'mvLogisticJobs'): ?>
              
              
              <td><?php echo $job->get('mvLogisticJob_id') ?></td>
              <td><?php echo $job->get('mvLogisticOrder_id') ?></td>

              <!--<td><?php //echo $job->get('mvLogisticStatus_id') ?></td>-->
              <td><?php echo $statuses[$job->get('mvLogisticStatus_id')] ?></td>
              <!--
              <td><?php echo $job->get('DateCreditApproved')->format("Y-m-d") ?></td>
              <td><?php echo $job->get('FreightAllocated') ?></td>
              <td style="<?php echo !$job->get('DelInfo') ? 'background-color: yellow' : '' ?>"><?php echo $job->get('DelInfo') ? 'Yes' : 'No' ?></td>
              <td><?php echo $job->get('OffloadRequired') ? 'Yes' : 'No' ?></td>
              <td><?php echo $job->get('mvLogisticDelBy_id') ?></td>
              
              <td><?php echo $job->get('DateDelSched')->format("Y-m-d") ?></td>
              <td><?php echo $job->get('DateDelSched2')->format("Y-m-d") ?></td>
              
              <td><?php echo $job->get('mvLogisticSKU_id') ?></td>
              <td><?php echo $job->get('SKU') ?></td>
              <td><?php echo $job->get('Container') ?></td>
              <td><?php echo $job->get('WO') ?></td>
              <td><?php echo $job->get('DateShopIn')->format("Y-m-d") ?></td>
              <td><?php echo $job->get('DateShopOutSched')->format("Y-m-d") ?></td>
              
              <td><?php echo $job->get('DateShopOutActual')->format("Y-m-d") ?></td>
              <td><?php echo $job->get('Weight') ?></td>
              <td><?php echo $job->get('LxWxH') ?></td>
              <td style="<?php echo $job->get('AlertWeightDimension') ? 'background-color: yellow' : '' ?>"><?php echo $job->get('AlertWeightDimension') ? 'Yes' : 'No' ?></td>
              
        -->      <td class='puDetails' onclick="$(<?php echo $job->get('mvLogisticJob_id') ?>puDetails).style.backgroundcolor= '#000';"><b><?php echo $job->get('PUCityYard') ?></b>
              		<div id="<?php echo $job->get('mvLogisticJob_id') ?>puDetails">
              		<?php echo $job->get('PUProv') ?><br/>
              		Scheduled Pick Up Date:
              		<?php echo $job->get('PUDateSched')->format("Y-m-d") ?><br/>
              		Actual Pick Up Date:
              		<?php echo $job->get('PUDateActual')->format("Y-m-d") ?>
              		</div>
              </td>
              <!--<td><?php echo $job->get('PUProv') ?></td>       
              <td><?php echo $job->get('PUDateSched')->format("Y-m-d") ?></td>
              <td><?php echo $job->get('PUDateActual')->format("Y-m-d") ?></td>-->
              <td><b><?php echo $job->get('DelCityYard') ?></b>
              	    <div>
              		<?php echo $job->get('DelProv') ?><br/>
              		Scheduled Pick Up Date:
              		<?php echo $job->get('DelDateSched')->format("Y-m-d") ?><br/>
              		Actual Pick Up Date:
              		<?php echo $job->get('DelDateActual')->format("Y-m-d") ?>
              		</div>
              </td>
              <!--<td><?php echo $job->get('DelProv') ?></td>
              <td><?php echo $job->get('DelDateSched')->format("Y-m-d") ?></td>
              <td><?php echo $job->get('DelDateActual')->format("Y-m-d") ?></td>-->  
              
              
              <!--<td><?php echo $job->get('mvLogisticServiceType_id') ?></td>-->
              <td><?php echo $job->get('CarrierName') ?></td>
              <td><?php echo $job->get('CarrierQuote') ?></td>
              <td><?php echo $job->get('CarrierNotes') ?></td>  
              
              <!-- <td id="customer" class="sortcol">Customer</td> -->
              
              <td><a href="<?php echo $view['router']->generate('job_edit', array(
                        'id' => $job->get('mvLogisticJob_id'))) ?>">Edit</a>/
                  <a href="<?php echo $view['router']->generate('job_delete', array(
                        'id' => $job->get('mvLogisticJob_id'))) ?>">Delete</a>
              </td>
        <?php endif; ?>
