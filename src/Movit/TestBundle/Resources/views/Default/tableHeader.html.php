        <?php if($table == 'mvLogisticOrders'): ?>
        <tr>
            <th colspan="3"><h3>Orders Managment</h3></th>
        </tr>
        <tr>
                <th>
                    <input type="button" id="show_button" name="show_filters" value="Show Filters" style="width: 100px"/>
                    <input style="display: none; width: 100px" type="button" id="hide_button" name="hide_filters" value="Hide Filters" />
                </th>
                <th>
                    <?php //echo $view['form']->rest($form) ?>
                    <input type="button" id="filter_button" value="Apply Filters" style="width: 100px"/>
                    
                </th>
                <th>
                    <input type="button" value="Clear input" id="clear_button" style="width: 100px"/>
                </th>
        </tr>
        <tr id="filter_row" style="display: none">
            
                <?php //echo $view['form']->widget($order_f_form) ?>
                <th id="order_filter_id"  class="activate">
                    From:<?php echo $view['form']->widget($form['mvLogisticOrder_id_from']) ?><br>
                    &nbsp;&nbsp;&nbsp;&nbsp;To:<?php echo $view['form']->widget($form['mvLogisticOrder_id_to']) ?>
                
                </th>
                <th id="order_filter_date"  class="activate">
                    From:<?php echo $view['form']->widget($form['Date_from']) ?>
                    &nbsp;&nbsp;&nbsp;&nbsp;To:<?php echo $view['form']->widget($form['Date_to']) ?>
                </th>
                <th id="order_filter_date_commited"  class="activate">
                    From:<?php echo $view['form']->widget($form['DateCommitted_from']) ?>
                    &nbsp;&nbsp;&nbsp;&nbsp;To:<?php echo $view['form']->widget($form['DateCommitted_to']) ?>
                </th> 
                <th id="order_filter_salesperson_id"  class="activate">
                    From:<?php echo $view['form']->widget($form['Salesperson_id_from']) ?><br>
                    &nbsp;&nbsp;&nbsp;&nbsp;To:<?php echo $view['form']->widget($form['Salesperson_id_to']) ?>
                </th>
                <th id="order_filter_truck_allocated_total"  class="activate">
                    From:<?php echo $view['form']->widget($form['TruckAllocatedTotal_from']) ?><br>
                    &nbsp;&nbsp;&nbsp;&nbsp;To:<?php echo $view['form']->widget($form['TruckAllocatedTotal_to']) ?>
                </th>
                <th id="order_filter_truck_cost_total"  class="activate">
                    From:<?php echo $view['form']->widget($form['TruckingCostTotal_from']) ?><br>
                    &nbsp;&nbsp;&nbsp;&nbsp;To:<?php echo $view['form']->widget($form['TruckingCostTotal_to']) ?>
                </th>
                <th id="order_filter_truck_cost_percent"  class="activate">
                    From:<?php echo $view['form']->widget($form['TruckingCostPercent_from']) ?><br>
                    &nbsp;&nbsp;&nbsp;&nbsp;To:<?php echo $view['form']->widget($form['TruckingCostPercent_to']) ?>
                </th>

          
            
        </tr>
	<tr>
            <th id="order_orders" class="sortfirstdesc sortcol sortdesc">Order</th>
            <th id="date_orders" class="sortcol">Date</th>
            <th id="date_commit" class="sortcol">DateCommitted</th>
            <th id="salesperson" class="sortcol">Requester ID</th>
            <th id="ak9_orders" class="sortcol">TruckAllocatedTotal</th> <?php //SUM of FreightAllocat in Jobs ?>
            <th id="al9_orders" class="sortcol">TruckingCostTotal</th> <?php //SUM of CarrierQuote from Jobs ?>
            <th id="am9_orders" class="sortcol">TruckingCostPercent</th> <?php // AM9 = (1 - (AK9 / AL9))*100. ?>
            <!--<th id="customer" class="sortcol">Customer</th>-->
            <th id="actions">Actions</th>
        </tr>
        
        
        
        <?php elseif($table == 'mvLogisticJobs'): ?>
        
       <tr>
            <th colspan="3"><h3>Jobs Managment</h3></th>
        </tr>
        <tr>
                <th>
                    <input type="button" id="job_show_button" name="job_show_filters" value="Show Filters" style="width: 100px"/>
                    <input style="display: none; width: 100px" type="button" id="job_hide_button" name="job_hide_filters" value="Hide Filters" />
                </th>
                <th>
                    <?php //echo $view['form']->rest($form) ?>
                    <input type="button" id="job_button" value="Apply Filters" style="width: 100px"/>
                    
                </th>
                <th>
                    <input type="button" value="Clear input" id="job_clear_button" style="width: 100px"/>
                </th>
        </tr>
        
       <tr id="job_filter_row" style="display: none">
            
                <?php //echo $view['form']->widget($order_f_form) ?>
                <th id="job_filter_id"  class="job_activate">
                    From:<?php echo $view['form']->widget($form['mvLogisticJob_id_from']) ?><br>
                    &nbsp;&nbsp;&nbsp;&nbsp;To:<?php echo $view['form']->widget($form['mvLogisticJob_id_to']) ?>
                
                </th>
                <th id="job_filter_date"  class="job_activate">
                    From:<?php echo $view['form']->widget($form['mvLogisticOrder_id_from']) ?>
                    &nbsp;&nbsp;&nbsp;&nbsp;To:<?php echo $view['form']->widget($form['mvLogisticOrder_id_to']) ?>
                </th>
          
            
        </tr>
        
        
        
	<tr>	
            <th id="job" class="sortfirstdesc sortcol sortdesc">Job</th> <?php //mvLogisticJob_id in table, primary key ?>
            <th id="order_jobs" class="sortcol">Order</th> <?php //mvLogisticOrder_id, FK ?>
            <th id="status" class="sortcol">Status</th> <?php //FK to status, possible values: B23 - B26 in Logistic worksheet ?>
            
            <!--
            <th id="credit_approved" class="sortcol">CreditApproved</th>
            <th id="fr_alloc" class="sortcol">FreightAllocated</th>
            <th id="del_info" class="sortcol">DelInfo</th>
            <th id="offload_req" class="sortcol">OffloadRequired</th>
            <th id="delby" class="sortcol">DelBy</th> <?php //possible values in J7 ?>
            
            <th id="deldate_sched" class="sortcol">DelDateSched</th>
            <th id="deldate_sched2" class="sortcol">DelDateSched2</th> <?php //null unless value in DelBy is set to a value between range (delby is end) ?>
            <th id="sku_id" class="sortcol">SKU_id</th> <?php //dropdown to model "SKU", SKU sheet, column C ?>
            <th id="sku_" class="sortcol">SKU</th> <?php //dropdown to model "SKU", SKU sheet, column C ?>
            <th id="container_id" class="sortcol">Container</th>
            <th id="wo" class="sortcol">WO</th>
            
            <th id="shop_in" class="sortcol">ShopIn</th>
            <th id="shipout_sched" class="sortcol">ShipOutSched</th>
            <th id="shipout_act" class="sortcol">ShopOutAct</th>
            
           
            
            <th id="weight" class="sortcol">Weight</th>
            <th id="lwh" class="sortcol">L x W x H</th>
            <th id="alert" class="sortcol">Alert per weight or dimensions</th>
            
           -->
            
            <?php //PickUp ?>
            
            <th id="pucity_yard" class="sortcol resizable">Origin Point</th> <?php //reference values from E2 dropdown, Yards worksheet ?>
            <!--<th id="pu_pr" class="sortcol">PuProv</th>
            <th id="pu_sched" class="sortcol">PuSched</th>
            <th id="pu_actual" class="sortcol">PuActual</th>-->
            
            
            <?php //Delivery ?>
            <th id="delcity_yard" class="sortcol">Delivery Point</th> <?php //reference values from E2 dropdown, Yards worksheet ?>
            <!--<th id="del_pr" class="sortcol">DelPr</th>
            <th id="del_sched" class="sortcol">Delsched</th>
            <th id="del_actual" class="sortcol">DelActual</th>-->
            
            
            <?php //Job Carrier ?>
            <!--
            <th id="job_serv_type" class="sortcol">JobServType</th> <?php //mvJobServType_id, Sales worksheet, J51 - K51, reference values from mvJobServTypes ?>
			-->       
            
            <th id="carrier_name" class="sortcol">carrierName</th> <?php  // mvCarriers table (reference) ?>
            <th id="carrier_quote" class="sortcol">carrierQuote</th>
            <th id="notes" class="sortcol">carrierNotes</th>
            <th id="actions">Actions</th>
        </tr>
        
        <?php endif; ?>