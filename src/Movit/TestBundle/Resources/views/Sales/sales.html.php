<!-- src/Acme/HelloBundle/Resources/views/Hello/index.html.php -->
<?php $view->extend('::salesview.html.php') ?>
	<script src="<?php echo $view['assets']->getUrl('bundles/movittest/js/autocomplete.js') ?>"></script>
	

<script>

	function populateTable()
	{
                var my_val = $('skus_search').value;
                alert(my_val);
                
                
		var url = 'sales/sku/ajax?size_type=' + my_val;
		
		var myAjax = new Ajax.Request(
			url, 
			{
				method: 'get', 
				onComplete: showResponse
			});
                        
		
	}

	function showResponse(originalRequest)
	{
                $('my_tbody').update(originalRequest.responseText);
	}
        
        function showForm()
        {
            $('new_city').setStyle({
              display: 'block'
            });
           $('city_msg').setStyle({
                       display: 'none'
           });
        }
        
        
        function saveCity(){

            //tablekit_ajax();
                    
            $('new_city_form').request({ //form request
              method: 'post',
              parameters: { new_city: '1' },
              onComplete: function(){
                    //hide the form
                    $('new_city').setStyle({
                       display: 'none'
                    });
                    $('city_msg').setStyle({
                       display: 'block'
                    });
              }
            })

        }


        //var typeValue = Form.getInputs('myform','radio','type').find(function(radio) { return radio.checked; }).value;
        
        document.observe("dom:loaded", function() {
          // initially hide all containers for tab content
             populateTable();
        });
</script>
        
	
	<div>
	   <form id="yardSearch" action="http://conc.ealto.me/movit/app_dev.php/hello">
               
            <input class="radio_change" type="radio" name="type" value="yty"/>Yard to Yard<br>
            <input class="radio_change" type="radio" name="type" value="ctc"/>City to City<br>
            <input class="radio_change" type="radio" name="type" value="ytc"/>Yard to City<br>
            <input class="radio_change" type="radio" name="type" value="cty"/>City to Yard<br>
            
            <!-- Queries yard info and autocompleted the yard name OK -->
            <!-- City selection must be based on table with city names and province will need to be concatenated onto the city name-->   

            <label id="orig_yard" style="display: none">
                Originating yard: </label><input style="display: none" type="text" name="origin_yard" id="query"/>
            
            <label id="orig_city" style="display: none">
                Originating city: </label><input style="display: none" type="text" name="origin_city" id="query2"/>
            
            <label id="dest_yard" style="display: none">
                Destination yard: </label><input style="display: none" type="text" name="destionation_yard" id="query3" />
            

            <label id="dest_city" style="display: none">
                Destination city: </label><input style="display: none" type="text" name="destionation_city" id="query4" />
            
            
            <a href='#' onclick='showForm(); //alert('This will pull up a form for user to fill in a request for Vlad to approve');'>Request New City</a><br/>
	    
            SKU:
                <select id="skus_search" onchange="populateTable()">
                    <option>Select one</option>
                    <?php foreach($size_types as $size_type): ?>
                        <option><?php echo $size_type ?></option>
                        <!--<option>20S</option>
			<option>20H</option>
			<option>40S</option>
			<option>40H</option>-->
                    <?php endforeach; ?>
		</select>
			
		<?php
                
		/*The SKU options needs to be populated from a table.
		 * The SKU once selected will create a new element that will contain all the
		 * containers in each yard it must provide the following information from the 
		 * table : SKU/YARD/#of Containers in Yard/cost of container and delivery cost 
		 * based on destination (include yard transfer if required).
		 * calculate total cost of delivery.
		 */ 
		?>
            
		
		</form>
            
            
            <div id="city_msg" style="display: none"><br><br>City requested successfully!<br><br></div>
            <div id="new_city" style="display: none">
                <br><br>
            New City
            <form id="new_city_form" onsubmit="saveCity(); return false;" action="<?php echo $view['router']->generate('sales_ajax') ?>" method="post" <?php echo $view['form']->enctype($cities_form) ?> >
                <?php echo $view['form']->widget($cities_form) ?>


                <input type="submit"  value="Request new city"/>
            </form>
            <br><br>
            </div>
                
            
		<table id="skus_table">
			<th>SKU</th><th>YARD</th><th># of Containers</th><th>Cost Breakdown</th><th>Yard Details</th>
                        <?php //TR's have to be ajaxed ?>
                        <tbody id="my_tbody">
                            <tr>
                                    <td><a href='#' onclick='alert("This will expand yard info on a different table.");'>20S-GH</a></td>
                                    <td>VANCOUVER Yard Code</td><td>4</td>
                                    <td>$150 (container)<br/>$140 (yard)<br/>$180 (freight)<br/></td>
                                    <td><a href='#'>Show Yard Details</a></td>
                            </tr>
                            <tr><td>20S-OM</td><td>Edmonton Yard Code</td>
                                    <td>2</td>
                                    <td>$45 (container)<br/>$78 (yard)<br/>$135 (freight)<br/>
                                    </td><td><a href='#'>Show Yard Details</a></td>
                            </tr>
                        </tbody>
		
		</table>
		Final Costs: 
	</div>
        
        
        <script>

        function showFields()
        {
                var typeValue = Form.getInputs('yardSearch','radio','type').find(function(radio) { return radio.checked; }).value;
                if(typeValue == 'yty') {
                     //hide the one that has display to block
                     $('orig_city').setStyle({
                         display: 'none'
                     });
                     $('dest_city').setStyle({
                         display: 'none'
                     });
                     $('query2').setStyle({
                         display: 'none'
                     });
                     $('query4').setStyle({
                         display: 'none'
                     });
                     
                     $('orig_yard').setStyle({
                         display: 'block'
                     });
                     $('query').setStyle({
                         display: 'block'
                     });
                     $('dest_yard').setStyle({
                         display: 'block'
                     });
                     $('query3').setStyle({
                         display: 'block'
                     });

                } else if(typeValue == 'ctc') {
                     $('orig_yard').setStyle({
                         display: 'none'
                     });
                     $('dest_yard').setStyle({
                         display: 'none'
                     });
                     $('orig_city').setStyle({
                         display: 'block'
                     });
                     $('dest_city').setStyle({
                         display: 'block'
                     });
                     
                     
                     $('query').setStyle({
                         display: 'none'
                     });
                     $('query3').setStyle({
                         display: 'none'
                     });
                     $('query2').setStyle({
                         display: 'block'
                     });
                     $('query4').setStyle({
                         display: 'block'
                     });
                } else if(typeValue == 'ytc') {
                     $('orig_yard').setStyle({
                         display: 'block'
                     });
                     $('dest_yard').setStyle({
                         display: 'none'
                     });
                     $('orig_city').setStyle({
                         display: 'none'
                     });
                     $('dest_city').setStyle({
                         display: 'block'
                     });
                     
                     
                     $('query').setStyle({
                         display: 'block'
                     });
                     $('query3').setStyle({
                         display: 'none'
                     });
                     $('query2').setStyle({
                         display: 'none'
                     });
                     $('query4').setStyle({
                         display: 'block'
                     });
                } else if(typeValue == 'cty') {
                     $('orig_yard').setStyle({
                         display: 'none'
                     });
                     $('dest_yard').setStyle({
                         display: 'block'
                     });
                     $('orig_city').setStyle({
                         display: 'block'
                     });
                     $('dest_city').setStyle({
                         display: 'none'
                     });
                     
                     
                     $('query').setStyle({
                         display: 'none'
                     });
                     $('query3').setStyle({
                         display: 'block'
                     });
                     $('query2').setStyle({
                         display: 'block'
                     });
                     $('query4').setStyle({
                         display: 'none'
                     });
                }
        }

        document.observe("dom:loaded", function() {
            //show appropriate fields if something is selected
            showFields();
        });
           
           
        $('yardSearch').on('change', '.radio_change', function(event) {
                showFields();
        });
        
        
	new Autocomplete('query', { serviceUrl:'<?php echo $view['router']->generate('sales_ajax') ?>' });
        new Autocomplete('query2', { serviceUrl:'<?php echo $view['router']->generate('sales_city_ajax') ?>' });
        new Autocomplete('query3', { serviceUrl:'<?php echo $view['router']->generate('sales_ajax') ?>' });
        new Autocomplete('query4', { serviceUrl:'<?php echo $view['router']->generate('sales_city_ajax') ?>' });
	</script>