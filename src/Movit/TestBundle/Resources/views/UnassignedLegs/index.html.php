<?php $view->extend('::salesview.html.php') ?>


  
<p>Unassigned Movit legs</p>

<input type="button" style="width: 140px" value="Show job assignment" name="show_filters" id="show_button">

<form action="<?php echo $view['router']->generate('unassigned_legs') ?>" method="post" id="splitForm"></form>
<form action="<?php echo $view['router']->generate('meld') ?>" method="post" id="meldForm"></form>

<div id="tbl">

<table id="legs" class="sortable resizable">
    <thead>
        <tr>
            <th colspan="3"><h3>Unassigned Legs</h3></th>
        </tr>
        <tr>
                <th>
                    <input type="button" style="width: 100px" value="Show Filters" name="show_filters" id="show_button">
					
                    <input type="button" value="Hide Filters" name="hide_filters" id="hide_button" style="display: none; width: 100px">
                </th>

                <th>
                    <input type="button" style="width: 100px" id="clear_button" value="Clear input">
                </th>
        </tr>
        <tr> <!--  filters -->
            <!--<form id="filter_form"  action="<?php echo $view['router']->generate('unassigned_legs') ?>" method="post" <?php echo $view['form']->enctype($ff) ?>>
            <td>
                <?php //echo $view['form']->widget($ff) ?>
            </td>
            <td><input type="submit" value="Apply Filter"/></td>
            </form>-->
        </tr>
	<tr>
        
            <th class="meld_hide">Start</th>
            <th class="meld_hide">End</th>
            <th class="meld_hide">Delete</th>        
            
            
            <th class="sortcol" style="width: 35px">Leg ID</th>
            <th class="sortcol" >Substatus - Booking Status</th>
            <th class="sortcol">Service Type - Sales Rep</th>
            <th class="sortcol">SKU - Dimensions</th>
            <th class="sortcol" >PuCityYard - PUProv</th>
            <th class="sortcol">DelCityYard - DelProv</th>        

            <th class="sortcol">PUDateSched</th>   
            <th class="sortcol">DelDateSched</th>   
            <th class="sortcol" >Origin WO number</th>   
            <th class="sortcol" >Order Line #</th>   
            <th  class="sortcol">Offload</th>
            <th class="sortcol">CarrierRequested</th>
            <th  class="sortcol">Customer</th>
            <th  class="sortcol">Deleted</th>
            <th class="sortcol">Actions</th>
        </tr>
    </thead>


    <tfoot>
    </tfoot>
    
    <tbody>
        <?php foreach($legs as $leg): ?>
        
          <tr id="<?php echo $leg->get('mvLogisticLeg_id') ?>">
              
             <td class="meld_hide"><input type="checkbox" id="<?php echo $leg->get('mvLogisticLeg_id') ?>_start" /></td>
             <td class="meld_hide"><input type="checkbox" id="<?php echo $leg->get('mvLogisticLeg_id') ?>_end" /></td>
             <td class="meld_hide"><input type="checkbox" id="<?php echo $leg->get('mvLogisticLeg_id') ?>_delete" /></td>
             
             <td style="" class="SubStatus"><?php echo $leg->get('mvLogisticLeg_id') ?></td>
             <td class="SubStatus<?php echo $leg->get('mvLogisticLeg_id') ?>"><?php echo $leg->get('SubStatus') ? $leg->get('SubStatus') : 'n/a' ?> - <?php echo $leg->get('BookingStatus') ? $leg->get('BookingStatus') : 'n/a' ?></td>
             <td class="ServiceType<?php echo $leg->get('mvLogisticLeg_id') ?>"><?php echo $leg->get('ServiceType') ? $leg->get('ServiceType') : 'n/a' ?> - <?php echo $leg->get('SalesRep') ? $leg->get('SalesRep') : 'n/a' ?></td>
             <td class="SKU<?php echo $leg->get('mvLogisticLeg_id') ?>"><?php echo $leg->get('SKU') ? $leg->get('SKU') : 'n/a' ?> - <?php echo $leg->get('Dimensions') ? $leg->get('Dimensions') : 'n/a' ?></td>
             <td class="PuCityYard<?php echo $leg->get('mvLogisticLeg_id') ?>"><?php echo $leg->get('PUCityYard') ? $leg->get('PUCityYard') : 'n/a' ?> - <?php echo $leg->get('PUProv') ? $leg->get('PUProv') : 'n/a' ?></td>
             <td class="DelCityYard<?php echo $leg->get('mvLogisticLeg_id') ?>"><?php echo $leg->get('DelCityYard') ? $leg->get('DelCityYard') : 'n/a' ?> - <?php echo $leg->get('DelProv') ? $leg->get('DelProv') : 'n/a' ?></td>

             <td class="PuDateSched<?php echo $leg->get('mvLogisticLeg_id') ?>"><?php echo $leg->get('PUDateSched') ? $leg->get('PUDateSched')->format('Y-m-d') : 'n/a' ?></td>
             <td class="DelDateSched<?php echo $leg->get('mvLogisticLeg_id') ?>"><?php echo $leg->get('DelDateSched') ? $leg->get('DelDateSched')->format('Y-m-d') : 'n/a' ?></td>
             
             <td class="WO<?php echo $leg->get('mvLogisticLeg_id') ?>"><?php echo $leg->get('WO') ? $leg->get('WO') : 'n/a' ?></td>
             <td class="OrderLineNum<?php echo $leg->get('mvLogisticLeg_id') ?>"><?php echo $leg->get('OrderLineNum') ? $leg->get('OrderLineNum') : 'n/a' ?></td>
             
             <td class="OffloadRequired<?php echo $leg->get('mvLogisticLeg_id') ?>"><?php echo $leg->get('OffloadRequired') ? $leg->get('OffloadRequired') : 'n/a' ?></td>
             <td class="CarrierRequested<?php echo $leg->get('mvLogisticLeg_id') ?>"><?php echo $leg->get('CarrierRequested') ? $leg->get('CarrierRequested') : 'n/a' ?></td>
             <td class="Customer<?php echo $leg->get('mvLogisticLeg_id') ?>"><?php echo $leg->get('Customer') ? $leg->get('Customer') : 'n/a' ?></td>
             <td class="Deleted<?php echo $leg->get('mvLogisticLeg_id') ?>"><?php echo $leg->get('Deleted') ? $leg->get('Deleted') : 'n/a' ?></td>
             <td><a onClick="createRow(<?php echo $leg->get('mvLogisticLeg_id') ?>); FillForm(<?php echo $leg->get('mvLogisticLeg_id') ?>); ">Split</a></td>
              
          </tr>
        <?php endforeach; ?>
    </tbody>
    
    
</table>
</div>



<!--</form>-->
<p>
    <button type="button" onClick="showMeld(); disableSplit();">Meld</button>
    <button id="meld_apply" type="button" onClick="checkSelected();">Apply Meld</button>
    <button  id="meld_cancel" style="display: none" type="button" onClick="hideMeld();">Cancel</button>

    <!--<a id="meldTrigger" onClick="showMeld();">Meld</a>&nbsp;
    <a onClick="checkSelected(); ">Apply</a>&nbsp;
    <a onClick="hideMeld();">Cancel</a>-->
</p>

<div id="hide_me" style="display: none">
<p>New Leg</p>
   


<form id="new_leg"  action="<?php echo $view['router']->generate('unassigned_legs') ?>" method="post" <?php echo $view['form']->enctype($form) ?> >
<table id="insert_table">
    <tbody>
        <tr id="insert_row">
            
                
                
                        <td>
                            
                        </td>
                        <td><?php echo $view['form']->row($form['SubStatus']) ?><?php echo $view['form']->row($form['BookingStatus']) ?></td>
                        <td><?php echo $view['form']->row($form['ServiceType']) ?><?php echo $view['form']->row($form['SalesRep']) ?></td>
                        <td><?php echo $view['form']->row($form['SKU']) ?><?php //echo $view['form']->row($form['Dimension']) ?></td>
                        <td><?php echo $view['form']->row($form['PuCityYard']) ?><?php echo $view['form']->row($form['PuProv']) ?></td>
                        <td><?php echo $view['form']->row($form['DelCityYard']) ?><?php echo $view['form']->row($form['DelProv']) ?></td>
                        <td><?php echo $view['form']->row($form['PuDateSched']) ?></td>
                        <td><?php echo $view['form']->row($form['DelDateSched']) ?></td>
                        <td><?php echo $view['form']->row($form['WO']) ?></td>
                        <td><?php echo $view['form']->row($form['OrderLineNum']) ?></td>
                        <td><?php echo $view['form']->row($form['OffloadRequired']) ?></td>
                        <td><?php echo $view['form']->row($form['CarrierRequested']) ?></td>
                        <td><?php echo $view['form']->row($form['Customer']) ?></td>
                        <td><?php echo $view['form']->row($form['Deleted']) ?></td>
                        <td> <button value="CLICK HERE" onclick="red();">Click me</button><input  type="submit" value="Save"/> / <a href="">Cancel</a></td>
                        <td><?php echo $view['form']->row($form['_token']) ?></td>
                
                

        </tr>
    </tbody>
</table>
</form>



<table id="insert_table2">
    <tbody>
        <tr class="insert_row2">
            
                
                <form id="new_leg2"  action="<?php echo $view['router']->generate('unassigned_legs') ?>" method="post" <?php echo $view['form']->enctype($form2) ?> >
                        
                        <td>
                           <!-- Empty ID field -->
                        </td>
                        <td><?php echo $view['form']->row($form2['SubStatus']) ?><?php echo $view['form']->row($form2['BookingStatus']) ?></td>
                        <td><?php echo $view['form']->row($form2['ServiceType']) ?><?php echo $view['form']->row($form2['SalesRep']) ?></td>
                        <td><?php echo $view['form']->row($form2['SKU']) ?><?php //echo $view['form']->row($form['Dimension']) ?></td>
                        <td><?php echo $view['form']->row($form2['PuCityYard']) ?><?php echo $view['form']->row($form2['PuProv']) ?></td>
                        <td><?php echo $view['form']->row($form2['DelCityYard']) ?><?php echo $view['form']->row($form2['DelProv']) ?></td>
                        <td><?php echo $view['form']->row($form2['PuDateSched']) ?></td>
                        <td><?php echo $view['form']->row($form2['DelDateSched']) ?></td>
                        <td><?php echo $view['form']->row($form2['WO']) ?></td>
                        <td><?php echo $view['form']->row($form2['OrderLineNum']) ?></td>
                        <td><?php echo $view['form']->row($form2['OffloadRequired']) ?></td>
                        <td><?php echo $view['form']->row($form2['CarrierRequested']) ?></td>
                        <td><?php echo $view['form']->row($form2['Customer']) ?></td>
                        <td><?php echo $view['form']->row($form2['Deleted']) ?></td>
                        <td><input  type="submit" value="Save"/> / <a href="#" onclick="hideSplit(); return false;">Cancel</a></td>
                        <td><?php echo $view['form']->row($form2['_token']) ?></td>
                
                </form>

        </tr>
    </tbody>
</table>


</div>

<script>
    
    function red()
    {
            $('new_leg').request({ //form request
              method: 'post',
              onComplete: function(data){
                  //location.reload(true);
              }
            });
    }
    
    function checkSelected()
    {
        count1 = 0;
        count2 = 0;
        count3 = 0;
        $$('input[type="checkbox"][id*="start"]').each(function(item) {
             
             id = item.id;
             if($F('' + id) == 'on')
                ++count1;
        });
        $$('input[type="checkbox"][id*="end"]').each(function(item) {
             
             id = item.id;
             if($F('' + id) == 'on')
                ++count2;
        });
        $$('input[type="checkbox"][id*="delete"]').each(function(item) {
             
             id = item.id;
             if($F('' + id) == 'on')
                ++count3;
        });
        if(count1 == 0 && count2 == 0 && count3 > 0)
            setDeleted(true);
        else if(count1 > 1 || count2 > 1 || !count1 || !count2) {
            alert("You have to select exactly one start and one end.");
            return false;
        } else
            setDeleted(false);
    }
    
    function uni(keep_id, type, action)
    {
        keep_id = keep_id.toString();
        $$('input[type="checkbox"][id*="' + type + '"]').each(function(item) {
             
             id = item.id;
             id = id.split('_')[0];
             if(keep_id != id)
                 if(action == "disable")
                    item.disable();
                 else
                     item.enable();
        });
    }
    
    $$('input[type="checkbox"]').invoke('observe','click', function(field) {
        //alert(this.name + ' clicked ' + this.id);
        //alert($F("" + this.id));
        id = this.id;
        arr = id.split('_');
        id = arr[0];
        if($F("" + this.id) == "on") { //after clicking it to on
            if(arr[1] == 'start') {
                $(id + '_delete').disable();
                uni(id, 'start', 'disable');
                //disable all starts
                //disableStarts();
                
            } else if(arr[1] == 'end') {
                $(id + '_delete').disable();
                uni(id, 'end', 'disable');
                
            } else {
                $(id + '_start').disable();
                $(id + '_end').disable();
            }
        } else {
            if(arr[1] == 'start') {
                $(id + '_delete').enable();
                uni(id, 'start', 'enable');
            } else if(arr[1] == 'end') {
                $(id + '_delete').enable();
                uni(id, 'end', 'enable');
            } else {
                $(id + '_start').enable();
                $(id + '_end').enable();
            }
        }

        // other stuff ...
    });
    
    
    function findStartOrEnd(val)
    {
        $$('input[type="checkbox"][id*="' + val + '"]').each(function(item) {
             
             id = item.id;
             if($F('' + id) == 'on') 
                 vr = id.split('_')[0];
        });
        return vr;
    }
    
    
    function fillDeleted()
    {
        Form.Element.setValue($('form_Deleted'), 1);
    }
    
    function setDeleted(just_delete)
    {
        id_arr = "";
        $$('input[type="checkbox"][id*="delete"]').each(function(item) {
            
             id = item.id;
             if($F('' + id) == 'on') {
                
             
                arr = id.split('_');
                id_arr += arr[0] + ',';
            }
             //ajax request to a certain action which will modify the selected id's
        });
        alert(id_arr);
        if(just_delete) {
            start_id = end_id = 0;
        } else {
            start_id = findStartOrEnd('start');
            end_id = findStartOrEnd('end');
            if(start_id != end_id)
                id_arr += start_id + "," + end_id + ",";
        }
        alert(end_id);
        if(start_id != end_id) {
            FillForm(start_id);
            $('new_leg').request({ //form request
              method: 'post',
              onComplete: function(data){
              }
            });
            FillForm(end_id);
            $('new_leg').request({ //form request
              method: 'post',
              onComplete: function(data){
              }
            });
            
            
        }
        //populate the form here, send it.
        
        $('legsForm').request({ //form request
          method: 'post',
          parameters: { ids: id_arr },
          onComplete: function(data){
          }
        });
        //location.reload(true);
    }
    
    function showMeld()
    {
        $$('.meld_hide').each(function(item) { item.show(); });
        $('meld_cancel').show();
        $('meld_apply').show();
    }
    function hideMeld()
    {
        $$('.meld_hide').each(function(item) { item.hide(); });
        $('meld_cancel').hide();
        $('meld_apply').hide();
    }
    hideMeld();
    
    
    
    function showForm()
    {
        win = new Window({className: "mac_os_x", title: "New Leg", width: 300, height: 450, destroyOnClose: true, recenterAuto:false}); 
        win.getContent().update($('hide_me').innerHTML); 
        win.showCenter();
    }
    function trimNumber(s) {
          while (s.substr(0,1) == '0' && s.length>1) { s = s.substr(1,9999); }
          return s;
    }

    String.prototype.trim = function() {
        return this.replace(/^\s+|\s+$/g,"");
    }

    function fillDoubleField(strid1, strid2, trid)
    {
        str = $$("." + strid1 + trid)[0].innerHTML;
        arr = str.split('-');
        arr[0] = arr[0].trim();
        arr[1] = arr[1].trim();
        if(arr[0] != 'n/a') {
            Form.Element.setValue($('form_' + strid1), arr[0]);
        }
        if(arr[1] != 'n/a') {
            Form.Element.setValue($('form_' + strid2), arr[1]);
        }
    }
    
    function fillDateField(strid, trid)
    {
        str = $$("." + strid + trid)[0].innerHTML;
        arr = str.split('-');
        //alert(arr[0] + arr[1] + arr[2]); //yr, month, day
        
        
        Form.Element.setValue($('form_' + strid + '_year'), arr[0]);
        Form.Element.setValue($('form_' + strid + '_month'), trimNumber(arr[1]));
        Form.Element.setValue($('form_' + strid + '_day'), trimNumber(arr[2]));
    }
    
    function fillField(strid, trid)
    {
        if($$("." + strid + trid)[0].innerHTML != 'n/a')
            Form.Element.setValue($('form_' + strid), $$('.' + strid + trid)[0].innerHTML);
    }




    function FillForm(trid)
    {
        
        tr = $("" + trid);
        alert(tr);
        
        //children = tr.childElements();
        
        //alert(children[0].innerHTML);
        
        //SubStatus and BookingStatus
        fillDoubleField('SubStatus', 'BookingStatus', trid);
        fillDoubleField('ServiceType', 'SalesRep', trid);
        fillDoubleField('SKU', 'Dimensions', trid);
        //fillDoubleField('PuCityYard', 'PuProv', trid);
        fillDoubleField('DelCityYard', 'DelProv', trid);
        //setDelCityYard('DelCityYard', 'DelProv', trid);
        

        //Form.Element.setValue($('form_PUDateSched'), $('PUDateSched').innerHTML);
        fillDateField('PuDateSched', trid);
        fillDateField('DelDateSched', trid);
        
        
        fillField('WO', trid);
        fillField('OrderLineNum', trid);
        fillField('OffloadRequired', trid);
        fillField('CarrierRequested', trid);
        fillField('Customer', trid);
    }
    
    
    
    function createRow(trid)
    {
        //$$('.meld_hide').each(function(item) {
        //});
        if($$('.meld_hide')[0].readAttribute('style') == "") //visible
            return false;
        
        refrow = $('' + trid);
        
        table = $('legs');
        
        $('splitForm').update(table);

        
        elem = $$('.insert_row2')[0];
        //elem.className('rofla');
        //new row
        refrow.insert ({
            'after'  : elem
        });
        
    }
    
    function hideSplit()
    {
        //$('insert_row2');
        elem = $$('.insert_row2')[0];
        
        
        $$('#insert_table2 tbody')[0].insert(elem);
        
        $('tbl').insert($('legs'));
        
        
    }
    
    function disableSplit()
    {
        
    }
    
    
    
</script>