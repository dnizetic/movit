<!DOCTYPE html>
<html>
<head>
	<link href="<?php echo $view['assets']->getUrl('bundles/movittest/css/style2.css') ?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo $view['assets']->getUrl('bundles/movittest/css/mainLayout2.php') ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $view['assets']->getUrl('bundles/movittest/css/styles.css') ?>" rel="stylesheet" type="text/css" />
        
        
        <script type="text/javascript" charset="utf-8" src="<?php echo $view['assets']->getUrl('bundles/movittest/datatables/media/js/jquery.js') ?>"></script>
        <script type="text/javascript" charset="utf-8" src="<?php echo $view['assets']->getUrl('bundles/movittest/datatables/media/js/jquery.dataTables.js') ?>"></script>
        <script type="text/javascript" charset="utf-8" src="<?php echo $view['assets']->getUrl('bundles/movittest/JQuery-DataTables-ColumnFilter/media/js/jquery.dataTables.columnFilter.js') ?>"></script>
        <script type="text/javascript" charset="utf-8" src="<?php echo $view['assets']->getUrl('bundles/movittest/jeditable/jquery.jeditable.js') ?>"></script>
        <script type="text/javascript" charset="utf-8" src="<?php echo $view['assets']->getUrl('bundles/movittest/jqueryform/jquery.form.js') ?>"></script>
        
        
        <style type="text/css">
            .meld_hide {
                display: none;
            }
            #routes_wrapper {
                min-height: 100px;
            }
            
            #legs {
                width: 100% !important;
            }
        </style>
        <!--<style type="text/css" title="currentStyle">
            @import "<?php echo $view['assets']->getUrl('bundles/movittest/datatables/media/css/demo_table.css') ?>";
        </style>-->
        
        <link href="<?php echo $view['assets']->getUrl('bundles/movittest/datatables/media/css/demo_table.css') ?>" rel="stylesheet" type="text/css" />
</head>

<body class="grid home">
  
<div id="header">
  <div class="grid">
     <div class="logo-box col two">
        <h1 class="logo"><a id='movitTitle' href="http://conc.ealto.me/movit/app_dev.php/hello">Movit</a></h1>
        <div><button id="display_jobs" style="width: 144px">Job assignment &gt;&gt;</button><button style="width: 144px"  id="display_routes" type="button">Unique end points</button></div>
        <div id="jobs" style="height: 400px; background-color: white; display: none; margin-top: 15px;">
        <button id="create_job" class="">Create a Job</button>    
        <table id="jobs_table" class="display">
            <thead>
                <tr>

                    <th>Job ID</th>
                    <th>Details</th>
                    <th>Actions</th>

                </tr>
            </thead>
            <tbody>
            <?php foreach($jobs as $job): ?>

              <tr>

                 <td><?php echo $job->get('mvLogisticJob_id') ? $job->get('mvLogisticJob_id') : 'n/a' ?></td>
                 <td>Show</td>
                 <td><a id="<?php echo $job->get('mvLogisticJob_id') ?>" class="assign-link" href="#" style="color: blue;" onclick="return false;">Assign legs</a></td>

              </tr>
            <?php endforeach; ?>

            </tbody>

            <tfoot>

            </tfoot>

        </table>
        <script type="text/javascript">
            var jobsTable = $("#jobs_table").dataTable( { 
                //"aLengthMenu": [[ -1], [ "All"]]
                "bPaginate" : false
            });
        </script>
            
            
            
        </div>
        
        
        <div id="job" style="display: none">
            
        </div>
        <script type="text/javascript">
            function sessionRequest(arg)
            {
                    $.ajax({
                          type: 'POST',
                          async: false,
                          url: '<?php echo $view['router']->generate('ajax_session') ?>',
                          data: { "active" : arg },
                          success: function() {
                              //location.reload();
                          }
                    });
            }
            
            $('.assign-link').click(function() {
               id = $(this).attr('id');
                last_job_id = id;
                $("#job").load("<?php echo $view['router']->generate('ajax_job_legs') ?>", { job_id: id }, function() { //2nd arg is a list of arguments
                    $('#jobs').hide();
                    $('#job').show();
                    oTable.fnSetColumnVis(2, true);
                    
                    sessionRequest(id);
                        

                    $('#go_back').click(function() {
                        $('#job').hide();
                        $('#jobs').show();
                        oTable.fnSetColumnVis(2, false);
                        sessionRequest('jobs');
                        last_job_id = "";
                        
                    });
                    
                    
                });
               

               //hide jobs_table
               //show #job
               
            });
        </script>
        
     </div>


    <div class="nav-box col eight">
      <div class="inner">
        
        <ul id='spaceLink' class="user-navigation">
          <li class="home selected standardLink">
            <a href="http://conc.ealto.me/movit/app_dev.php/hello">
            <!--<img class='iconSet1' src='/movit/bundles/movittest/images/cube.png'/>-->
            Quality Control</a>
          </li>
          <li class="inbox empty standardLink">
            <a href="http://conc.ealto.me/movit/app_dev.php/sales">
            <!--<img class='iconSet1' src='/movit/bundles/movittest/images/phone.png'/>-->
            Sales
            </a>
          </li>  
          <li class="contacts standardLink">
          <a href="contacts.html">
          <!--<img class='iconSet1' src='/movit/bundles/movittest/images/flag.png'/>-->
          Contacts
          </a>
          </li>

        </ul>
          <!-- Modified -->

        <div class="user">
          <a href="#" class="dropdown">

            <b>Other Options</b>
          </a>
            <!--
          <ul class="user-options-navigation">
            <li><a href="https://podio.com/users/98507/edit">Account Settings</a></li>
            <li><a href="https://podio.com/-/store/own">My shared app</a></li>
            <li><a href="https://podio.com/-/organization/create" class="js-create-org">Create another organisation</a></li>
            <li><a href="https://podio.com/logout">Logout</a></li>

          </ul>
            -->
        </div>



      </div>
    </div>
  </div>
</div>

   
<div id='formsQC'>
    
    
    


    
    
    
    
<p><a onclick="callReset(); return false;" href="">Reset to 3 initial legs.</a></p>
<p><a onclick="callLegReset(); return false;" href="">Reset assigned legs.</a></p>

    
    

<p>Applied filters</p>
<table id="applied_routes">
    <thead>
	<tr>
        
            <th>Start city</th>
            <th>End city</th>
            <th># of legs</th>
            <th>Quantity Sum</th>
            <th>Filter</th>

        </tr>
    </thead>
    <tbody>
        
         
    </tbody>
    
    <tfoot>
        
    </tfoot>
        
</table>
<button id="reset_all" style="display: none" onclick="removeAllFilters();" class="">Remove all</button>
    
    
    
<!-- <button style="display: none" onclick="show_columns();" id="show_button" type="button">Show hidden</button> -->

<div id="unique_routes" style="display: none">
    <p style="margin-top: 20px">Unique routes</p>
    <table id="routes" class="display">
        <thead>
            <tr>

                <th>Start city</th>
                <th>End city</th>
                <th># of legs</th>
                <th>Quantity sum</th>
                <th>Filter</th>

            </tr>
        </thead>
        <tbody  style="text-align: center">
            <?php foreach($unique_routes as $ur): ?>

              <tr>

                 <td><?php echo $ur->get('PUCityYard') ? $ur->get('PUCityYard') : 'n/a' ?></td>
                 <td><?php echo $ur->get('DelCityYard') ? $ur->get('DelCityYard') : 'n/a' ?></td>
                 <td><?php echo $ur->get('num_legs') ? $ur->get('num_legs') : 'n/a' ?></td>
                 <td><?php echo $ur->get('quantity_sum') ? $ur->get('quantity_sum') : 'n/a' ?></td>
                 <td><img onclick="applyFiltering(this);" src="/movit/bundles/movittest/datatables/examples/examples_support/details_open.png"/></td>

              </tr>
            <?php endforeach; ?>
        </tbody>

        <tfoot>

        </tfoot>

    </table>

</div>
<script type="text/javascript">
    last_job_id = "";
    $("#display_routes").click(function() {
         if($("#unique_routes").is(":visible")) {
             $("#unique_routes").hide();
             $(this).text('Unique end points');
         } else {
             $("#unique_routes").show();
             $(this).text('Hide unique end pts');
         }
    });
    
    $("#display_jobs").click(function() {
         if($("#job").is(":visible")) {
             $("#job").hide();
             oTable.fnSetColumnVis(2, false);
             $("#jobs").hide();
             $('#formsQC').css('margin-left', '0px');
             sessionRequest('none');
         } else if($("#jobs").is(":visible")) {
             $("#jobs").hide();
             $('#formsQC').css('margin-left', '0px');
             sessionRequest('none');
         }  else {
             $("#jobs").show();
             $('#formsQC').css('margin-left', '300px');
             if(last_job_id != "") {
                 $("#" + last_job_id).click();
                 sessionRequest(last_job_id);
             } else {
                 sessionRequest('jobs');
                 last_job_id = "";
             }
                
         }
    });
</script>
    




<p id="split_message" style="display: none">Split</p>
<div id="buttons" style="margin: 30px 0 15px 0">
  
        <button style="display: none" onclick="show_columns();" id="show_button" type="button">Show hidden</button>
        <button onclick="showMeld();" id="meld" type="button">Meld</button>
        <button onclick="applyMeld();" id="apply_meld" style="display: none" type="button">Apply</button>
        <button onclick="cancelMeld();"id="cancel_meld" style="display: none" type="button">Cancel</button>

</div>


<form action="<?php echo $view['router']->generate('meld') ?>" method="post" id="meldForm"></form>

<form action="<?php echo $view['router']->generate('unassigned_legs_new') ?>" method="post" id="splitForm">
<div id="tbl">
<table id="legs"  class="display">
    <thead id="table_header">


	<tr id="headers">
        
            <th>Start</th>
            <th>End</th>
            <th>Assign</th>
            <!--<th>Delete</th>-->
            
            <th>Path ID</th>
            <th>Leg ID</th>
            <th>Quantity</th>
            <th>Original Quantity</th>
            <th>Step</th>
            <th>Substatus - Booking Status</th>
            <th>Service Type - Sales Rep</th>
            <th>SKU - Dimensions</th>
            <th id="start_city_header">Start City - PUProv</th>
            <th id="end_city_header">End City - DelProv</th>        

            <th>PU Date Sched</th>   
            <th>Del Date Sched</th>
            <th>Origin number</th>   
            <th>Order Line #</th>   
            <th>Offload</th>
            <th>Carrier Requested</th>
            <th>Customer</th>
            <th>Deleted</th>
            <!--<th class="split_column center">Actions</th>-->
        </tr>

        

    </thead>

    
    <tbody>
        
<?php 
    //used by foreach to determine coloring.
    //3.1.2012.
    function compareAddress($leg1, $leg2)
    {
        if(!$leg1 || !$leg2)
            return false;
        if($leg2->get('mvLogisticLeg_id') == $leg1->get('mvLogisticLeg_id'))
            return false;

        if($leg1->get('PuCityYard') == $leg2->get('PuCityYard')
        && $leg1->get('PuProv') == $leg2->get('PuProv')
        && $leg1->get('PuAddress') == $leg2->get('PuAddress'))
            return true;

        return false;
    }
    
    
    $grandparent = "";
    $counter = 1;
?>
        
        <?php foreach($legs as $leg): ?>
        
          <tr class="odd" id="<?php echo $leg->get('mvLogisticLeg_id') ?>" title="<?php
                        if($leg->get('InitialLegID')) {
                            if($leg->get('IsQuantityParent'))
                                echo 'quantity_parent';
                            else
                                echo 'child';
                        }
                        else
                            echo 'parent';
                  ?>" 
              style="
                <?php 
                    if($leg->get('InitialLegID')) {
                        if($leg->get('IsQuantityParent'))
                            echo 'background-color : #FFFFC0';
                    }
                    else
                        echo 'background-color : #FFC4C4';
                    
                    
                ?>">
             
             
             <?php
                    
                    if(!$leg->get('InitialLegID')/* && !$leg->get('IsQuantityParent')*/) {
                        $grandparent = $leg;
                        $counter = 1;
                    } else {
                        ++$counter;
                    }
             ?>              
              
             <td class="meld_start"><input type="checkbox" id="<?php echo $leg->get('mvLogisticLeg_id') ?>_start" /></td>
             <td class="meld_end"><input type="checkbox" id="<?php echo $leg->get('mvLogisticLeg_id') ?>_end" /></td>
             <td class="assign_leg"><img class="assign-image" src="<?php echo $view['assets']->getUrl('bundles/movittest/datatables/examples/examples_support/details_open.png') ?>"/></td>
             <!--<td class="meld_delete"><input type="checkbox" id="<?php echo $leg->get('mvLogisticLeg_id') ?>_delete" /></td> -->
             
             <td class="PathID<?php echo $leg->get('mvLogisticLeg_id') ?>"><?php echo $leg->get('PathID') ? $leg->get('PathID') : '0' ?></td>
             <td class="mvLogisticLeg_id<?php echo $leg->get('mvLogisticLeg_id') ?>" style="<?php echo $leg->get('mvLogisticJob_id') ? 'background-color: #E5EDC4' : '' ?>"><?php echo $leg->get('mvLogisticLeg_id') ?></td>
             
             
             <td class="Quantity<?php echo $leg->get('mvLogisticLeg_id') ?>" style="
                     <?php 
                     
                        if($leg->get('IsQuantityParent') == 1) {
                            if(compareAddress($grandparent, $leg))
                                echo "background-color: #FFC4C4";
                            else
                                echo "background-color: #FFFFC0";
                            //echo $grandparent->get('PuAddress') == $leg->get('PuAddress');
                        }
                        //echo $leg->get('IsQuantityParent') ? 'background-color: #FFFFC0' : '' 
                     ?>"
             ><?php echo $leg->get('Quantity') ? $leg->get('Quantity') : 'n/a' ?></td>
             <td class="OriginalQuantity<?php echo $leg->get('mvLogisticLeg_id') ?>"><?php echo $leg->get('OriginalQuantity') ?></td>
             <td class="Step<?php echo $leg->get('mvLogisticLeg_id') ?>"><?php echo $counter ?></td>
             <td class="SubStatus<?php echo $leg->get('mvLogisticLeg_id') ?>"><?php echo $leg->get('SubStatus') ? $leg->get('SubStatus') : 'n/a' ?> - <?php echo $leg->get('BookingStatus') ? $leg->get('BookingStatus') : 'n/a' ?></td>
             <td class="ServiceType<?php echo $leg->get('mvLogisticLeg_id') ?>"><?php echo $leg->get('ServiceType') ? $leg->get('ServiceType') : 'n/a' ?> - <?php echo $leg->get('SalesRep') ? $leg->get('SalesRep') : 'n/a' ?></td>
             <td class="SKU<?php echo $leg->get('mvLogisticLeg_id') ?>"><?php echo $leg->get('SKU') ? $leg->get('SKU') : 'n/a' ?> - <?php echo $leg->get('Dimensions') ? $leg->get('Dimensions') : 'n/a' ?></td>
             
             <td class="PuCityYard<?php echo $leg->get('mvLogisticLeg_id') ?>"><?php echo $leg->get('PuCityYard') ? $leg->get('PuCityYard') : 'n/a' ?> - <?php echo $leg->get('PuProv') ? $leg->get('PuProv') : 'n/a' ?> - <?php echo $leg->get('PuAddress') ? $leg->get('PuAddress') : 'n/a' ?></td>
             <td class="DelCityYard<?php echo $leg->get('mvLogisticLeg_id') ?>"><?php echo $leg->get('DelCityYard') ? $leg->get('DelCityYard') : 'n/a' ?> - <?php echo $leg->get('DelProv') ? $leg->get('DelProv') : 'n/a' ?> - <?php echo $leg->get('DelAddress') ? $leg->get('DelAddress') : 'n/a' ?></td>

             <td width="130%" class="PuDateSched<?php echo $leg->get('mvLogisticLeg_id') ?>"><?php echo $leg->get('PuDateSched') ? $leg->get('PuDateSched')->format('Y-m-d') : 'n/a' ?></td>
             <td width="130%" class="DelDateSched<?php echo $leg->get('mvLogisticLeg_id') ?>"><?php echo $leg->get('DelDateSched') ? $leg->get('DelDateSched')->format('Y-m-d') : 'n/a' ?></td>
             
             <td class="WO<?php echo $leg->get('mvLogisticLeg_id') ?>"><?php echo $leg->get('WO') ? $leg->get('WO') : 'n/a' ?></td>
             <td class="OrderLineNum<?php echo $leg->get('mvLogisticLeg_id') ?>"><?php echo $leg->get('OrderLineNum') ? $leg->get('OrderLineNum') : 'n/a' ?></td>
             
             <td class="OffloadRequired<?php echo $leg->get('mvLogisticLeg_id') ?>"><?php echo $leg->get('OffloadRequired') ? $leg->get('OffloadRequired') : 'n/a' ?></td>
             <td class="CarrierRequested<?php echo $leg->get('mvLogisticLeg_id') ?>"><?php echo $leg->get('CarrierRequested') ? $leg->get('CarrierRequested') : 'n/a' ?></td>
             <td class="Customer<?php echo $leg->get('mvLogisticLeg_id') ?>"><?php echo $leg->get('Customer') ? $leg->get('Customer') : 'n/a' ?></td>
             <td class="Deleted<?php echo $leg->get('mvLogisticLeg_id') ?>"><?php echo $leg->get('Deleted') ? $leg->get('Deleted') : 'n/a' ?><div style="display: none" id="FinalDestination<?php echo $leg->get('mvLogisticLeg_id') ?>"><?php echo $leg->get('FinalDestination') ?></div></td>
             <!--<td class="split_column center"><a onClick="createSplitRow(<?php echo $leg->get('mvLogisticLeg_id') ?>); "></a></td>-->
              

             
             
          </tr>
        <?php endforeach; ?>
    </tbody>
    
    

    <tfoot>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <!--<th></th>-->
                <th>Path ID</th>
                <th>Leg ID</th>
                <th>Quantity</th>
                <th>Original Quantity</th>
                <th>Step</th>
                <th>Substatus - Booking Status</th>
                <th>Service Type - Sales Rep</th>
                <th>SKU/Dim.</th>
                <th id="start_city_filter">PuCity Yard - PUProv</th>
                <th id="end_city_filter">DelCity Yard - DelProv</th>        

                <th>PU Date Sched</th>   
                <th>Del Date Sched</th>   
                <th>Origin WO number</th>   
                <th>Order Line #</th>   
                <th>Offload</th>
                <th>Carrier Requested</th>
                <th>Customer</th>
                <th>Deleted</th>
                <!--<th class="split_column">Actions</th>-->
            </tr>
            
            
            <tr id="hide_me_buttons">
                <th></th>
                <th></th>
                <!--<th></th>-->
                
                <script type="text/javascript">
                    //script will generate the hide me buttons
                    //get first row of legs table, if empty, break
                    //if not empty, get num of children (tds)
                    //create it, starting from index 2
                    $(document).ready(function() {
                        num_columns = $("#headers").children().length;
                        if(num_columns) {
                            for (i = 2; i < num_columns; i++)
                            {
                                var elem = $('<th> <button onclick="oTable.fnSetColumnVis(' + i + ', false); button_show();"  type="button">Hide</button></th>');
                                $("#hide_me_buttons").append(elem);

                            }
                        }
                    });
                    
                </script>


                <!--<th> <button id="last_hide_button" onclick="oTable.fnSetColumnVis( 17, false ); button_show();"  type="button">Hide</button></th>-->
            </tr>
    </tfoot>
    
    
</table>
</div>
</form>

    
</div>
<!--
<p style="margin-top: 55px">History: first is most recent. Note: Actions's must be undoed in the order they were performed to preserve route connections (disabled links).</p>
<table id="history">
    <thead>
	<tr>
        
            <th>Date</th>
            <th>Split Leg ID</th>
            <th>Start Leg ID</th>
            <th>End Leg ID</th>
            <th>Type</th>
            <th>Action</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php $i = 0; foreach($history as $h): ?>
        <tr>
            <td><?php echo $h->get('Date') ? $h->get('Date')->format('Y-m-d H:i:s') : 'n/a' ?></td>
            <td><?php echo $h->get('SplitLegID') ?></td>
            <td><?php echo $h->get('StartLegID') ?></td>
            <td><?php echo $h->get('EndLegID') ?></td>
            <td><?php echo $h->get('Type') == 0 ? 'Route' : 'Quantity' ?></td>
            <td><?php echo $h->get('Action') == 0 ? 'Split' : 'Meld' ?></td>
            <?php if($i == 0): ?>
                <td><a href="#" onclick="callSplitUndo(<?php echo $h->get('SplitLegID') ?>, <?php echo $h->get('StartLegID') ?>, <?php echo $h->get('EndLegID') ?>); return false;">Undo</a></td>
                <?php ++$i; ?>
            <?php else: ?>
                <td>Disabled</td>
            <?php endif; ?>
            
        </tr>
        <?php endforeach; ?>
         
    </tbody>
    
    <tfoot>
        
    </tfoot>
        
</table> 
-->
    

<div id="hide_me" style="display: none">

<table id="split_insert_table">
    <thead>
        <tr class="split_insert_header" colspan="15" style="display: none">
            <th>Split</th>
        </tr>
    </thead>
    <tbody>
        <tr class="split_insert_row" align="center">
            
                
                
                        <td><div>n/a</div></td>
                        <td>n/a</td>
                        <td><?php echo $view['form']->row($form['Quantity']) ?></td> <!-- Insert here -->
                        <td class="original_quantity">--|--</td>
                        <td>n/a</td>
                        <td><?php echo $view['form']->row($form['SubStatus']) ?><?php echo $view['form']->row($form['BookingStatus']) ?></td>
                        <td><?php echo $view['form']->row($form['ServiceType']) ?><?php echo $view['form']->row($form['SalesRep']) ?></td>
                        <td><?php echo $view['form']->row($form['SKU']) ?><?php //echo $view['form']->row($form['Dimension']) ?></td>
                        <td><?php echo $view['form']->row($form['PuCityYard']) ?><?php echo $view['form']->row($form['PuProv']) ?><?php echo $view['form']->row($form['PuAddress']) ?></td>
                        <td><?php echo $view['form']->row($form['DelCityYard']) ?><?php echo $view['form']->row($form['DelProv']) ?><?php echo $view['form']->row($form['DelAddress']) ?></td>
                        <td><?php echo $view['form']->row($form['PuDateSched']) ?></td>
                        <td><?php echo $view['form']->row($form['DelDateSched']) ?></td>
                        
                        
                        <td class="wo_number">--|--</td>
                        <td class="order_line_number">--|--</td>

                        
                        <td><?php echo $view['form']->row($form['OffloadRequired']) ?></td>
                        <td><?php echo $view['form']->row($form['CarrierRequested']) ?></td>
                        <td><?php echo $view['form']->row($form['Customer']) ?></td>
                        <td><?php echo $view['form']->row($form['Deleted']) ?> <?php echo $view['form']->row($form['_token']) ?> 
                                <?php echo $view['form']->row($form['mvLogisticLeg_id']) ?>
                                <?php echo $view['form']->row($form['start_city']) ?>
                                <?php echo $view['form']->row($form['InitialLegID']) ?>
                                <?php echo $view['form']->row($form['PathID']) ?>
                                <?php echo $view['form']->row($form['IsQuantityParent']) ?>
                                <?php echo $view['form']->row($form['OriginalQuantity']) ?>
                                <?php echo $view['form']->row($form['WO']) ?>
                                <?php echo $view['form']->row($form['OrderLineNum']) ?>
                                <?php echo $view['form']->widget($form['FinalDestination']) ?>
                        </td>
                        <td><input class="splitSaveButton" type="submit" value="Save"/> <a href="#" onclick="return false;"></a> <!--/ <a href="#" onclick="return cancelCreateSplitRow();">Cancel</a>--></td>
                      
                
        </tr>

    </tbody>
</table>
    
<script type="text/javascript">
    $(document).ready(function() {
        //challenge 1
        //$("#split_insert_table .wo_number input, #split_insert_table .order_line_number input").attr("disabled", "disabled");
        
        //$("#form_OrderLineNum").attr("disabled", "disabled");
        //alert("I'm here");
    });
</script>
    
    
    
    
    
    
    

    
</div>    
    
    
    
    
    
    
   
<script type="text/javascript">
    
    
    $("#create_job").click(function() {
        $.ajax({
          type: 'POST',
          url: '<?php echo $view['router']->generate('create_job') ?>',
          data: { },
          success: function() {
              location.reload(); //reload a page
          }
        }); 
    });
    
    
    /*
    $(window).unload(function() {
       //alert('Handler for .unload() called.');
       //if #job is visible, take it's ID
       var active = "";
       if($("#job").is(':visible')) {
            active = $('#job_id').text();

       } else if($("#jobs").is(":visible")) {
            active = 'jobs';
       } else {
            active = 'none';
       }
       
       $.ajax({
          type: 'POST',
          async: false,
          url: '<?php echo $view['router']->generate('ajax_session') ?>',
          data: { "active" : active },
          success: function() {
              //location.reload();
          }
        });
    
        
       //if #jobs is visible
    }); */
    

    
    
    function initialiseDisassignLeg(selector)
    {
          $(selector).click(function() {
                  var leg_id = $(this).parent().parent().attr('id');
                  $.ajax({
                      type: 'POST',
                      url: '<?php echo $view['router']->generate('ajax_assign_job_legs') ?>',
                      data: { "leg_id" : leg_id, "job_id" : 0 },
                      success: function() {
                          alert('Unassigning leg.');
                          location.reload(); //reload a page
                      }
                });
          });
    }
    function warnDifferentCity(start_city, end_city)
    {
          if(legsTable.fnSettings().fnRecordsTotal() == 0)
              return;
          alert(start_city, ' ', end_city);
          different_start = different_end = 1;
          $("#legs_table tbody tr").each(function() { //different from all
              //alert('Comparing start cities: ' + $(this).find('.assigned_start_city').text() + ', ' + start_city)
              if($(this).children().eq(1).text() == start_city)
                  different_start = 0;
              if($(this).children().eq(2).text() == end_city)
                  different_end = 0;
          });
          if(different_start)
              alert('Start city of the inserted leg differs from all start cities of already existing legs for this job.');
          if(different_end)
              alert('End city of the inserted leg differs from all end cities of already existing legs for this job.');    
    }

    $(document).ready(function() {
        $('.assign-image').click(function() {
            //alert('Predicted');
            var leg_id = $(this).parent().parent().attr('id');
            var job_id = $('#job_id').text();
            
            
            
            alert(leg_id + " " + job_id);
            
            //check if meld_active and if starT/end is clicked
            if(meld_active && ($("#" + leg_id + "_start").attr('checked') == 'checked' || $("#" + leg_id + "_end").attr('checked') == 'checked')) {
                alert('Turn off the checkbox first.');
            } else {
                $.ajax({
                  type: 'POST',
                  url: '<?php echo $view['router']->generate('ajax_assign_job_legs') ?>',
                  data: { "leg_id" : leg_id, "job_id" : job_id },
                  success: function() {


                      //add data
                      //pass leg_id, start_city, end_city and actions + initialise the action
                      
                      
                      var start_city = $(".PuCityYard" + leg_id).text().split('-')[0].trim();
                      var end_city = $(".DelCityYard" + leg_id).text().split('-')[0].trim();
                      
                      warnDifferentCity(start_city, end_city);
                      
                      var img = '<img class="assign-image" src="<?php echo $view['assets']->getUrl('bundles/movittest/datatables/examples/examples_support/details_close.png') ?>" />';
                      idx = $("#legs_table").dataTable().fnAddData([ //this creates a dependency
                            leg_id,
                            start_city,
                            end_city,
                            img
                      ]);
                      
                      //compare start_city with all the start_cities in the table left
                      //compare end_city with all the end_cities in the table left
                      //if same, alert it
                      //dependency: '-' seperated - city name must not contain one
                      
                      $("#legs_table").dataTable().fnDraw();

                      //onclick, call the controller to set it back to null
                      node = $("#" + leg_id)[0];
                      oTable.fnDeleteRow( node );


                      var nNode = legsTable.fnGetNodes( idx );
                      obj = $(nNode); 
                      obj.attr('id', leg_id);
                      initialiseDisassignLeg("#" + leg_id + " img");



                  }
                });
            }
            

            
        });
    });




    /* V 1.1. Quantity SPLIT 2.12.2011. START */
    //Quantity split form
    function validateSplitForm(row_id) 
    {
        //currently you can only qsplit last leg.
        //parent can be qsplit only if he's the only one in that route
        //childs can be qsplit if they're leaves -- at the end of a subroute of a route
        //all qparents with no kids can be qsplit (kids include kids + qparents).
        

        
        var row = $("#" + row_id);
        
        split_leg_quantity = $(".Quantity" + row_id).first().text();
        new_leg_quantity = $("#form_Quantity" + row_id).val(); //mark this
        //alert(split_leg_quantity + " " + new_leg_quantity);
        if(parseInt(new_leg_quantity) > parseInt(split_leg_quantity)) { //mark this
            alert("The new quantity must be smaller then the initial quantity.");
            return false;
        }
        
        fin_dest = $("#FinalDestination" + row_id).text();
        curr_dest = row.find('.DelCityYard' + row.attr('id')).text();
        
        
        explode_end1 = fin_dest.split('-');
        explode_end2 = curr_dest.split('-');
        
        //alert("fin_dest: " + fin_dest + " curr:" + curr_dest);
        
        if(explode_end1[0].trim() == explode_end2[0].trim()
        && explode_end1[1].trim() == explode_end2[1].trim()
        && explode_end1[2].trim() == explode_end2[2].trim())
            return true;
        else {
            alert('End city must go to the final destination to be qsplitable.')
        }
        
        return false;

        
        
    }
    /* V 1.1. Quantity SPLIT 2.12.2011. END */
    
    
    
    function callReset()
    {
        
        $.ajax({
          url: '<?php echo $view['router']->generate('reset_database') ?>',
  
          success: function() {
              location.reload();
          }
        });
        
    }
    
    
    function callLegReset()
    {
        
        $.ajax({
          url: '<?php echo $view['router']->generate('reset_leg_jobs') ?>',
  
          success: function() {
              location.reload();
          }
        });
        
    }
    
    function callSplitUndo(split_leg_id, start_id, end_id)
    {
        array = { split_leg_id : split_leg_id, start_id : start_id, end_id : end_id };
        $.ajax({
          type: 'POST',
          url: '<?php echo $view['router']->generate('split_undo') ?>',
          data: array,
          success: function() {
              //location.reload();
              //location.reload();
          }
          //dataType: dataType
        });
    }
    
    
    var groupTable = $("#routes").dataTable( { 
        //"aLengthMenu": [[ -1], [ "All"]]
        "bPaginate" : false
    });
        
    
    //dependency: outputed PuCityYard has to have an order
    function validateSplitStartEqualty(id)
    {
        start_location = $('.PuCityYard' + id).text();
        end_location = $('.DelCityYard' + id).text();
        pu = $('#form_PuCityYard' + id).val();
        prov = $('#form_PuProv' + id).val();
        add = $('#form_PuAddress' + id).val();
        
        arr = start_location.split('-');
        arr2 = end_location.split('-');
        arr[0] = arr[0].trim();
        arr[1] = arr[1].trim();
        arr[2] = arr[2].trim();
        
        arr2[0] = arr2[0].trim();
        arr2[1] = arr2[1].trim();
        arr2[2] = arr2[2].trim();
        
       
        if(arr[0] == pu
        && arr[1] == prov
        && arr[2] == add)
            return true;
        if(arr2[0] == pu
        && arr2[1] == prov
        && arr2[2] == add)
            return true;
        
        return false;
    }
    
    $(document).ready(function() {
            /*
             * Insert a 'details' column to the table
             */
            var nCloneTh = document.createElement( 'th' );
            
            //var nCloneTd = document.createElement( 'td' );
            //nCloneTd.innerHTML = '<img onclick="fillSplitForm(1);" src="<?php echo $view['assets']->getUrl('bundles/movittest/datatables/examples/examples_support/details_open.png') ?>">';
            //nCloneTd.className = "center";

            //dependency - 36?
            num_nodes = num_columns * 2 + 4; //+4 is a dependency on meld.
            //num_nodes determines which column to put the split image in.
            $('#legs thead tr').each( function () {
                    this.insertBefore( nCloneTh, this.childNodes[num_nodes] );
            } );

            /*$('#legs tbody tr').each( function () {
                    this.insertBefore(  nCloneTd.cloneNode( true ), this.childNodes[35] );
                  
            } );*/
            
            
            $('#legs tbody tr').each( function () {
    
                    tr_id = $(this).attr('id');
                    var nCloneTd = document.createElement( 'td' );
                    nCloneTd.innerHTML = '<img class="split_image" src="<?php echo $view['assets']->getUrl('bundles/movittest/datatables/examples/examples_support/details_open.png') ?>">';
                    nCloneTd.className = "center";
    
                    this.insertBefore(  nCloneTd.cloneNode( true ), this.childNodes[num_nodes] );
                  
            } );
        

            /*
             * Initialse DataTables, with no sorting on the 'details' column
             */
            /*
            var oTable = $('#legs').dataTable( {
                    "aoColumnDefs": [
                            { "bSortable": false, "aTargets": [ 0 ] }
                    ]
                    //"aaSorting": [[1, 'asc']]
            });*/

            /* Add event listener for opening and closing details
             * Note that the indicator for showing which row is open is not controlled by DataTables,
             * rather it is done here
             */
            $('.split_image').live('click', function () { //.live() is deprecated.
                    var nTr = this.parentNode.parentNode;
                    if ( this.src.match('details_close') )
                    {
                            
                            /* This row is already open - close it */
                            this.src = "<?php echo $view['assets']->getUrl('bundles/movittest/datatables/examples/examples_support/details_open.png') ?>";
                            oTable.fnClose( nTr );
                    }
                    else
                    {
                            /* Open this row */
                            this.src = "<?php echo $view['assets']->getUrl('bundles/movittest/datatables/examples/examples_support/details_close.png') ?>";
                            oTable.fnOpen( nTr, fnFormatDetails(oTable, nTr), 'details' );
                            
                            //alert(nTr.id);
                            fillSplitForm(nTr.id);
                            $("#splitForm" + nTr.id).submit(function() {
                                //quantity check
                                split_quantity = $(".Quantity" + nTr.id).text();
                                new_quantity = $("#form_Quantity" + nTr.id).val();
                                //alert("Split quantity: " + split_quantity + " New quant" + new_quantity);
                                if(parseInt(new_quantity) > parseInt(split_quantity)) {
                                    alert("Specified quantity should be less then or equal to the previous leg quantity.");
                                    return false;
                                }
                                if(new_quantity <= 0) {
                                    alert("Specified quantity must be a positive integer.");
                                    return false;
                                }
                                
                                
                                //PuCityYard has to be specified
                                if($("#form_PuCityYard" + nTr.id).val() == "" || 
                                   $("#form_PuProv" + nTr.id).val() == "") {
                                    alert("Start city or start province not specified.");
                                    return false;
                                }
                                //validate start_location
                                if(validateSplitStartEqualty(nTr.id) == true) {
                                    alert('Start point cannot match the start nor end point of the split leg.');
                                    return false;
                                }

                                
                                //must be last leg in route
                                //quantity has changed?
                                //needs to be sorted by PathID
                                if(split_quantity != new_quantity) {
                                    
                                    if(validateSplitForm(nTr.id) == false)
                                        return false;
                                    /*
                                    current_row = $(nTr);
                                    next_row = current_row.next().next();
                                    
                                    if(!next_row.length)
                                        return true;
                                    
                                    if(next_row.attr("title") == 'parent'
                                    || next_row.attr("title") == 'quantity_parent')
                                        return true;
                                   
                                    alert('Quantity split is only allowed on the last point of a route.');
                                    return false; */
                                }
                                $("#form_DelCityYard" + nTr.id).removeAttr("disabled");
                                $("#form_DelProv" + nTr.id).removeAttr("disabled");
                                $("#form_DelAddress" + nTr.id).removeAttr("disabled");

                                return true;
                            });

                            
                            normaliseWidth(nTr.id);
                            normaliseIds(nTr.id);
                            
                    }
            } );
            $('#legs thead th:last').html('Split');
           
    } ); 
    
    
    
    
    function resetMeldForm()
    {
        $("input").attr('checked', false);
        $("input").attr('disabled', false);
    }
    
    
    
    col_hidden = 0;
    $(document).ready(function() { //solved via http://datatables.net/examples/basic_init/scroll_x.html
        
        
        resetMeldForm();
        
        //avoid dependency for filters.
        //get the num_columns variable, set it to real num (+ 2 for meld) - don't count in split
        //create the needed array
        filterArray = new Array();
        //meld
        filterArray[0] = null;
        filterArray[1] = null;
        for (i = 2; i < num_columns; i++) //- meld columns
        {
            if($("#headers").children().eq(i).attr("id") == "start_city_header"
            || $("#headers").children().eq(i).attr("id") == "end_city_header") {
                filterArray[i] = { type: "text", bRegex:true };
            } else {
                filterArray[i] = { type: "text" };
            }
        }
        //filter Array is fine.

        var oTable = $('#legs').dataTable( {
            //"sPaginationType": "full_numbers",
            "bPaginate" : false,
            //"aLengthMenu": [[ -1 /*, 10, 25, 50*/], [ "All" /*, 10, 25, 50 */]],
            //"bAutoWidth": false,
            "fnDrawCallback": function () {
                
                $("#legs tbody tr").each(function() {
                   if($(this).title == 'parent')  {
                       $(this).children().first().css('background-color', '#FFC4C4');
                   }
                });
                //if meld is active
                if(meld_active) {
                    $('.meld_hide').show();
                    $('.split_column').hide();
                } else {
                    $('.meld_hide').hide();
                    $('.split_column').show();
                }
                
                
                
                if(start_disabled) {
                    
                    $(".meld_start input").attr('disabled', true);
                    $(".meld_start input:checked").attr('disabled', false);
                    
                } else {
                    
                    $('.meld_start input').attr('disabled', false);
                }
                
                if(end_disabled) {
                    
                    $('.meld_end input').attr('disabled', true);
                    $(".meld_end input:checked").attr('disabled', false);
                    
                } else {
                    
                    $('.meld_end input').attr('disabled', false);
                } 
            
                //iterate through deletes, if checked, disable both start and end
                /* $.each($('.meld_delete input:checked'), function() {
                    //$(this)
                    fnd = '.meld_start input[id=' + $(this).attr("id").split('_')[0] + '_start'  + ']';
                    $(fnd).attr('disabled', true);
                    fnd = '.meld_end input[id=' + $(this).attr("id").split('_')[0] + '_end'  + ']';
                    $(fnd).attr('disabled', true);
                }); */
                
                
                
            }//,
            //"sScrollX": "100%"
            //"sScrollXInner": "110%",
            //"bScrollCollapse": true
        } ).columnFilter( {
        
                aoColumns: filterArray
               
                  }
        
        );
        
        

        //var routesTable = $('#routes').dataTable( { } );
    
    
	// Apply the jEditable handlers to the table 
	/*$('td', oTable.fnGetNodes()).editable( '../examples_support/editable_ajax.php', {
		"callback": function( sValue, y ) {
			var aPos = oTable.fnGetPosition( this );
			oTable.fnUpdate( sValue, aPos[0], aPos[1] );
		},
		"submitdata": function ( value, settings ) {
			return {
				"row_id": this.parentNode.getAttribute('id'),
				"column": oTable.fnGetPosition( this )[2]
			};
		},
		"height": "14px"
	} );*/
        
        
    
        //hide meld columns
        //dependency
        oTable.fnSetColumnVis( 0, false );
        oTable.fnSetColumnVis( 1, false );
        oTable.fnSetColumnVis( 2, false );
        //at #breakpoint
        //oTable.fnSetColumnVis( 2, false );
    } );
    
    //fill form functions
    function trimNumber(s) {
          while (s.substr(0,1) == '0' && s.length>1) { s = s.substr(1,9999); }
          return s;
    }

    function fillTripleField(strid1, strid2, strid3, trid)
    {
        str = $("." + strid1 + trid)[0].innerHTML;
        arr = str.split('-');
        arr[0] = arr[0].trim();
        arr[1] = arr[1].trim();
        arr[2] = arr[2].trim();
        if(arr[0] != 'n/a') {
            $('#form_' + strid1).val(arr[0]);
        }
        if(arr[1] != 'n/a') {
            $('#form_' + strid2).val(arr[1]);
        }
        if(arr[2] != 'n/a') {
            $('#form_' + strid3).val(arr[2]);
        }
    }

    function fillDoubleField(strid1, strid2, trid)
    {
        str = $("." + strid1 + trid)[0].innerHTML;
        arr = str.split('-');
        arr[0] = arr[0].trim();
        arr[1] = arr[1].trim();
        if(arr[0] != 'n/a') {
            $('#form_' + strid1).val(arr[0]);
        }
        if(arr[1] != 'n/a') {
            $('#form_' + strid2).val(arr[1]);
        }
    }
    
    function fillDateField(strid, trid)
    {
        str = $("." + strid + trid)[0].innerHTML;
        arr = str.split('-');
        //alert(arr[0] + arr[1] + arr[2]); //yr, month, day
        $('#form_' + strid + '_year').val(arr[0]);
        $('#form_' + strid + '_month').val(trimNumber(arr[1]));
        $('#form_' + strid + '_day').val(trimNumber(arr[2]));
        
        //Form.Element.setValue($('form_' + strid + '_year'), arr[0]);
        //Form.Element.setValue($('form_' + strid + '_month'), trimNumber(arr[1]));
        //Form.Element.setValue($('form_' + strid + '_day'), trimNumber(arr[2]));
    }
    
    function fillField(strid, trid)
    {
        if($("." + strid + trid)[0].innerHTML != 'n/a')
            $('#form_' + strid).val($('.' + strid + trid)[0].innerHTML);
        
            //Form.Element.setValue($('form_' + strid), $$('.' + strid + trid)[0].innerHTML);
    }
    
    
    function fillSplitForm(trid)
    {
        
        tr = $("#" + trid);
        
        
        $("#form_DelCityYard").attr("disabled", "disabled");
        $("#form_DelProv").attr("disabled", "disabled");
        $("#form_DelAddress").attr("disabled", "disabled");
        //$("#splitForm" + trid + " .wo_number input").attr('disabled', 'disabled');
        //$("#splitForm" + trid + " .order_line_number input").attr('disabled', 'disabled');
        //$("#splitForm" + trid + " .original_quantity input").attr('disabled', 'disabled');
        //alert(tr);

        //SubStatus and BookingStatus
        fillDoubleField('SubStatus', 'BookingStatus', trid);
        fillDoubleField('ServiceType', 'SalesRep', trid);
        fillDoubleField('SKU', 'Dimensions', trid);
        //fillDoubleField('PuCityYard', 'PuProv', trid);
        
        //setDelCityYard('DelCityYard', 'DelProv', trid);
        

        //Form.Element.setValue($('form_PUDateSched'), $('PUDateSched').innerHTML);
        fillDateField('PuDateSched', trid);
        fillDateField('DelDateSched', trid);
        
        
        
        fillField('WO', trid);
        fillField('OrderLineNum', trid);
        fillField('OffloadRequired', trid);
        fillField('CarrierRequested', trid);
        fillField('Customer', trid);
        fillField('Quantity', trid); //add this
        fillField('OriginalQuantity', trid); //add this
        $('#form_FinalDestination').val($('#FinalDestination' + trid).text());
        
        //
        //
        fillTripleField('DelCityYard', 'DelProv', 'DelAddress', trid);
        //fillField('DelAddress', trid);
       
        //color dependency
        if(tr.attr("title") == 'quantity_parent'
      || $(".Quantity" + trid).css("background-color") == "rgb(255, 255, 192)") {
            $("#form_IsQuantityParent").val("1");
        }
        
        $("#form_mvLogisticLeg_id").val(trid); //hidden field
        $("#form_start_city").val(tr.find('.PuCityYard' + trid).first().text()); //hidden field
        
        //populate the hidden field for InitialLegID
        $("#form_PathID").val(tr.children().first().text());
        if(tr.attr("title") == 'parent')
            $("#form_InitialLegID").val('parent');
        else {
            //iterate upwards
            prev = tr.prev();
            while(prev.length) {
                if(prev.attr("title") == "parent") {
                    id = prev.attr("id");
                    $("#form_InitialLegID").val(id);
                    break;
                }
                prev = prev.prev();
            }
        }

        
        //
        
        
        //alert(trid);
        //alert("#form_mvLogisticLeg_id" + trid);
        //$("#form_mvLogisticLeg_id").val(trid);
    }
    
    
    $(document).ready(function() { 
        // bind 'myForm' and provide a simple callback function 
        var options = { 
            data: { 'start_id': start_id, 'end_id': end_id, 'delete_ids' : delete_ids },
            dataType:  'json'        // 'xml', 'script', or 'json' (expected server response type) 

        }; 
        
        $('#splitForm').ajaxForm(options, function(data) { 
            alert("Split action - success.");
        });
        
    }); 
    

   
    
    
    
    //meld function
    var meld_active = 0;
    function showMeld()
    {
        //dependency
        oTable.fnSetColumnVis( 0, true );
        oTable.fnSetColumnVis( 1, true );
        //oTable.fnSetColumnVis( 2, true );
        
        //$('.meld_hide').show();
        $('.split_column').hide();
        
        
        $('#last_hide_button').hide();
        $('#apply_meld').show();
        $('#cancel_meld').show();
        
        $("#splitForm").attr('action', "<?php echo $view['router']->generate('meld') ?>");
        
        meld_active = 1;
    }
    
    function cancelMeld()
    {
        
        //$('.meld_hide').hide();
        
        //dependency
        oTable.fnSetColumnVis( 0, false );
        oTable.fnSetColumnVis( 1, false );
        //oTable.fnSetColumnVis( 2, false );
        $('.split_column').show();
        $('#apply_meld').hide();
        $('#cancel_meld').hide();
        $('#last_hide_button').show();
        
        $("#splitForm").attr('action', "<?php echo $view['router']->generate('unassigned_legs_new') ?>");
        
        
        meld_active = 0;
    }
    
    
    //disable checkboxes system
    //start: disable all other starts and delete OK
    //start disable: enable only starts that arent disabled by delete OK --> enable them only if their delete is not checked --> iterate through ALL delete's, enable only starts if delete is off
    //               enable delete if both start and end are disabled NOT OK
    //               enable delete only if end is not checked NOT OK
    
    //end: disable all other ends and delete    OK
    //end disable: disable only ends that arent disabled by delete OK
    //             enable delete if both start and end are disabled  NOT OK
    //             enable delete only if start is not checked NOT OK
   
    
    //delete: disable both start and end OK
    //delete disable: enable start and end, only if they're not disabled OK
    
    
    /*
    function startEndDisable(arg) //arg: start_ or end_
    {
        $.each($('.meld_delete input:not(:checked)'), function() {
            //$(this)
            fnd = '.meld' + arg + ' input[id=' + $(this).attr("id").split('_')[0] + arg  + ']';
            $(fnd).attr('disabled', false);
        });
    }
    
    function enableDelete(id, type)
    {
        fnd_start = 'input:not(:checked)[id=' + id + '_start]';
        fnd_end = 'input:not(:checked)[id=' + id + '_end]';
        if(type == '_start') {
            if($(fnd_end).length == 1) {
                fnd = 'input[id=' + id + '_delete]';
                $(fnd).enable();
            }
                
        } else {
            if($(fnd_start).length == 1) {
                fnd = 'input[id=' + id + '_delete]';
                $(fnd).enable();
            }
        }
           
    }*/
    
    
    start_disabled = 0;
    $('.meld_start input').click(function() {
        //fnd = '.meld_delete input[id=' + $(this).attr("id").split('_')[0] + '_delete'  + ']';
        if(start_disabled) { //reenable them only if they're not disabled by delete
             $('.meld_start input').attr('disabled', false);
            //$(fnd).attr('disabled', false);
            start_disabled = 0;
            //enableDelete($(this).attr("id").split('_')[0], '_start');
            //goes off
            start_id = [];
        }
        else { 
            $('.meld_start input').attr('disabled', true);
            //$(fnd).attr('disabled', true);
            start_disabled = 1;
            //goes on
            start_id[0] = $(this).attr("id").split('_')[0];
            
        }
        $(this).enable();
        
        

    });
    
    end_disabled = 0;
    $('.meld_end input').click(function() {
        //fnd = '.meld_delete input[id=' + $(this).attr("id").split('_')[0] + '_delete'  + ']';
        if(end_disabled) {
            //$('.meld_end input').attr('disabled', false);
            $('.meld_end input').attr('disabled', false);
            //$(fnd).attr('disabled', false);
            end_disabled = 0;
            //enableDelete($(this).attr("id").split('_')[0], '_end');
            end_id = [];
        }
        else {
            $('.meld_end input').attr('disabled', true);
            //$(fnd).attr('disabled', true);
            end_disabled = 1;
            end_id[0] = $(this).attr("id").split('_')[0];
        }
        
        $(this).enable(); 
        
    });
    
    /*
    $('.meld_delete input').click(function() {
        fnd_start = '.meld_start input[id=' + $(this).attr("id").split('_')[0] + '_start'  + ']';
        fnd_end = '.meld_end input[id=' + $(this).attr("id").split('_')[0] + '_end'  + ']';

        if($(this).is(":checked")) {

            $(fnd_start).attr('disabled', true);

            $(fnd_end).attr('disabled', true);

            //turn on (delete the element with this id)
            delete_ids.push($(this).attr("id").split('_')[0]);
            
        } else {
            if(!start_disabled) //dont enable it if start is disabled, enable if start is not disabled
                $(fnd_start).attr('disabled', false);
            if(!end_disabled)
                $(fnd_end).attr('disabled', false);   
            
            //turn on (push id)
            var idx = delete_ids.indexOf($(this).attr("id").split('_')[0]);
            if(idx != -1) 
                delete_ids.splice(idx, 1); 
            //delete_ids.splice(0, 1, $(this).attr("id").split('_')[0]);
            
        }
        $(this).enable(); 

    });*/
    
    
    

    //hide features

    /**
     * Behaves just like the python range() built-in function.
     * Arguments:   [start,] stop[, step]
     *
     * @start   Number  start value
     * @stop    Number  stop value (excluded from result)
     * @step    Number  skip values by this step size
     *
     * Number.range() -> error: needs more arguments
     * Number.range(4) -> [0, 1, 2, 3]
     * Number.range(0) -> []
     * Number.range(0, 4) -> [0, 1, 2, 3]
     * Number.range(0, 4, 1) -> [0, 1, 2, 3]
     * Number.range(0, 4, -1) -> []
     * Number.range(4, 0, -1) -> [4, 3, 2, 1]
     * Number.range(0, 4, 5) -> [0]
     * Number.range(5, 0, 5) -> []
     *   Number.range(5, 4, 1) -> []
     * Number.range(0, 1, 0) -> error: step cannot be zero
     * Number.range(0.2, 4.0) -> [0, 1, 2, 3]
     */
    Number.range = function() {
      var start, end, step;
      var array = [];

      switch(arguments.length){
        case 0:
          throw new Error('range() expected at least 1 argument, got 0 - must be specified as [start,] stop[, step]');
          return array;
        case 1:
          start = 0;
          end = Math.floor(arguments[0]) - 1;
          step = 1;
          break;
        case 2:
        case 3:
        default:
          start = Math.floor(arguments[0]);
          end = Math.floor(arguments[1]) - 1;
          var s = arguments[2];
          if (typeof s === 'undefined'){
            s = 1;
          }
          step = Math.floor(s) || (function(){ throw new Error('range() step argument must not be zero'); })();
          break;
       }

      if (step > 0){
        for (var i = start; i <= end; i += step){
          array.push(i);
          
        }
      } else if (step < 0) {
        step = -step;
        if (start > end){
          for (var i = start; i > end + 1; i -= step){
            array.push(i);
          }
        }
      }
      return array;
    }
    
    //meld apply
    start_id = [];
    end_id = [];
    
    delete_ids = [];
    
    
    function findParentId(tr) //for parents and quantity parents combinations
    {
        if(tr.attr('title') != 'parent')
            prev = tr.prev();
        else
            return tr.attr('id');
        while(prev.length) {
            if(prev.attr('title') == 'parent')
                return prev.attr('id');
            prev = prev.prev();
        } 
        return 0;
    }
    
    function findChildParentOrQuantityId(tr) //for parents and quantity parents combinations
    {
        if(tr.attr('title') != 'parent')
            prev = tr.prev();
        else
            return tr.attr('id');
        while(prev.length) {
            if(prev.attr('title') == 'parent' || prev.attr('title') == 'quantity_parent')
                return prev.attr('id');
            prev = prev.prev();
        } 
        return 0;
    }
    
    //iterate from next row (after start_row), next match must be a child with the same end_id, if quantity_parent or grandparent is spotted during iteration or empty, fail
    function findChild(start_row)
    {
        next = start_row.next();
        while(next.length) {
            if(next.attr('id') == end_id[0] && next.attr('title') == 'child')
                return 1;
            if(next.attr('title') == 'parent' || next.attr('title') == 'quantity_parent')
                return 0;
            next = next.next();
        }
        alert('Meld validation failed.');
        return 0;
    }
    
    //parent, qparent or child
    function hasKids(tr) //for both quantity_parents / parents
    {
        if(tr.attr('title') == 'child') {
            alert('Wrong call.');
            return -1;
        }
        next = tr.next();
        if(!next.length)
            return 0;
        if(next.attr('title') != 'child')
            return 0;
        return 1;
    }
    /*
    function fillTripleField(strid1, strid2, strid3, trid)
    {
        str = $("." + strid1 + trid)[0].innerHTML;
        arr = str.split('-');
        arr[0] = arr[0].trim();
        arr[1] = arr[1].trim();
        arr[2] = arr[2].trim();
        if(arr[0] != 'n/a') {
            $('#form_' + strid1).val(arr[0]);
        }
        if(arr[1] != 'n/a') {
            $('#form_' + strid2).val(arr[1]);
        }
        if(arr[2] != 'n/a') {
            $('#form_' + strid3).val(arr[2]);
        }
    }
     **/
    
    //dependency: the 3 fields don't have '-' in their name
    function compareAddress(start_row, end_row)
    {
        start_address1 = $('.PuCityYard' + start_row.attr('id')).text();
        start_address2 = $('.PuCityYard' + end_row.attr('id')).text();
        
        explode_start1 = start_address1.split('-');
        explode_start2 = start_address2.split('-');
        
        end_address1 = $('.PuCityYard' + start_row.attr('id')).text();
        end_address2 = $('.PuCityYard' + end_row.attr('id')).text();
        
        explode_end1 = end_address1.split('-');
        explode_end2 = end_address2.split('-');
        
        if(explode_start1[0].trim() == explode_start2[0].trim()
        && explode_start1[1].trim() == explode_start2[1].trim()
        && explode_start1[2].trim() == explode_start2[2].trim()
        && explode_end1[0].trim() == explode_end2[0].trim()
        && explode_end1[1].trim() == explode_end2[1].trim()
        && explode_end1[2].trim() == explode_end2[2].trim())
            return true;
        
        
        
        return false;
    }
    
    //dependency: the 3 fields don't have '-' in their name
    function compareStartAddress(start_row, end_row)
    {
        start_address1 = $('.PuCityYard' + start_row.attr('id')).text();
        start_address2 = $('.PuCityYard' + end_row.attr('id')).text();
        
        explode_start1 = start_address1.split('-');
        explode_start2 = start_address2.split('-');
        

        
        if(explode_start1[0].trim() == explode_start2[0].trim()
        && explode_start1[1].trim() == explode_start2[1].trim()
        && explode_start1[2].trim() == explode_start2[2].trim())
            return true;
        
        
        
        return false;
    }
    
    function compareFinalAddress(start_row) //start_rows end city has to match final dest
    {
        start_address1 = $('.DelCityYard' + start_row.attr('id')).text();
        explode_start1 = start_address1.split('-');
 
        fin_dest = $("#FinalDestination" + start_row.attr('id')).text();
      
        explode_end1 = fin_dest.split('-');
        
        //alert(start_address1, " ", fin_dest);
        
        if(explode_start1[0].trim() == explode_end1[0].trim()
        && explode_start1[1].trim() == explode_end1[1].trim()
        && explode_start1[2].trim() == explode_end1[2].trim())
            return true;
        
        
        
        return false;
    }

    function sleep(milliseconds) {
      var start = new Date().getTime();
      for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds){
          break;
        }
      }
    }



    //function to check 
    
    function checkAssignedLegs(start_id, end_id)
    {
        //get all pathids whose tr id is >= start_id and <= end_id
        //get start_row, iterate next
            //if pathid isn't +1, return false, 
            //if same as end_id, return success
            //if null, return false - something went wrong
        
        found = "initial";
        $.ajax({
          type: 'POST',
          url: '<?php echo $view['router']->generate('ajax_error_meld_assign_job_legs') ?>',
          data: { "start_path_id" : $('.PathID' + start_id).text(), "end_path_id" : $(".PathID" + end_id).text() },
          async: false,
          success: function(data) {
              
              //alert(data.length);
              if(data.length != 2) {
                alert('An intermediary point in between is assigned to a job.\n' + data);
                found = 1;
                
              } else
                found = 0;
            

              
          }
        }); 
        if(found == 1)
            return false;
        return true;
        
    }
    
    //4.1.2011.
    function validateMeld()
    {
       //returns 1 for normal meld, 2 for quantity meld
       //start is a selected, it's sorted by PathID
       //it can be a grandparent, quantity parent and child
       //if grandparent, iterate to next rows, a child end must be met before anything else
       //if quantity parent, 
            //iterate to next rows, a child end must be met before anything else
            //if not found, check whether end row and start_row is a quantity parent with no siblings and under same grandparent
                //if true, meld those 2 into 1, and add quantity
            //for now ignore grandparents from this
       //if child, iterate to next rows, a child end must be met before anything else
       
       //iteration is done through PathID's
       //row = $();
       
       //get the start_row
       start_row = $("#" + start_id[0]);
       end_row = $("#" + end_id[0]);
       
       //if grandparent, end_row must be either a child or quantity parent, otherwise fail
            //if end_row child, iterate from next row (after start_row), next match must be a child with the same end_id, if quantity_parent or grandparent is spotted, or empty, fail
            //if end_row quantity parent, check if any of them have kids (start and end). if yes, fail. if no, check if the quantity parent's grandparent is the start_row's id.
       //if quantity parent, end_row must be quantity parent or a child
            //if end_row is a child, next match must be a child with the quantity parent start_row and same end_id, if quantity_parent or grandparent is spotted or empty, fail
            //if end_row is a quantity parent, check if any of them have kids, if yes, fail, if no, check whether they belong to same grandparent.
       //if child, end_row must be a child.
            //if not, fail, if yes, check the parent/quantity parent  of the start_row - then check the parent/quantity parent of the end_row. they have to match, otherwise fail.
       if(start_row.attr('title') == 'parent') {
           if(end_row.attr('title') == 'child') {
               return findChild(start_row) & checkAssignedLegs(start_id[0], end_id[0]);
           } else if(end_row.attr('title') == 'quantity_parent') { //here
               
               //if parent is a qparent aswell, compare the kids first, then addresses. otherwise, this is an error. --> dependency: color
               if($(".Quantity" + start_row.attr("id")).css("background-color") == "rgb(255, 255, 192)") {
                   if(!hasKids(start_row) && !hasKids(end_row)) {
                       if(compareStartAddress(start_row, end_row)) {
                           return 2;
                       } else {
                           alert('Meld validation failed: quantity parents must have the same origin (same start) and no children.');
                           return 0;
                       }
                   } else {
                       alert('One of the quantity parents has children.');
                       return 0;
                   }
               } else {
                   alert('Meld validation failed: grandparent is not a quantity parent.');
               }
               /*if(!hasKids(start_row) && !hasKids(end_row)) {
                   if(findParentId(end_row) == start_row.attr('id')) //instead of this func, we'll need a function compareAddresses(start_row, end_row).
                       return 2;
                   alert('Meld validation failed: quantity parent must have the start parent.');
                   return 0;
               } else {
                   alert('One of the parents has children.');
                   return 0;
               }*/
           } else {
               alert('Meld validation failed: cannot meld 2 parents.');
               return 0;
           }
       } else if(start_row.attr('title') == 'quantity_parent') {
           if(end_row.attr('title') == 'child') {
               return findChild(start_row) & checkAssignedLegs(start_id[0], end_id[0]);
           } else if(end_row.attr('title') == 'quantity_parent') { //here
               if(!hasKids(start_row) && !hasKids(end_row)) {
                   //if(findParentId(end_row) == findParentId(start_row))
                   //    return 2;
                   if(compareStartAddress(start_row, end_row) && compareFinalAddress(start_row) && compareFinalAddress(end_row))
                       return 2;
                   alert('Meld validation failed: quantity parents must have the same origin (same start) and no children.');
                   return 0;
               } else {
                   alert('One of the quantity parents has children.');
                   return 0;
               }
           } else {
               alert('Cannot meld outside the current route.');
               return 0;
           }
       } else if(start_row.attr('title') == 'child') {
           if(end_row.attr('title') == 'child') {
               if(findChildParentOrQuantityId(start_row) == findChildParentOrQuantityId(end_row)) {
                   return 1 & checkAssignedLegs(start_id[0], end_id[0]);
               } else {
                   alert('Selected childs dont have the same parent/quantity parent.');
                   return 0;
               }
           } else {
               alert('If start is a child, end must be a child aswell.');
               return 0;
           }
       }
       
        
    }
    
    function isRegularChild(start_id, end_id)
    {
        //find parent
        parent_id = findParentId($("#" + start_id));
        next = $("#" + parent_id);
        found = 0;
        while(next.length) {
            if(next.attr('title') == 'quantity_parent' || next.attr('title') == 'parent') {
                if(compareStartAddress(next, $("#" + start_id)) && start_id != next.attr('id') && end_id != next.attr('id')) //exists, create a qp
                    return false;
            }
            
            next = next.next();
        }
        return true;
        
    }
    
    //dependency --> PathID has to be exactly 3rd column
    function applyMeld()
    {
       if(start_id == "" || end_id == "" || start_id[0] == end_id[0]) {
           alert("Meld validation failed - start and end point must be selected and must not be the same.");
           return false;
       }
       //end cannot be a parent
       if($('#' + end_id).attr('title') == 'parent') {
           alert("Meld validation failed - end point cannot be a parent.");
           return false;
       }

       
       alert("Start id: " + start_id[0] + " End id: " + end_id[0]);
       //dependency: path has to be the 3rd column
       start_path_id = $(".PathID" + start_id[0]).text();
       end_path_id = $(".PathID" + end_id[0]).text();
       //start_path_id = $("#" + start_id[0]).children().eq(2).text();
       //end_path_id = $("#" + end_id[0]).children().eq(2).text();
       
       alert(start_path_id + " " + end_path_id);
       if(parseInt(start_path_id) > parseInt(end_path_id)) { //parse as INT
           alert("Meld validation failed - only forward melding is allowed. End path must come after the start path.");
           return false;
       }
       
       status = validateMeld();
       if(status == 2) { //1 for normal, 2 for quantity meld
           //all we need is an ajax request with 3 arguments: startid, endid, delete id's
            regular = isRegularChild(start_id[0], end_id[0]);
            array = { start_id : start_id[0], end_id : end_id[0], quantity : 1, regular: regular };
            //check for regular: if there are no other qp legs with same start location, create a regular child
            
            $.ajax({
              type: 'POST',
              url: '<?php echo $view['router']->generate('meld') ?>',
              data: array,
              success: function() {
                    delete_ids = [];
                    start_id = [];
                    end_id = [];
                    location.reload();
                    
              }
              //dataType: dataType
            });
            
       } else if(status == 1) {
           deletes = [];
           for (var i = start_path_id; i <= end_path_id; i++){
              deletes.push(i);
           }
           alert("Deleting PathID's: " + deletes);


           delete_ids = [];
           $("#legs tbody tr").each(function() {

                pid = $(this).children().eq(2).text();
                for(var i in deletes)
                {
                    if(deletes[i] == pid) {
                        delete_ids.push($(this).attr("id"));
                        break;
                    }
                }

           });

           alert("Delete ids: " + delete_ids);

           //all we need is an ajax request with 3 arguments: startid, endid, delete id's
           array = { delete_ids : delete_ids, start_id : start_id[0], end_id : end_id[0] };
            $.ajax({
              type: 'POST',
              url: '<?php echo $view['router']->generate('meld') ?>',
              data: array,
              success: function() {
                    delete_ids = [];
                    start_id = [];
                    end_id = [];
                  location.reload();
              }
              //dataType: dataType
            });
       } else {
            alert("Meld validation failed");
       }
    }
    


    function button_show()
    {
        col_hidden = 1;
        $('#show_button').show();
    }
    
    function show_columns() //dependency (number of columns)
    {
        //dependency
        indexes = Number.range(2, num_columns);
        
        for(var i in indexes) {
            oTable.fnSetColumnVis( indexes[i], true );
        }
        
        $('#show_button').hide();
    }
    
    
    
    /* Formating function for row details */
    function fnFormatDetails ( oTable, nTr )
    {
            var aData = oTable.fnGetData( nTr );//get data from row
            
            var sOut = "";
            
            
            //onSubmit="return validateSplitForm(' + nTr.id + '); return false;"
            //id="form' + nTr.id + '"
            sOut = '<form   action="<?php echo $view['router']->generate('unassigned_legs_new') ?>" method="post" id="splitForm' + nTr.id + '">';
            sOut += '<table>';
            
            var split_insert_row = $('.split_insert_row').html();
            
            
            sOut += split_insert_row;
            
            sOut += '</table>';
            sOut += '</form>';
            


            return sOut;
    }
    
    
    //3rd thing to do -- ability to have multiple forms open for splitting.
    
    
    function normaliseWidth(id)
    {
        $('.details').css('padding', '0 0 0 0');
        iter = 1;
        $("#splitForm" + id + " td").each(function() {
            //alert($(this)); 
            $(this).width($("#" + id + " td:nth-child(" + iter + ")").width());
            ++iter;
        });
    }
    
    function normaliseIds(id)
    {
        $("#splitForm" + id + " input, #splitForm" + id + " select").each(function() {
            $(this).attr("id", $(this).attr("id") + id);
        });

    }
   

    
    
    //Filtering
    //Dependency: adding columns to the applied filters table
    start_string = "";
    end_string = "";
    
    //dependency -- > start/end cities must not contain |'s
    function removeString(start_city, end_city)
    {
        //the substring has to be removed 100%
        //the only question is whether it has a | after it
        //first check for a str + | , if it exists, delete, otherwise, delete only the str
        
        //A     //on start
        //A|B   //on start, but not last
        //B|A|C  //in middle
        //simulate:
        //A|A|B
        old_start_string = start_string;
        
        
        //if the dependency above is fixed, then it can be done like this:
        /* if(start_string.length == 1) { //it's not length - it's about the number of |'s
            start_string = start_string.replace(start_city, "");
        } else {
            start_string = start_string.replace("|" + start_city, "");
            if(old_start_string == start_string)
                start_string = start_string.replace(start_city, "");
        }
        */
        
        start_string = start_string.replace(start_city + "|", "");
        if(old_start_string == start_string)
            start_string = start_string.replace("|" + start_city, "");
        if(old_start_string == start_string)
            start_string = start_string.replace(start_city, "");
        
        //start_string.replace(start_city, "");
        
        old_end_string = end_string;
        end_string = end_string.replace(end_city + "|", "");
        if(old_end_string == end_string)
            end_string = end_string.replace("|" + end_city, "");
        if(old_end_string == end_string)
            end_string = end_string.replace(end_city, "");
        //end_string.replace(end_city, "");
        
        
    }
    
    num_filters_active = 0;
    function populateFilter()
    {
        $("#start_city_filter input").val(start_string).keyup();
        $("#end_city_filter input").val(end_string).keyup();
    }
    
    function applyFiltering(n)
    {
        
        nd = $(n);
        tr = nd.parent().parent();
        
        
        
        start_city = tr.children().first().html();
        end_city = tr.children().first().next().html();
        
        alert("Filtering by " + start_city + " -> " + end_city);
        
        
        if(start_string != "")
            start_string += "|";
        start_string += start_city;
        if(end_string != "")
            end_string += "|";
        end_string += end_city;
        
        
        //fix this once and for all
        populateFilter();
        
        //put tr in the above table
        //#applied_routes
        //
        //
        
        tr.find('img').attr('onclick', 'removeFilter(this);');
        tr.find('img').attr('src', '/movit/bundles/movittest/datatables/examples/examples_support/details_close.png');
        new_tr = tr.clone().appendTo('#applied_routes tbody');
        
        
        //change the image's attribute onclick to something else

        
                
        node = nd.parent().parent()[0];
        //alert(node);
        groupTable.fnDeleteRow( node );
        //$("#applied_routes tbody").append(tr);
        
        ++num_filters_active;
        $("#reset_all").show();
        
    }
    
    
    function removeFilter(n)
    {
        tr = $(n).parent().parent();

        start_city = tr.children().first().html();
        end_city = tr.children().first().next().html();

        //alert(start_city + " " + end_city);
        //remove start_city and end_city from the global strings
        alert(start_city +  " " + end_city);
        removeString(start_city, end_city);


        populateFilter();

        tr.find('img').attr('onclick', 'applyFiltering(this)');
        tr.find('img').attr('src', '/movit/bundles/movittest/datatables/examples/examples_support/details_open.png');
        data = [];
        tr.children().each(function() {
           //alert($(this).html());
           data.push($(this).html());

        });
        //data.push("4th column");

        tr.remove();
        /*idx = $("#routes").dataTable().fnAddData([ //this creates a dependency
            data[0],
            data[1],
            data[2],
            data[3],
            data[4]
        ]);*/
        idx = $("#routes").dataTable().fnAddData(data);
        $("#routes").dataTable().fnDraw();
        //alert(idx);
        var nNode = groupTable.fnGetNodes( idx );

        //alert(nNode);
        obj = $(nNode).find('img'); //$.get(nNode);

        //alert(obj);

        //change onclick attribute
        --num_filters_active;
        if(num_filters_active == 0)
            $("#reset_all").hide();
    }

    function removeAllFilters()
    {
        $("#applied_routes img").each(function() {
            removeFilter($(this)[0]);
        });
    }
    
    
    //if #job is visible on page load, call the ajax to get it
    $(document).ready(function() {
        active = '<?php echo $active ?>';
        //alert(active);
        if(active == 'jobs') {
             $("#jobs").show();
             $('#formsQC').css('margin-left', '300px');
        } else if(active == 'none') {
            
        } else {
            $("#jobs").show();
            $("#" + active).click();
             $('#formsQC').css('margin-left', '300px');
        }
    });
    
</script>
    
    
    
</body>


</html>