function tablekit_ajax(table)
{
    var timestamp = new Date().getTime();
    var new_table = 'my_table' + timestamp;
    var url = 'ajax_orders';
    
    new Ajax.Updater( table, url, { 
        parameters: { timestamp: timestamp, table: table },
        
        asynchronous: true,
        onComplete: function() {
            new TableKit( new_table, { sortable: true, editable: false });



            if(table == 'mvLogisticOrders') {

                $('filter_button').observe('click', buttonClick);
                $('clear_button').observe('click', buttonClearClick);
                
      
                $('show_button', 'hide_button').each(function(s) 
                {         
                      s.observe('click', function(event) {
                          if(this.id == 'show_button')
                              buttonShowHideClick(table, 'show');
                          else
                              buttonShowHideClick(table, 'hide');
                          //alert(this.id);
                      });
                }); 
            } else {
                
                      
                $('job_show_button', 'job_hide_button').each(function(s) 
                {         
                      s.observe('click', function(event) {
                          if(this.id == 'job_show_button')
                              buttonShowHideClick(table, 'show');
                          else
                              buttonShowHideClick(table, 'hide');
                          //alert(this.id);
                      });
                }); 
                //missing filter and clear 

            }
            
            
            //$('job_show_button').observe('click', buttonClick);
            //$('hide_button').observe('click', buttonHideClick);
            //$('clear_button').observe('click', buttonClearClick);
      }
    });
}


function buttonClick(event){

    //tablekit_ajax();
    
    var timestamp = new Date().getTime();
    var new_table = 'my_table' + timestamp;
    
    $('orders_filter_form').request({ //form request
      method: 'post',
      parameters: { timestamp: timestamp, clk: 'yes', table: 'mvLogisticOrders' },
      onComplete: function(data){
          
          $('mvLogisticOrders').update(data.responseText);
          new TableKit( new_table, { sortable: true, editable: false });
          $('show_button').observe('click', buttonShowClick);
          $('button').observe('click', buttonClick);
          $('hide_button').observe('click', buttonHideClick);
          $('clear_button').observe('click', buttonClearClick);
          
          $('filter_row').show();
          $('show_button').hide();
          $('hide_button').show();
      }
    }) 

}


$('jobs_button').observe('click', buttonClick2);

function buttonClick2(event){

    //tablekit_ajax();
    
    var timestamp = new Date().getTime();
    var new_table = 'my_table' + timestamp;
    
    $('jobs_filter_form').request({ //form request
      method: 'post',
      parameters: { timestamp: timestamp, clk: 'yes', table: 'mvLogisticJobs' },
      onComplete: function(data){
          
          $('mvLogisticJobs').update(data.responseText);
          new TableKit( new_table, { sortable: true, editable: false });
      }
    }) 

}

function buttonShowHideClick(table, action)
{
    if(table == 'mvLogisticOrders') {
        if(action == 'show') {
            $('filter_row').show();
            $('show_button').hide();
            $('hide_button').show();
        } else {
            $('filter_row').hide();
            $('show_button').show();
            $('hide_button').hide();
        }
    } else {
        if(action == 'show') {
            $('job_filter_row').show();
            $('job_show_button').hide();
            $('job_hide_button').show();
        } else {
            $('job_filter_row').hide();
            $('job_show_button').show();
            $('job_hide_button').hide();
        }
    }
 
}


document.observe("dom:loaded", function() {
    tablekit_ajax('mvLogisticOrders');
    tablekit_ajax('mvLogisticJobs');
      

    

});

function buttonClearClick(event){
    
    //alert('ok');
    $$('.activate input, .activate select').each(function(s) 
    {         
        s.clear();     
    });

    //$('form_mvLogisticOrder_id_from').reset(); //won't work, don't know why
}


