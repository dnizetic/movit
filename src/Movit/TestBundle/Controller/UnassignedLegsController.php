<?php

namespace Movit\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;


use Movit\TestBundle\Entity\mvLogisticLegs as Leg;
use Movit\TestBundle\Entity\mvLogisticJob as Job;
use Movit\TestBundle\Entity\mvLogisticLegHistory as History;
use Movit\TestBundle\Entity\BaseObject as BaseObject;

class UnassignedLegsController extends Controller {


    private function getLegForm($defaultData = null) {

        //validation
        //http://symfony.com/doc/2.0/book/forms.html#form-validation
        return $this->createFormBuilder($defaultData)

                //numeric
                ->add('mvLogisticJob_id', 'integer', array('required' => false))
                ->add('units', 'integer', array('required' => false))
                ->add('WO', 'hidden', array('required' => false, 'max_length' => 50))
                ->add('OrderLineNum', 'hidden', array('required' => false, 'max_length' => 50, 'label' => 'Order Line Number'))
                ->add('ReleaseId', 'text', array('required' => false, 'max_length' => 50))
                ->add('PuCityYard', 'text', array('required' => false, 'max_length' => 50))
                ->add('PuProv', 'text', array('required' => false, 'max_length' => 2))
                ->add('PuDateSched', 'date', array('required' => false, 'label' => 'Pu Date Sched'))
                ->add('DelCityYard', 'text', array('required' => false, 'max_length' => 50, 'label' => 'Del City Yard'))
                ->add('DelProv', 'text', array('required' => false, 'max_length' => 2))
                ->add('DelDateSched', 'date', array('required' => false, 'label' => 'Del Date Sched'))
                ->add('Status', 'choice', array('choices' => array('0' => 0, '1' => 1)))
                ->add('SubStatus', 'text', array('required' => false, 'max_length' => 50))
                ->add('BookingStatus', 'text', array('required' => false, 'max_length' => 50, 'label' => 'Booking Status'))
                ->add('ServiceType', 'text', array('required' => false, 'max_length' => 50))
                ->add('SKU', 'text', array('required' => false, 'max_length' => 20))
                ->add('OffloadRequired', 'choice', array('choices' => array('0' => 0, '1' => 1), 'label' => "Offload Required"))
                ->add('CarrierRequested', 'text', array('required' => false, 'max_length' => 50, 'label' => "Carrier Req"))
                ->add('Customer', 'text', array('required' => false, 'max_length' => 255))
                ->add('lwh', 'text', array('required' => false, 'max_length' => 50))
                ->add('SalesRep', 'text', array('required' => false, 'max_length' => 255))
                ->add('Deleted', 'choice', array('choices' => array('0' => 0, '1' => 1)))

                ->add('mvLogisticLeg_id', 'hidden', array('property_path' => false))
                ->add('mvLogisticParent_id', 'hidden', array('property_path' => false))

                ->add('start_city', 'hidden', array('property_path' => false))
                ->add('InitialLegID', 'hidden', array('required' => false))
                ->add('PathID', 'hidden', array('required' => false))
                
                ->add('Quantity', 'integer', array('required' => false))
                ->add('OriginalQuantity', 'hidden', array('required' => false))
                ->add('IsQuantityParent', 'hidden', array('required' => false))
                
                
                ->add('PuAddress', 'text', array('required' => false))
                ->add('DelAddress', 'text', array('required' => false))
                //
                ->add('FinalDestination', 'hidden', array('required' => false))
                ->getForm();
    }


    /**
     * Ajax for index page.
     *
     * @Route("/unassigned-legs", name="unassigned_legs")
     * @Template()
     */
    public function indexAction() {

        //group1: Substatus and Booking status

        die(var_dump($configuration['soap']));
        $dal_class = $this->get('DAL')->getInstance();
        $form = $this->getLegForm();

        $request = $this->getRequest();

        //$tsql = "SELECT * FROM mvLogisticLegs ORDER BY mvLogisticLeg_id ASC";



        if ($request->getMethod() == 'POST') {

            $form->bindRequest($request);

            if ($form->isValid()) {


                $data = $form->getData();
                $leg = new Leg($data);
                $leg->save();
                $new = 1;
            }
        }

        //die(var_dump($tsql));
        $legs = $dal_class->query($tsql);

        $ff = $this->getLegFilterForm();
        return $this->render('MovitTestBundle:UnassignedLegs:index.html.php', array('legs' => $legs, 'form' => $form->createView(),
                    'ff' => $ff->createView(), 'form2' => $form->createView()));
    }
    
    /**
     * Ajax for index page.
     *
     * @Route("/reset-database", name="reset_database")
     * @Template()
     */
    public function resetDatabaseAction() {
        $dal_class = $this->get('DAL')->getInstance();
        
        $tsql = "DELETE FROM mvLogisticLegs WHERE mvLogisticLeg_id > 516";
        $dal_class->query($tsql);
        
        
        
        
        $tsql = "DELETE FROM mvLogisticLegHistory";
        $dal_class->query($tsql);
        
        $tsql = "UPDATE mvLogisticLegs SET Deleted = 0 WHERE mvLogisticLeg_id = 513 OR mvLogisticLeg_id = 515 OR mvLogisticLeg_id = 516";
        $dal_class->query($tsql);
        
        $tsql = "UPDATE mvLogisticLegs SET PathID = 0 WHERE mvLogisticLeg_id = 513";
        $dal_class->query($tsql);
        
        $tsql = "UPDATE mvLogisticLegs SET PathID = 1 WHERE mvLogisticLeg_id = 515";
        $dal_class->query($tsql);
        
        $tsql = "UPDATE mvLogisticLegs SET PathID = 2 WHERE mvLogisticLeg_id = 516";
        $dal_class->query($tsql);
        
        die("Success");
    }
    
    /**
     * Ajax for index page.
     *
     * @Route("/reset-database-leg-jobs", name="reset_leg_jobs")
     * @Template()
     */
    public function resetDatabaseLegsAction() {
        $dal_class = $this->get('DAL')->getInstance();
        
        $tsql = "UPDATE mvLogisticLegs SET mvLogisticJob_id = NULL";
        $dal_class->query($tsql);
    
        die("Success");
    }
    

    /**
     * Ajax for index page.
     *
     * @Route("/split-undo", name="split_undo")
     * @Template()
     */
    public function splitUndoAction() {


        $dal_class = $this->get('DAL')->getInstance();
        $bo = new BaseObject();


        $request = $this->getRequest();
        
        //die(var_dump($request->request));

        $split_leg_id = $request->request->get('split_leg_id');
        $start_id = $request->request->get('start_id');
        $end_id = $request->request->get('end_id');
        
        
        /*
3 args: split leg ID - set Deleted to 0
start_leg ID, end_leg ID - set Deleted to 1

All after end leg id's path ID have to be decremented by 1.
         */
        
        $obj = $bo->find($start_id, 'mvLogisticLegs');
        $obj->setDeleted(1);        
        $obj = $bo->find($end_id, 'mvLogisticLegs');
        $obj->setDeleted(1);
        
        $obj = $bo->find($split_leg_id, 'mvLogisticLegs');
        $obj->setDeleted(0);
        
        //if parent, normalise ID's again
        if($obj->InitialLegID == NULL) {
            $this->normaliseIDs($split_leg_id);
        }
        
        $end_leg = $bo->find($end_id, 'mvLogisticLegs');
        $end_leg_path_id = $end_leg->PathID;
        
        $this->cascadePath($end_leg_path_id + 1, -1);
        //if a new parent was in question, we need to normalise ID's
        
        //if the split leg ID is a parent, then we need to normalise them.
        
        die("Success");
    }
    
    
    
    private function meldCascadePath($offset, $start)
    {
        $dal_class = $this->get('DAL')->getInstance();
        $leg = new Leg();
        //path crawler: select parent's end city and ID, then select it's childs (where InitiaLegID == the ID)
        /*$tsql = "SELECT mvLogisticLeg_id FROM mvLogisticLegs WHERE Deleted = '0' AND InitialLegID IS NULL";
        $parent = $dal_class->query($tsql);
        
        $id = $parent[0]->get('mvLogisticLeg_id');*/
        $tsql = "SELECT PathID, mvLogisticLeg_id FROM mvLogisticLegs WHERE Deleted = '0' AND PathID >= $offset ORDER BY PathID ASC"; //here.
        $childs = $dal_class->query($tsql);
        
        
        
        //die(var_dump($childs));
        foreach($childs as $key => $child) {
            $leg->mvLogisticLeg_id = $child->get('mvLogisticLeg_id');
            $new_id = $start;
           // die(var_dump($new_id));
            $leg->setPathID($new_id);
            ++$start;
        }
        
    }
    
    


    
    
    /**
     * Ajax for index page.
     *
     * @Route("/meld", name="meld")
     * @method("post")
     * @Template()
     */
    public function meldAjaxAction() {

        $dal_class = $this->get('DAL')->getInstance();
        $bo = new BaseObject();


        $request = $this->getRequest();
        
        //die(var_dump($request->request));

        $ids = $request->request->get('delete_ids');
        $start_id = $request->request->get('start_id');
        $end_id = $request->request->get('end_id');
        $quantity = $request->request->get('quantity');
        $regular = $request->request->get('regular');
        
        
        //quantity Meld is somewhat different
       
        
        //$start_id = $start_id[0];
        //$end_id = $end_id[0];

        if ($ids != NULL)
            foreach ($ids as $id) {
                $obj = $bo->find($id, 'mvLogisticLegs');

                $obj->setDeleted(1);
            }
            
        $start_leg = $bo->find($start_id, 'mvLogisticLegs');
        
       
        $end_leg = $bo->find($end_id, 'mvLogisticLegs');
       
        $start_leg->setDeleted(1);
        $end_leg->setDeleted(1);
        
        //if start leg has InitialLegID NULL (grandparent), then set always to 0
        //$leg = new Leg();
        $start_leg->persisted = 0; //because during meld, a new leg is created with this ID
        $start_leg->Deleted = 0;
        $start_leg->DelCityYard = $end_leg->DelCityYard;
        $start_leg->DelProv = $end_leg->DelProv;
        $start_leg->DelAddress = $end_leg->DelAddress;

        
        //if($start_leg->InitialLegID == NULL && $quantity == NULL) //if grandparent
        //    $start_leg->IsQuantityParent = 0;
        if($quantity == 1/* && $start_leg->InitialLegID*/) {
            
            if($start_leg->InitialLegID == NULL) {
                if($start_leg->IsQuantityParent) {
                    $start_leg->Quantity += $end_leg->Quantity;
                }
            } else 
                $start_leg->Quantity += $end_leg->Quantity;
            
            if($regular != "false") {
                $start_leg->IsQuantityParent = 0;
            }
           /* if($start_leg->InitialLegID != NULL)
                $start_leg->IsQuantityParent = 1;
            else
                $start_leg->IsQuantityParent = 0;*/
        }

        //call a query to check whether there are InitialLeg
        
        
        $end_path_id = $end_leg->PathID;
        $start_leg->save(); //we need this ID if it was a parent
        if($start_leg->InitialLegID == NULL) { //if parent was a starting point of meld, we need to call the ID cascading function
            
            $inserted_id = $dal_class->query("SELECT IDENT_CURRENT('mvLogisticLegs') as last_id;");
            $last_id = $inserted_id[0]->get('last_id');
            $count = $this->normaliseIDs($last_id, $start_id); //return number of normalised ID's, based on this number, decide whether to the leg not a quantity parent
            if($count == 0) { //setIsQuantityParent
                $leg = new Leg();
                $leg->mvLogisticLeg_id = $last_id;
                $leg->setIsQuantityParent(0);
            }
                
        }
        //check end_path + 1 leg. it cannot be a child because it was validated by the js. it can be either a gparent or a qparent.
        //if it's a qparent, it means 
        
        if(!$quantity)
            $this->meldCascadePath($end_leg->PathID + 1, ($start_leg->PathID ? $start_leg->PathID : 0) + 1); //obrnuti proces
        else
            $this->meldCascadePath($end_leg->PathID + 1, $end_leg->PathID); //obrnuti proces;
        
        die(var_dump("Success"));
        //create a new leg with:
        //PathID: same as start_leg's path id
        //End City set end_legs end city
        //Set Deleted back to 0
        //Everything else same as start_leg
        
        
            

        if($ids)
            die("Deleted ids: " . var_dump($ids));
        else
            die("Meld OK");


    }

   
    

    private function normaliseIDs($last_id, $old_initial = 0)
    {
        $dal_class = $this->get('DAL')->getInstance();
        $leg = new Leg();
        
        $tsql = "SELECT mvLogisticLeg_id FROM mvLogisticLegs WHERE Deleted = '0' AND InitialLegID = $old_initial";
        $childs = $dal_class->query($tsql);
        
        $count = 0;
        foreach($childs as $child) {
            
            $leg->mvLogisticLeg_id = $child->get('mvLogisticLeg_id');
            $leg->setInitialLegID($last_id);
            ++$count;
 
        }
        return $count;
    }


    private function cascadePath($offset, $step = 1)
    {
        $dal_class = $this->get('DAL')->getInstance();
        $leg = new Leg();
        //path crawler: select parent's end city and ID, then select it's childs (where InitiaLegID == the ID)
        /*$tsql = "SELECT mvLogisticLeg_id FROM mvLogisticLegs WHERE Deleted = '0' AND InitialLegID IS NULL";
        $parent = $dal_class->query($tsql);
        
        
        $id = $parent[0]->get('mvLogisticLeg_id');
        $tsql = "SELECT PathID, mvLogisticLeg_id FROM mvLogisticLegs WHERE Deleted = '0' AND InitialLegID = $id AND PathID >= $offset"; //here.*/
        $tsql = "SELECT PathID, mvLogisticLeg_id FROM mvLogisticLegs WHERE Deleted = '0' AND PathID >= $offset";
        $childs = $dal_class->query($tsql);
        
        
        //die(var_dump($childs));
        foreach($childs as $key => $child) {
            $leg->mvLogisticLeg_id = $child->get('mvLogisticLeg_id');
            $new_id = $child->get('PathID') + $step;
           // die(var_dump($new_id));
            $leg->setPathID($new_id);
        }

    }
    
    /*
    private function cascadeStep($offset, $wo, $line, $step = 1)
    {
        $dal_class = $this->get('DAL')->getInstance();
        $leg = new Leg();

        $tsql = "SELECT Step, mvLogisticLeg_id FROM mvLogisticLegs WHERE WO = $wo AND OrderLineNum = $line AND Deleted = '0' AND Step >= $offset";
        $childs = $dal_class->query($tsql);
        
        foreach($childs as $key => $child) {
            $leg->mvLogisticLeg_id = $child->get('mvLogisticLeg_id');
            $new_id = $child->get('Step') + $step;
            $leg->setStep($new_id);
        }

    } */
    
    
    //meldCascadepath, cascadepath, normalise IDS and the function

    /**
     * Ajax for index page.
     *
     * @Route("/unassigned-legs-new", name="unassigned_legs_new")
     * @Template()
     */
    public function unassignedlegsAction() 
    {
      
        
        $dal_class = $this->get('DAL')->getInstance();

        $form = $this->getLegForm();
        $request = $this->getRequest();
        
        if ($request->getMethod() == 'POST') {


            $form->bindRequest($request);

            if ($form->isValid()) { 
                
                $data = $form->getData();
                //first, we need to mark the split leg as deleted.
                //get id FIRST
                $form_data = $request->request->get('form'); //includes non property fields
                $id = $form_data['mvLogisticLeg_id'];
                $split_leg_id = $id;
                
                //split LEG
                $leg = new Leg();
                $leg->mvLogisticLeg_id = $id; //this is OK for deletion
                $leg->setDeleted(1);
                
                
                $tsql = "SELECT PathID, Quantity FROM mvLogisticLegs WHERE mvLogisticLeg_id = $split_leg_id";
                $split_leg = $dal_class->query($tsql);
                $split_leg_path_id = $split_leg[0]->get('PathID');
                
                
                
                
                $parent = $data['InitialLegID'];
                
                $start_city = explode(' - ', $form_data['start_city']);
                $backup_data = $data;
                //create another one (current)
                //edit the end city to start_city of the newly created leg
                $backup_data['PuCityYard'] = $start_city[0];
                $backup_data['PuProv'] = $start_city[1];
                $backup_data['PuAddress'] = $start_city[2];
                $backup_data['DelCityYard'] = $data['PuCityYard'];
                $backup_data['DelProv'] = $data['PuProv'];
                $backup_data['DelAddress'] = $data['PuAddress'];
                // we need start city of the above ID
                //die(var_dump($backup_data));
                //if data has InitialLegID set to null, it's a parent - set it to this ID to other legs, meaning the below leg needs to have the value set!
                if($parent == 'parent') { //if parent isn't null 
                    $backup_data['InitialLegID'] = NULL; 
                    $backup_data['PathID'] = $split_leg_path_id;
                    //$backup_data['QuantityParent'] = 0;
                    
                } else { //child - keeps path ID
                    //keeps his path ID always
                    $backup_data['PathID'] = $split_leg_path_id;
                    
                }
                //parent or child, we need to set it to true if it was a quantity split
                if($split_leg[0]->get('Quantity') != $data['Quantity'])
                    $backup_data['IsQuantityParent'] = 1;
                $leg = new Leg($backup_data);
                $leg->save(); //get last inserted ID - this won't work if they're both childs. We need to send it as a hidden argument (ID of the parent).
                //die(var_dump($arr));
                
                $inserted_id = $dal_class->query("SELECT IDENT_CURRENT('mvLogisticLegs') as last_id;");
                $last_id = $inserted_id[0]->get('last_id');
                $start_last_id = $last_id;

                $parent_id = null;
                $cascade_offset = "";
                if($parent == 'parent') {//needs to be cascaded, not just this one - All that have deleted 0 and are children, need to have this as last id now.
                    $data['InitialLegID'] = $last_id; //fetch last inserted ID (above leg).
                    $parent_id = $last_id;
                    
                    $this->normaliseIDs($last_id, $split_leg_id);
                    
                    //$data['PathID'] = 1; //
                    $cascade_offset = $data['PathID'] = $split_leg_path_id + 1;
                    //cascade ALL
                } else {

                    $cascade_offset = $data['PathID'] = $split_leg_path_id + 1;
                }
                
                $data['IsQuantityParent'] = 0;
                
                //$split_leg_step_id = $split_leg->get('Step');
                //$step_cascade = $data['Step'] = $split_leg_step_id + 1;
                
                //cascade PathID's
                $this->cascadePath($cascade_offset); //PathID: 2, you need 2, 3 - those >= 3 need to be moved
                //that's the reason cascading is before saving
             
     
                //$this->cascadeStep($step_cascade); 
     
                
                $leg = new Leg($data);
                $leg->save();
                
                
                $inserted_id = $dal_class->query("SELECT IDENT_CURRENT('mvLogisticLegs') as last_id;");
                $last_id = $inserted_id[0]->get('last_id');
                $end_last_id = $last_id;
                
                //quantitySplit
                //1. check if quantity's aren't the same
                $type = $parent;
                if($split_leg[0]->get('Quantity') != $data['Quantity'])
                   $this->quantitySplit($type, $last_id, $split_leg_id, 
                           $split_leg[0]->get('Quantity') - $data['Quantity'], $parent_id);
                
                

               
                //History table.
                $hist = new History();
                $hist->Date = date('Y-m-d H:i:s');
                $hist->SplitLegID = $split_leg_id;
                $hist->StartLegID = $start_last_id;
                $hist->EndLegID = $end_last_id;
                $hist->Type = 0; //0 for route, 1 for quantity
                $hist->Action = 0; //0 for split, 1 for meld
                $hist->save();
                  
                //path crawler: take all legs from the DB. select parent, then select it's childs.
                //$this->executePathCrawler($last_id);
                
                
                $id = $request->request->get('id');
                $data['id'] = $id;
                return $this->redirect($this->generateUrl('unassigned_legs_new'));
                //return $this->render('MovitTestBundle:UnassignedLegs:unassigned_legs_ajax.html.php', array('data' => $data, 'id' => $id));
             
            }
        }
        
        $tsql = "SELECT * FROM mvLogisticLegs WHERE Deleted = '0' AND mvLogisticJob_id IS NULL ORDER BY PathID ASC";
        $legs = $dal_class->query($tsql);
        
        
        //
        $tsql = "SELECT PUCityYard, DelCityYard, COUNT(*) as 'num_legs', SUM(Quantity) as quantity_sum FROM mvLogisticLegs
             WHERE Deleted = '0'
             GROUP BY [PUCityYard], [DelCityYard]";
        
        $unique_routes = $dal_class->query($tsql);
        
        
        $tsql = "SELECT * FROM mvLogisticLegHistory ORDER BY Date DESC";
        $history = $dal_class->query($tsql);
        
        
        $tsql = "SELECT TOP 10 mvLogisticJob_id FROM mvLogisticJobs ORDER BY mvLogisticJob_id DESC";
        $jobs = $dal_class->query($tsql);

        
        $sess  = $this->get("session");
        $sess->set( 'some_key', 'some_value' );
        $val = $sess->get('some_key');
        $active = $sess->get( 'active' );
        
        
        return $this->render('MovitTestBundle:UnassignedLegs:unassigned_legs.html.php', 
                array('legs' => $legs, 'form' => $form->createView(),
                      'unique_routes' => $unique_routes,
                      'history' => $history, 'jobs' => $jobs,
                      'active' => $active));
    }
    
    
    //2.1.2012.
    private function quantitySplit($type, $last_leg_id, $split_leg_id, $quantity, /* step_offset, */ $initial_id = null)
    {
    
            
        //get the last leg inserted and get it's path ID, create space for
        //a new leg
        $bo = new BaseObject();
        $last_leg = $bo->find($last_leg_id, 'mvLogisticLegs');
        $path_offset = $last_leg->PathID;
        $this->cascadePath($path_offset + 1);
        //cascade Step aswell
        //$this->cascadeStep($step_offset + 1);

        //now we have room to insert at path_offset
        //so, get the Split leg and insert a new one
        $split_leg = $bo->find($split_leg_id, 'mvLogisticLegs');
        $split_leg->persisted = 0; //because during meld, a new leg is created with this ID
        $split_leg->Deleted = 0;
        $split_leg->PathID = $path_offset + 1;
        //set Step to step_offset + 1
        //$split_leg->Step = $step_offset + 1;
        $split_leg->IsQuantityParent = 1;
        $split_leg->Quantity = $quantity;


        //if type was parent, we need to set InitialLegID properly
        if($type == 'parent') {
           $split_leg->InitialLegID = $initial_id;
        }

        $split_leg->save();

    }
    
    
    
    
    
    
    /**
     * Ajax for index page.
     *
     * @Route("/ajax-job-legs", name="ajax_job_legs")
     * @method("post")
     * @Template()
     */
    public function ajaxJobLegsAction() {

        $dal_class = $this->get('DAL')->getInstance();


        $request = $this->getRequest();
        
        //die(var_dump($request->request));

        $job_id = $request->request->get('job_id');
        
        
        $legs = $dal_class->query("SELECT mvLogisticLeg_id, PuCityYard, DelCityYard FROM mvLogisticLegs WHERE mvLogisticJob_id = ? AND Deleted = '0'", $job_id);
        
        return $this->render('MovitTestBundle:UnassignedLegs:ajaxJobLegs.html.php', 
                array('legs' => $legs ? $legs : array(), 'job_id' => $job_id));

    }
    
    
    /**
     * Ajax for index page.
     *
     * @Route("/ajax-assign-job-legs", name="ajax_assign_job_legs")
     * @method("post")
     * @Template()
     */
    public function ajaxAssignJobLegsAction() {

        $dal_class = $this->get('DAL')->getInstance();


        $request = $this->getRequest();
        
        //die(var_dump($request->request));

        $job_id = $request->request->get('job_id');
        $leg_id = $request->request->get('leg_id');
        
        //if job_id is 0, concatenate null, otherwise this
        if(!$job_id)
            $tsql = "UPDATE mvLogisticLegs SET mvLogisticJob_id = NULL  WHERE mvLogisticLeg_id = $leg_id";
        else
            $tsql = "UPDATE mvLogisticLegs SET mvLogisticJob_id = $job_id  WHERE mvLogisticLeg_id = $leg_id";
        $dal_class->query($tsql);
        
        die("Success");

    }
    
    /**
     * Ajax for index page.
     *
     * @Route("/ajax-error-meld-assign-job-legs", name="ajax_error_meld_assign_job_legs")
     * @method("post")
     * @Template()
     */
    public function ajaxErrorAssignJobLegsAction() {

        $dal_class = $this->get('DAL')->getInstance();


        $request = $this->getRequest();
        
        //die(var_dump($request->request));

        $start_id = $request->request->get('start_path_id');
        $end_id = $request->request->get('end_path_id');
        
        
        $tsql = "SELECT mvLogisticLeg_id, mvLogisticJob_id FROM mvLogisticLegs WHERE Deleted = '0' AND PathID > $start_id AND PathID < $end_id AND mvLogisticJob_id IS NOT NULL";
        $failed_legs = $dal_class->query($tsql);
        
        if(count($failed_legs) > 0) {
            $error = "";
            foreach($failed_legs as $leg)
            {
                $error .= "LegID: " . $leg->get('mvLogisticLeg_id') . ", JobID: " . $leg->get('mvLogisticJob_id');
                $error .= "\n";
            }

            die($error);
        } else {
            die();
        }
       

    }
    
    
    
    /**
     * Ajax for sessions.
     *
     * @Route("/ajax-session", name="ajax_session")
     * @method("post")
     * @Template()
     */
    public function ajaxSessionAction() {

        $dal_class = $this->get('DAL')->getInstance();


        $request = $this->getRequest();
        
        //die(var_dump($request->request));
        $sess  = $this->get("session");
        $sess->set( 'some_key', 'some_value' );
        $val = $sess->get('some_key');
        
        $active = $request->request->get('active');
        
        $sess->set( 'active', $active );
    
        
        
        
        die($sess->get('active'));
       

    }
    
    
    /**
     * Ajax for sessions.
     *
     * @Route("/create-job", name="create_job")
     * @method("post")
     * @Template()
     */
    public function createJobAction() {

        $dal_class = $this->get('DAL')->getInstance();


        $request = $this->getRequest();
        
        $job = new Job();
        
        $job->mySave();
        
        die("Success");
    }

}