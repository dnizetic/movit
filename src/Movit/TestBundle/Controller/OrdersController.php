<?php 
namespace Movit\TestBundle\Controller;
	
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;


use Movit\TestBundle\Entity\mvLogisticOrders as Order;
use Movit\TestBundle\Entity\BaseObject as BaseObject;


class OrdersController extends Controller
{
    
    private function CRUDOrderForm($defaultData = null)
    {
        //validation
        //http://symfony.com/doc/2.0/book/forms.html#form-validation
        return $this->createFormBuilder($defaultData)
             ->add('TruckAllocatedTotal', 'money', array('required' => false))
             ->add('TruckingCostTotal', 'money', array('required' => false))
             ->add('TruckingCostPercent', 'number', array('required' => false))
             ->add('Date', 'date', array('required' => false))
             ->add('DateCommitted', 'date', array('required' => false))
             ->add('Salesperson_id', 'integer', array('required' => false))
                
             ->getForm();
    }
    
    
   /**
     * Displays a form to create a new Order entity.
     *
     * @Route("/order/new", name="order_new")
     * @Template()
     */
    public function neworderAction()
    {
        
        $dal_class = $this->get('DAL')->getInstance();
        $order = new Order();
        
        $form = $this->CRUDOrderForm();
        $request = $this->getRequest();
        
        if($request->getMethod() == 'POST') {
                $form->bindRequest($request);
                
                if($form->isValid()) { //add is_valid() method, for error messagess here
                    $data = $form->getData();
                    $order = new Order($data);
                    $order->save(); 
                    
                    return $this->redirect($this->generateUrl('MovitTestBundle_homepage')); //contact is name of route
                }
        }
        $error = '';
        
        return $this->render('MovitTestBundle:Orders:new.html.php', array('results' => 'No',
                    'form' => $form->createView(), 'error' => $error ));
    }
    
    
   /**
     * Displays a form to edit an existing Order entity.
     *
     * @Route("/order/edit/{id}", name="order_edit")
     * @Template()
     */
    public function editorderAction($id) //user can only edit his addresses
    {
        //get the object from the DB
        
        $request = $this->getRequest();
        if($request->getMethod() == 'POST') {
            
                $form = $this->CRUDOrderForm();
                $form->bindRequest($request);
                
                if($form->isValid()) { //add is_valid() method, for error messagess here
                    $data = $form->getData();

                    $order = new Order($data);
   
                    /*
                    $order->Date = ($data['Date'] != NULL) ? $data['Date']->format('Y-m-d') : NULL;
                    $order->DateCommitted = ($data['DateCommitted'] != NULL) ? $data['DateCommitted']->format('Y-m-d') : NULL;
                    $order->Salesperson_id = $data['Salesperson_id'];
                    $order->TruckAllocatedTotal = $data['TruckAllocatedTotal'];
                    $order->TruckingCostTotal = $data['TruckingCostTotal'];
                    $order->TruckingCostPercent = $data['TruckingCostPercent']; */

                    //die(var_dump($order->save()));
                    $order->save();
                    //$order->myUpdate($id);
                    
                    return $this->redirect($this->generateUrl('MovitTestBundle_homepage')); //contact is name of route
                }
                //die(var_dump($order));
                //die(var_dump($data));
                //pass data, get applied_filter messages back
                
                return $this->render('MovitTestBundle:Orders:editorder.html.php', array(
                                        'editForm' => $form->createView(), 'id' => $id));
                
        } 
        
        $bo = new BaseObject();
        $order = $bo->find($id, 'mvLogisticOrders'); //order with the given ID
       
        //$data = $bo->findData($id, 'mvLogisticOrders', 'mvLogisticOrder_id');
        $data = $order->getCurrentData();
        
        //$data = $order->getData(); //idea
        
       
        //var_dump($data);
        
        $defaultData = array(
            'Date' => $order->Date ? $order->Date : NULL,
            'DateCommitted' => $order->DateCommitted ? $order->DateCommitted : NULL, //DateCommited
            'Salesperson_id' => $order->Salesperson_id,
            'TruckAllocatedTotal' => $order->TruckAllocatedTotal,
            'TruckingCostTotal' => $order->TruckingCostTotal,
            'TruckingCostPercent' => $order->TruckingCostPercent
        );
        
        $editForm = $this->CRUDOrderForm($defaultData);
        
        var_dump($defaultData);
        
        return $this->render('MovitTestBundle:Orders:edit.html.php', array('results' => 'No',
                                'editForm' => $editForm->createView(), 'id' => $id));
    }
    
    
    
  /**
     * Delete an entity.
     *
     * @Route("/order/delete/{id}", name="order_delete")
     * @Template()
     */
    public function deleteorderAction($id)
    {
        
        $bo = new BaseObject();
        $order = $bo->find($id, 'mvLogisticOrders'); //order with the given ID
        $order->delete();
        
        return $this->redirect($this->generateUrl('MovitTestBundle_homepage')); //contact is name of route
    }
    
}