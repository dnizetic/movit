<?php 
namespace Movit\TestBundle\Controller;
	
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Movit\TestBundle\Entity\mvLogisticJob as Job;
use Movit\TestBundle\Entity\BaseObject as BaseObject;

class JobsController extends Controller
{  
    private function CRUDJobForm($defaultData = null)
    {
        //statuses
        /*
        $dal_class = $this->get('DAL')->getInstance();
        $statuses = $dal_class->query('SELECT * FROM mvLogisticStatuses');
        
        var_dump($statuses); */
        
        
        //validation
        //http://symfony.com/doc/2.0/book/forms.html#form-validation
        return $this->createFormBuilder($defaultData)
             
             //numeric
             ->add('mvLogisticOrder_id', 'integer', array('required' => false))
             ->add('mvLogisticStatus_id', 'integer', array('required' => false))
                
                
             ->add('DateCreditApproved', 'date', array('required' => false))
             ->add('FreightAllocated', 'money', array('required' => false))
             ->add('DelInfo', 'choice', array('required' => false, 'choices' => array('1__choice__' => 'Yes', '0__choice__' => 'No')))   
             ->add('OffloadRequired', 'choice', array('required' => false, 'choices' => array('1__choice__' => 'Yes', '0__choice__' => 'No')))
             ->add('mvLogisticDelBy_id', 'integer', array('required' => false))   
             ->add('DateDelSched', 'date', array('required' => false))
             ->add('DateDelSched2', 'date', array('required' => false))
             ->add('mvLogisticSKU_id', 'integer', array('required' => false))      
             ->add('SKU', 'text', array('required' => false))   
             ->add('Container', 'text', array('required' => false, 'max_length' => 20))
             ->add('WO', 'text', array('required' => false, 'max_length' => 50))   
                
             ->add('DateShopIn', 'date', array('required' => false))
             ->add('DateShopOutSched', 'date', array('required' => false))
             ->add('DateShopOutActual', 'date', array('required' => false))
                
             ->add('Weight', 'integer', array('required' => false))
             ->add('lwh', 'text', array('required' => false, 'max_length' => 50))
             ->add('AlertWeightDimension', 'choice', array('required' => false, 'choices' => array(1 => 'Yes', 0 => 'No')))
             ->add('PUCityYard', 'text', array('required' => false))
             ->add('PUProv', 'text', array('required' => false, 'max_length' => 2))   
             ->add('PUDateSched', 'date', array('required' => false))
             ->add('PUDateActual', 'date', array('required' => false))
             ->add('DelCityYard', 'text', array('required' => false, 'max_length' => 50))
             ->add('DelProv', 'text', array('required' => false, 'max_length' => 2))
             ->add('DelDateSched', 'date', array('required' => false))
             ->add('DelDateActual', 'date', array('required' => false))
             ->add('mvLogisticServiceType_id', 'integer', array('required' => false))
                
             ->add('CarrierName', 'text', array('required' => false, 'max_length' => 50))
             ->add('CarrierQuote', 'number', array('required' => false))
             ->add('CarrierNotes', 'text', array('required' => false))


                
             ->getForm();
    }
    
    
   /**
   *
   * @Route("/job/new/{id}", name="job_new", requirements = {"id" = "\d+"},
                defaults={"id" = "0"})
   * @Template()
   */
    public function newjobAction($id)
    {
        
        //check whether Order with $id exists in DB
        $form = $this->CRUDJobForm();
        
        if($id) {
            $bo = new BaseObject();
            $order = $bo->find($id, 'mvLogisticOrders'); //order with the given ID

            $dd = array(
                'mvLogisticOrder_id' => $id
            );
            $form = $this->CRUDJobForm($dd);
        }
        
        $dal_class = $this->get('DAL')->getInstance();
        $job = new Job();
        
        
        
        $request = $this->getRequest();
        
        
        if($request->getMethod() == 'POST') {
            
                
                $form->bindRequest($request);
                
                if($form->isValid()) { //add is_valid() method, for error messagess here
                    $data = $form->getData();


                    //$job = $this->get('mvLogisticJob');
                    $job = new Job();
                    //Definition and Usage. The eval() function evaluates a string as PHP code. The string must be valid PHP code and must end with semicolon
                    
                    //goal:
                    //'argument', 'argument', ...
                    
                    $arg_string = "";
                    $i = 0;
                    foreach($data as $key => $value) {

                        if($i > 0)
                            $arg_string .= ', ';
                        if(is_object($data[$key])) { //datetime
                            
                            $arg_string .= '$data';
                            $arg_string .= "['$key']";
                        } else
                            $arg_string .= ($data[$key] == NULL) ? "''" : "'$data[$key]'";
                        
                            
                        ++$i;
                    }
                    //die(var_dump($data) . var_dump($arg_string));
                    //die($arg_string);
                    
                    eval("\$job->mvLogisticJob($arg_string);");
                   
                    //die(var_dump($job));
        
                    $job->mySave();
                    
                    return $this->redirect($this->generateUrl('MovitTestBundle_homepage')); //contact is name of route
                }
                //die(var_dump($order));
                //die(var_dump($data));
                //pass data, get applied_filter messages back
                
                
                
        }
        
        $error = '';
        
        
        return $this->render('MovitTestBundle:Jobs:new.html.php', array('results' => 'No',
                    'form' => $form->createView(), 'error' => $error ));
    }
    
    
  /**
     * Edit a job.
     *
     * @Route("/job/edit/{id}", name="job_edit")
     * @Template()
     */
    public function editjobAction($id) //user can only edit his addresses
    {
        //get the object from the DB
        
        
        $request = $this->getRequest();
        if($request->getMethod() == 'POST') {
            
                $form = $this->CRUDJobForm();
                $form->bindRequest($request);
                
                if($form->isValid()) { //add is_valid() method, for error messagess here
                    $data = $form->getData();

                    
                    
                    //$job = $this->get('mvLogisticJob');
                    $job = new Job();
                    //below can be shortened via eval
                    
                     $job->mvLogisticOrder_id = $data['mvLogisticOrder_id']; 
                     $job->mvLogisticStatus_id = $data['mvLogisticStatus_id']; 
                     $job->DateCreditApproved = ($data['DateCreditApproved'] != NULL) ? $data['DateCreditApproved']->format('Y-m-d') : NULL;
                     $job->FreightAllocated = $data['FreightAllocated']; 
                     $job->DelInfo = $data['DelInfo']; 
                     $job->OffloadRequired = $data['OffloadRequired']; 
                     $job->mvLogisticDelBy_id = $data['mvLogisticDelBy_id']; 
                     $job->DateDelSched = ($data['DateDelSched'] != NULL) ? $data['DateDelSched']->format('Y-m-d') : NULL;
                     $job->DateDelSched2 = ($data['DateDelSched2'] != NULL) ? $data['DateDelSched2']->format('Y-m-d') : NULL;
                     $job->mvLogisticSKU_id = $data['mvLogisticSKU_id']; 
                     $job->SKU = $data['SKU']; 
                     $job->Container = $data['Container']; 
                     $job->WO = $data['WO']; 
                     $job->DateShopIn = ($data['DateShopIn'] != NULL) ? $data['DateShopIn']->format('Y-m-d') : NULL;
                     $job->DateShopOutSched = ($data['DateShopOutSched'] != NULL) ? $data['DateShopOutSched']->format('Y-m-d') : NULL;
                     $job->DateShopOutActual = ($data['DateShopOutActual'] != NULL) ? $data['DateShopOutActual']->format('Y-m-d') : NULL;
                     $job->Weight = $data['Weight']; 
                     $job->lxv = $data['lwh']; 
                     $job->AlertWeightDimension = $data['AlertWeightDimension']; 
                     $job->PUCityYard = $data['PUCityYard']; 
                     $job->PUProv = $data['PUProv']; 
                     $job->PUDateSched = ($data['PUDateSched'] != NULL) ? $data['PUDateSched']->format('Y-m-d') : NULL;
                     $job->PUDateActual = ($data['PUDateActual'] != NULL) ? $data['PUDateActual']->format('Y-m-d') : NULL;
                     $job->DelCityYard = $data['DelCityYard']; 
                     $job->DelProv = $data['DelProv']; 
                     $job->DelDateSched = ($data['DelDateSched'] != NULL) ? $data['DelDateSched']->format('Y-m-d') : NULL;
                     $job->DelDateActual = ($data['DelDateActual'] != NULL) ? $data['DelDateActual']->format('Y-m-d') : NULL;
                     $job->mvLogisticServiceType_id = $data['mvLogisticServiceType_id']; 
                     $job->CarrierName = $data['CarrierName']; 
                     $job->CarrierQuote = $data['CarrierQuote']; 
                     $job->CarrierNotes = $data['CarrierNotes'];  
                    
                     //die(var_dump($job));

                     $job->myUpdate($id);
                    
                     return $this->redirect($this->generateUrl('MovitTestBundle_homepage')); //contact is name of route
                }
                //die(var_dump($order));
                //die(var_dump($data));
                //pass data, get applied_filter messages back
                
                return $this->render('MovitTestBundle:Jobs:editorder.html.php', array(
                                        'editForm' => $form->createView(), 'id' => $id));
                
        } 
        //find the object by ID
        //$order = $this->get('mvLogisticOrders')->findById($id);
        //$job = $this->get('mvLogisticJob');
        $job = new Job();
        
        $my_job = $job->findById($id);
        if($my_job == false)
            die('No such obj');
        
        
        $dd = array(
            'mvLogisticOrder_id' => $my_job->get('mvLogisticOrder_id'),  //target $my_job->get('field'),
            'mvLogisticStatus_id' => $my_job->get('mvLogisticStatus_id'), 
            'DateCreditApproved' => $my_job->get('DateCreditApproved'),
            'FreightAllocated' => $my_job->get('FreightAllocated'),
            'DelInfo' => $my_job->get('DelInfo'), 
            'OffloadRequired' => $my_job->get('OffloadRequired'), 
            'mvLogisticDelBy_id' => $my_job->get('mvLogisticDelBy_id'),
            'DateDelSched' => $my_job->get('DateDelSched'),  
                                        
            'DateDelSched2' => $my_job->get('DateDelSched2'),
            'mvLogisticSKU_id' => $my_job->get('mvLogisticSKU_id'),
            'SKU' => $my_job->get('SKU'),
            'Container' => $my_job->get('Container'),  
            'WO => $this->WO' => $my_job->get('WO'),  
            'DateShopIn' => $my_job->get('DateShopIn'),
            'DateShopOutSched' => $my_job->get('DateShopOutSched'),  
            'DateShopOutActual' => $my_job->get('DateShopOutActual'), 
                                        
            'Weight' => $my_job->get('Weight'),
            '[L x W x H]' =>  $my_job->get('lxv'),
            'AlertWeightDimension' => $my_job->get('AlertWeightDimension'),
            'PUCityYard' => $my_job->get('PUCityYard'),  
            'PUProv' => $my_job->get('PUProv'),  
            'PUDateSched' => $my_job->get('PUDateSched'),  
            'PUDateActual' => $my_job->get('PUDateActual'),  
            'DelCityYard' => $my_job->get('DelCityYard'),  
                                        
            'DelProv' => $my_job->get('DelProv'),  
            'DelDateSched' => $my_job->get('DelDateSched'),  
            'DelDateActual' => $my_job->get('DelDateActual'),  
            'mvLogisticServiceType_id' => $my_job->get('mvLogisticServiceType_id'),  
            'CarrierName' => $my_job->get('CarrierName'),  
            'CarrierQuote' => $my_job->get('CarrierQuote'),  
            'CarrierNotes' => $my_job->get('CarrierNotes'),
        );
        
        var_dump($dd); //default data
        
        $editForm = $this->CRUDJobForm($dd);
        
        //var_dump($defaultData);
        
        return $this->render('MovitTestBundle:Jobs:edit.html.php', array('editForm' => $editForm->createView(), 
                                'id' => $id));
    }
    
    /**
     * Delete an entity.
     *
     * @Route("/job/delete/{id}", name="job_delete")
     * @Template()
     */
    public function deletejobAction($id) //user can only edit his addresses
    {
        //$job = $this->get('mvLogisticJob');
        $job = new Job();
        
        //$my_order = $job->findById($id);
        
        if($job->myDelete($id) == false)
           die('No such obj');
        
        return $this->redirect($this->generateUrl('MovitTestBundle_homepage')); //contact is name of route
    }
}