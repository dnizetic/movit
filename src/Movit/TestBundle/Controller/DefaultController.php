<?php

namespace Movit\TestBundle\Controller;
	
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Movit\TestBundle\Entity\mvLogisticOrder;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;


class DefaultController extends Controller
{
    

    
    
    public function indexAction()
    {
        $dal_class = $this->get('DAL')->getInstance();

        
        //indexed list of Statuses
        //status[id] = value
        

        //var_dump($statuses);
        
        //$dal_class->loadFixtures();
        
        $orders_filter_form = $this->createOrdersFilterForm();
        $jobs_filter_form = $this->createJobsFilterForm();
        
        $orders = $dal_class->query('SELECT * FROM mvLogisticOrders');
        $jobs = $dal_class->query('SELECT * FROM mvLogisticJobs');
        
        
        
        return $this->render('MovitTestBundle:Default:index.html.php', array('orders' => $orders, 'jobs' => $jobs,
                             'order_f_form' => $orders_filter_form->createView(),
                             'job_f_form' => $jobs_filter_form->createView()));

    }
	

    public function vendorAction()
    {   
        //query to get certain data from db
        $test = new mvLogisticOrder();
        //test.
        return $this->render('MovitTestBundle:Default:vendor.html.twig');
        //return array('name' => 'this');
    }
    
    
	public function salesAction()
	{
        return $this->render('MovitTestBundle:Default:sales.html.php');
	}
    
    
     
    
     
  /**
     * Ajax for index page.
     *
     * @Route("/ajax_orders", name="index_ajax")
     * @Template()
     */
    public function ajaxAction()
    {
        $request = $this->getRequest();
        
        
        $my_filter = $this->get('Filter');
        
        $table = $request->request->get('table');
        $orders_query = "SELECT * FROM $table";
       
        
        
        $timestamp = $request->request->get('timestamp');
        $clk = $request->request->get('clk');
        $applied_filters_messages = array();
        $form = ($table == 'mvLogisticOrders') ? 
            $this->createOrdersFilterForm() : $this->createJobsFilterForm();
        if($request->getMethod() == 'POST' && $clk == 'yes') {
            


                $form->bindRequest($request);
                $data = $form->getData();

                //var_dump($data);
                //pass data, get applied_filter messages back
                //$applied_filters_messages = $this->applyFilters($data, $orders_query);
                $applied_filters_messages = $my_filter->applyFilters($data, $orders_query);
                
        }
        
        //return all rows from orders, non filtered
        $dal_class = $this->get('DAL')->getInstance();
        
        
        
        $statuses = $dal_class->query('SELECT * FROM mvLogisticStatuses');
        $statuses_desc = array();
        foreach($statuses as $status) 
            $statuses_desc[$status->get('mvLogisticStatus_id')] = $status->get('Description');
        
        
        $results = $dal_class->query($orders_query);
        //die(var_dump($orders_query));
        
        return $this->render('MovitTestBundle:Default:ajax.html.php', array('results' => $results, 
            'timestamp' => $timestamp, 'table' => $table, 'msgs' => $applied_filters_messages,
            'statuses' => $statuses_desc, 'form' => $form->createView()));
    }
    
    
    
    
    
    private function createOrdersFilterForm()
    {
        return $this->createFormBuilder()
             ->add('mvLogisticOrder_id_from', 'integer', array('required' => false))
             ->add('mvLogisticOrder_id_to', 'integer', array('required' => false))
             
                
             ->add('TruckAllocatedTotal_from', 'number', array('required' => false))
             ->add('TruckAllocatedTotal_to', 'number', array('required' => false))
                
             ->add('TruckingCostTotal_from', 'number', array('required' => false))
             ->add('TruckingCostTotal_to', 'number', array('required' => false))
                
                
             ->add('TruckingCostPercent_from', 'number', array('required' => false))
             ->add('TruckingCostPercent_to', 'number', array('required' => false))
                
             ->add('Date_from', 'date', array('required' => false))
             ->add('Date_to', 'date', array('required' => false))
                
             ->add('DateCommitted_from', 'date', array('required' => false))
             ->add('DateCommitted_to', 'date', array('required' => false))
                
             ->add('Salesperson_id_from', 'integer', array('required' => false))
             ->add('Salesperson_id_to', 'integer', array('required' => false))
             ->getForm();
    }
    
    private function createJobsFilterForm()
    {
        return $this->createFormBuilder()
             
             //numeric
             ->add('mvLogisticJob_id_from', 'integer', array('required' => false))
             ->add('mvLogisticJob_id_to', 'integer', array('required' => false))
			 
                
             ->add('mvLogisticOrder_id_from', 'integer', array('required' => false))
             ->add('mvLogisticOrder_id_to', 'integer', array('required' => false))

             ->add('Weight_from', 'integer', array('required' => false))
             ->add('Weight_to', 'integer', array('required' => false))
                
             ->add('FreightAllocated_from', 'number', array('required' => false))
             ->add('FreightAllocated_to', 'number', array('required' => false))
                

             ->add('CarrierQuote_from', 'number', array('required' => false))
             ->add('CarrierQuote_to', 'number', array('required' => false))
                
                
             //date fields
             ->add('DateCreditApproved_from', 'date', array('required' => false))
             ->add('DateCreditApproved_to', 'date', array('required' => false))
             
             ->add('DateDelSched_from', 'date', array('required' => false))
             ->add('DateDelSched_to', 'date', array('required' => false))
                
             ->add('DateDelSched2_from', 'date', array('required' => false))
             ->add('DateDelSched2_to', 'date', array('required' => false))
                
             ->add('DateShopIn_from', 'date', array('required' => false))
             ->add('DateShopIn_to', 'date', array('required' => false))
                
             ->add('DateShopOutSched_from', 'date', array('required' => false))
             ->add('DateShopOutSched_to', 'date', array('required' => false))
                
             ->add('DateShopOutActual_from', 'date', array('required' => false))
             ->add('DateShopOutActual_to', 'date', array('required' => false))
                
             ->add('PUDateSched_from', 'date', array('required' => false))
             ->add('PUDateSched_to', 'date', array('required' => false))
                
                
             ->add('PUDateActual_from', 'date', array('required' => false))
             ->add('PUDateActual_to', 'date', array('required' => false))
                
             ->add('DelDateSched_from', 'date', array('required' => false))
             ->add('DelDateSched_to', 'date', array('required' => false))
                
             ->add('DelDateActual_from', 'date', array('required' => false))
             ->add('DelDateActual_to', 'date', array('required' => false))
                
                
             //bit fields: yes or no
             ->add('DelInfo', 'choice', array('required' => false, 'choices' => array('1__choice__' => 'Yes', '0__choice__' => 'No')))
             ->add('OffloadRequired', 'choice', array('required' => false, 'choices' => array('1__choice__' => 'Yes', '0__choice__' => 'No')))
             //->add('AlertWeightDimension', 'choice', array('required' => false, 'choices' => array(1 => 'Yes', 0 => 'No')))
             
             ->add('SKU', 'text', array('required' => false))
                
             ->add('PUCityYard', 'text', array('required' => false))
             ->add('DelCityYard', 'text', array('required' => false))
                
             ->getForm();
    }
    

}