<?php

namespace Movit\TestBundle\Controller;
	
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Response;


class SalesController extends Controller
{
    public function salesAction()
    {
        //get SKU's
        $dal_class = $this->get('DAL')->getInstance();
        
        $skus = $dal_class->query('SELECT * FROM mvLogisticSKUs');
        
        //$cities = $dal_class->query('SELECT * FROM mvLogisticCities');
        //var_dump($cities);
        
        //var_dump($skus);
        
        $size_types = array();
        foreach($skus as $sku) {
            if($sku->get('SizeType'))
                $size_types[] = $sku->get('SizeType');
        }
        
        //var_dump($size_types);

        $stock_items = array();
        foreach($skus as $stock_item) {
            if(strlen($stock_item->get('StockItems')) > 0)
                $stock_items[] = $stock_item->get('StockItems');
        }
        
        //var_dump($stock_items);
        $cities_form = $this->createCitiesForm();
        
        return $this->render('MovitTestBundle:Sales:sales.html.php', array(
                    'size_types' => $size_types, 'cities_form' => $cities_form->createView()
        ));
    }
    
   /**
     * Ajax for Sales page.
     *
     * @Route("/sales/ajax", name="sales_ajax")
     * @Template()
     */
    public function ajaxAction()
    {
        $request = $this->getRequest();
        
        if($request->getMethod() == 'POST') {
            $form = $this->createCitiesForm();
            
            $form->bindRequest($request);
            
            if($form->isValid()) { //add is_valid() method, for error messagess here
               
                $data = $form->getData();
                
                $dal_class = $this->get('DAL')->getInstance();
                $conn = $dal_class->dbconnect();
                        
                $tsql = "INSERT INTO mvLogisticCities (City, Province, Approved) VALUES (?,?,?)";
                

                /* Prepare and execute the statement. */
                $insertReview = sqlsrv_prepare($conn, $tsql, 
                        array(
                            &$data['City'], 
                            &$data['Province'], 0));

                
                if( $insertReview === false )
                    { return sqlsrv_errors(); }
                    

                /* By default, all stream data is sent at the time of query execution. */
                if( sqlsrv_execute($insertReview) === false )
                    { return sqlsrv_errors();  }
                
            }
            return new Response();
        }
        //GET request
        
        $query = $request->query->get('query');
        
        //1. get all data from Yards
        $dal_class = $this->get('DAL')->getInstance();
        $yards = $dal_class->query('SELECT * FROM mvLogisticYards');
        
        $suggestions = array();
        foreach($yards as $yard) 
            if(strpos($yard->get('YardCode'), strtoupper($query)) !== false) 
                $suggestions[] = $yard->get('YardCode');
            
        
        //var_dump($suggestions);
        //get array of suggestions
        
        
        return $this->render('MovitTestBundle:Sales:ajaxsales.html.php', array(
                'query' => $query, 'suggestions' => $suggestions
        ));
    }
    
    
   /**
     * Ajax for Sales page.
     *
     * @Route("/sales/sku/ajax", name="sales_sku_ajax")
     * @Template()
     */
    public function ajaxskuAction()
    {
        $request = $this->getRequest();
        
        
        $query = $request->query->get('size_type');
        
        
        //1. get all data from Yards
        $dal_class = $this->get('DAL')->getInstance();
        $skus = $dal_class->query('SELECT * FROM mvLogisticSKUs');
        
        $sku_objects = array();
        foreach($skus as $sku) 
            if(strpos($sku->get('StockItems'), strtoupper($query)) !== false) 
                $sku_objects[] = $sku;
            
        
        //var_dump($sku_objects);
        //get array of suggestions
        
        //var_dump($sku_objects);
        
        return $this->render('MovitTestBundle:Sales:ajaxskusales.html.php', array(
                'size_type' => $query, 'sku_objects' => $sku_objects
        ));
    }
    
   /**
     * Ajax for Sales page.
     *
     * @Route("/sales/city/ajax", name="sales_city_ajax")
     * @Template()
     */
    public function ajaxcityAction()
    {
        //GET request
        
        $request = $this->getRequest();
        
        
        $query = $request->query->get('query');
        
        //1. get all data from Yards
        $dal_class = $this->get('DAL')->getInstance();
        $cities = $dal_class->query('SELECT * FROM mvLogisticCities');
        
        
        //var_dump($cities);
        $suggestions = array();
        foreach($cities as $city) 
            if(strpos(strtoupper($city->get('City')), strtoupper($query)) !== false /*&& $city->get('Approved')*/) 
                $suggestions[] = $city->get('City') . ", " . $city->get('Province');
            
        //var_dump($suggestions);
        //get array of suggestions
        
        return $this->render('MovitTestBundle:Sales:ajaxsales.html.php', array(
                'query' => $query, 'suggestions' => $suggestions
        ));
    }
    
    
    private function createCitiesForm()
    {
        return $this->createFormBuilder()
             ->add('City', 'text', array('required' => false, 'max_length' => 50))
             ->add('Province', 'text', array('required' => false, 'max_length' => 2))
             ->getForm();
    }
   
}
